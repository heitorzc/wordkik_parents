package com.wordkik.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.activities.ChildFeatures;
import com.wordkik.activities.MainActivity;
import com.wordkik.activities.QrReader;
import com.wordkik.fragments.ChildProfilesFragment;
import com.wordkik.objects.Child;
import com.wordkik.objects.ResponseChild;
import com.wordkik.tasks.ChildTask;
import com.wordkik.tasks.TaskManager;
import com.wordkik.views.ConfirmationDialog;
import com.wordkik.views.ManageChildDialog;

import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;


/**
 * Created by heitorzc on 09/02/16.
 * Last update on 11/10/16.
 */

public class ChildManager implements ChildTask.TaskListener, TaskManager.IMethodName {


    private ManageChildDialog childDialog;
    private Activity context;
    private Child child;
    private String cancel_reason;

    private static ChildManager instance = null;



    public ChildManager(Context context){
        this.context = (Activity) context;

        instance = this;
    }



    public static ChildManager getInstance(Activity context){

        if (instance != null){
            return instance;
        }
        else {
            return new ChildManager(context);
        }

    }




    /**
     * Method called to read child`s QR Code
     */
    public void startQrReader(){
        int CAMERA = ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA);

        if (CAMERA == PackageManager.PERMISSION_GRANTED) {
            context.startActivity(new Intent(context, QrReader.class));
        }
        else {
            Nammu.init(context);
            Nammu.askForPermission(context, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA}, new PermissionCallback() {
                @Override
                public void permissionGranted() {
                    context.startActivity(new Intent(context, QrReader.class));
                }

                @Override
                public void permissionRefused() {

                }
            });
        }
    }




    /**
     * Method called to insert personal info about child before send the request to the server.
     */

    public void showAddChildFormDialog(String code){

        childDialog = new ManageChildDialog(context);
        final AlertDialog addChildDialog = childDialog.buildDialog(R.layout.child_add_dialog, R.string.text_ok, R.string.text_cancel);

        childDialog.setQRCode(code);

        addChildDialog.show();
        addChildDialog.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (childDialog.getNewChild() != null) {
                    new ChildTask(context, ChildManager.this).linkChildToParent(childDialog.getNewChild());
                    addChildDialog.dismiss();
                }

            }
        });

        addChildDialog.getButton(Dialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addChildDialog.dismiss();
                showCancelFeedbackDialog(null);

            }
        });

    }




    /**
     * Method called to edit a child profile already added.
     * @param child is the object to be edited.
     */

    public void edit(final Child child){
        this.child = child;

        childDialog = new ManageChildDialog(context);
        final AlertDialog editChildDialog = childDialog.buildDialog(R.layout.child_add_dialog, R.string.text_ok, R.string.text_cancel);

        editChildDialog.show();
        editChildDialog.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (childDialog.getNewChild() != null) {
                    new ChildTask(context, ChildManager.this).editChild(childDialog.getNewChild());
                    editChildDialog.dismiss();
                }

            }
        });

        childDialog.loadChildInfo(child);

    }



    /**
     * Method called to show a feedback dialog when user cancel adding child process.
     */

    public void showCancelFeedbackDialog(ManageChildDialog.ChildDialogListener listener){

        childDialog = (childDialog == null) ? new ManageChildDialog(context) : childDialog;
        childDialog.setOnDialogDismissListener(listener);

        final AlertDialog cancelFeedbackDialog = childDialog.buildDialog(R.layout.cancel_feedback_dialog, R.string.send, R.string.text_cancel);

        cancelFeedbackDialog.show();
        ((WordKik) context.getApplicationContext()).mpTrack("Cancelled add child process");

        final View customView = childDialog.getCustomView();

        final EditText etCustom = (EditText) customView.findViewById(R.id.etCustom);

        final RadioGroup rgReasons = (RadioGroup) customView.findViewById(R.id.rgReasons);
        rgReasons.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                RadioButton selected = (RadioButton) customView.findViewById(i);
                cancel_reason = selected.getText().toString();

                if (i == R.id.other){
                    etCustom.setVisibility(View.VISIBLE);
                }
                else {
                    etCustom.setVisibility(View.GONE);
                }

            }
        });

        cancelFeedbackDialog.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rgReasons.getCheckedRadioButtonId() == -1){
                    Toast.makeText(context, "Please, select an option to continue", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!etCustom.getText().toString().isEmpty() && rgReasons.getCheckedRadioButtonId() == R.id.other) {
                    cancel_reason = etCustom.getText().toString();
                }

                ((WordKik) context.getApplicationContext()).mpTrack("Selected: " + cancel_reason);

                cancelFeedbackDialog.dismiss();
            }

        });

    }



    /**
     * Callback to update the UI when a child is created or edited successfully.
     * @param responseObject is the response from the API
     * @param methodName is the name of the method that sent the request.
     */

    @Override
    public void performTask(Object responseObject, String methodName) {
        Log.e("PERFORM_TASK", "METHOD: " + methodName + " CLASS: " + ChildManager.class);

        switch (methodName) {
            case LINK_TO_PARENT:
                performLinkToParent(responseObject);
                break;
            case EDIT_CHILD:
                performEditChild(responseObject);
                break;
            default:
                Log.e("PERFORM_TASK", "METHOD NOT IMPLEMENTED");
                break;
        }

        WordKik.saveLogcatToFile("ADDCHD_396");
    }



    private void performLinkToParent(Object responseObject) {
        ResponseChild responseChild = (ResponseChild) responseObject;

        if (responseChild.isSuccess()) {

            Constants.childs.add(responseChild.getChild());
            ((MainActivity) context).reloadChildrenSection();

            ((WordKik) context.getApplicationContext()).mpTrack("Added a child");

        }

        else {

            Log.e("ERROR", "performLinkToParent");
            ConfirmationDialog.with(context)
                    .title(R.string.child_nf_title)
                    .message(R.string.child_nf_msg)
                    .background(R.color.red)
                    .show();

        }
    }



    private void performEditChild(Object responseObject) {
        ResponseChild responseChild = (ResponseChild) responseObject;

        if (responseChild.isSuccess()) {
            child = Constants.getChildById(child.getId());

            Constants.childs.add(Constants.childs.indexOf(child), responseChild.getChild());
            Constants.childs.remove(child);

            ((ChildFeatures) context).setChildData(responseChild.getChild());
            ChildProfilesFragment.hasEditedChild = true;


        }

    }

}
