package com.wordkik.utils;

import android.content.Context;
import android.os.Handler;
import android.view.View;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by Heitor Zanetti Cunha on 08/08/2014.
 * This class uses the following library to work:
 * Android View Animations by Daimajia
 * Link to download: https://github.com/daimajia/AndroidViewAnimations
 *
 * Usage:
 *        1- Add Android View Animation to your project;
 *        2- Copy this class to your package.
 *
 *        When you want to do a sequential animation just make a call:
 *
 *        Animate.anime(new View[] {
 *              viewToAnimate1,
 *              viewToAnimate2,
 *              viewToAnimate3,
 *              ... },
 *
 *              int waitToStartNextAnimation,
 *              int animationTime,
 *              Techniques.Tada);
 *
 * You can find all available effects on library's Github page.
 * https://github.com/daimajia/AndroidViewAnimations
 *
 *
 * Do you need any help?
 * You can find me at: https://plus.google.com/+HeitorZanettiCunha
 *                     https://facebook.com/heitorzcunha
 *
 *Good luck!! :)
 */
public class AnimateFeedback extends Thread {
    static Handler mHandler;
    static View v;
    static Context mContext;
    static int wait, animation;
    static Techniques effect;
    int i = 0;

    AnimateInterface listener;

    public interface AnimateInterface{
        void onAnimationFinish(View v);
    }


    public AnimateFeedback(Context c, View v, int wait, int animation, Techniques effect, AnimateInterface listener) {
        this.mContext = c;
        this.v = v;
        this.wait = wait;
        this.animation = animation;
        this.effect = effect;
        this.listener = listener;
    }

    @Override
    public void run() {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        addWorker(executor, wait, v);

        executor.shutdown();
        while (!executor.isTerminated()) {
            try {
                Thread.sleep(wait);
            } catch (InterruptedException e) {
            }
        }
    }

    public static void anime(View v, int wait, int animation, Techniques effect, AnimateInterface listener) {
        mHandler = new Handler();
        AnimateFeedback animate = new AnimateFeedback(mContext, v, wait, animation, effect, listener);
        animate.start();
    }

    public void addWorker(ExecutorService ex, int sleep, View v) {
        Runnable worker = new startAnimation(v);
        ex.submit(worker);

        try {
            Thread.sleep(sleep);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    public class startAnimation implements Runnable {
        private View mView;

        public startAnimation(View v) {
            this.mView = v;
        }

        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mView.setVisibility(View.VISIBLE);
                    YoYo.with(effect)
                            .duration(animation)
                            .playOn(mView);
                    i++;

                    if (listener != null){
                        listener.onAnimationFinish(mView);
                    }
                }
            });
        }
    }
}
