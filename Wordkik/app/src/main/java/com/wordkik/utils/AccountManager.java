package com.wordkik.utils;

import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.activities.LoginActivity;
import com.wordkik.activities.MainActivity;
import com.wordkik.objects.App;
import com.wordkik.objects.Child;
import com.wordkik.objects.Parent;
import com.wordkik.preferences.EditAccountDialog;
import com.wordkik.views.ConfirmationDialog;
import com.wordkik.views.ManagePinCodeDialog;

/**
 * Created by heitorzc on 03/10/16.
 */
public class AccountManager implements ConfirmationDialog.ConfirmationDialogInterface, EditAccountDialog.EditProfileListener{

    private final String TAG = "ACCMANAGER";

    public static final String ACCOUNT_FREE      = "free";
    public static final String ACCOUNT_PREMIUM   = "paying";

    private final int FREE_TIMELOCK_LIMIT  = 3;
    private final int FREE_GEOFENCE_LIMIT  = 2;
    private final int FREE_APP_USAGE_LIMIT = 3;

    private final int PREMIUM_LIMIT        = 9999;

    private int timelocks_remaining;
    private int subscription_left;
    private String accountType             = "";

    private Activity context;
    private Application application;
    private static AccountManager instance;


    private AccountManager(Application application) {
        this.application = application;
    }


    public static void init(Application application) {

        if (instance == null) {
            instance = new AccountManager(application);
        }

    }


    public static AccountManager getInstance(Context context){

        if (instance == null) {
            instance = new AccountManager((WordKik) context.getApplicationContext());
        }

        return instance;

    }

    public void setAccountData(Activity context, String accountType, int subscription_left) {
        this.context = context;
        this.accountType       = accountType;
        this.subscription_left = subscription_left;

        checkSubscription();
    }





    /**
     * This method must be called inside onResume of all activities and fragments to
     * set the new Context to AccountManager.
     * @param context
     */
    public void onResume(Activity context){

        if (accountType.isEmpty()){
            context.startActivity(new Intent(context, LoginActivity.class));
            context.finish();
            return;
        }

        this.context = context;

    }





    /**
     * @return the account type the user has, whether is ACCOUNT_FREE or ACCOUNT_PREMIUM.
     */
    public String getAccountType(){
        return this.accountType;
    }





    /**
     * Usually account info will change after a subscription is purchased.
     * This method will update the account type and subscription's days left.
     */
    public void updateAccountInfo(){
        this.accountType = Constants.user_account_type;
        this.subscription_left = Constants.user_expiration;
    }





    /**
     * @return the number of days remaining to expire a subscription.
     */
    public int getSubscriptionLeft(){
        return this.subscription_left;
    }





    /**
     * Check if the current subscription is already expired.
     * @return true if expired, false otherwise.
     */
    public boolean isSubscriptionExpired(){
        if (getAccountType().equals(ACCOUNT_PREMIUM)){
            return (getSubscriptionLeft() <= 0);
        }

        return false;
    }





    /**
     * In case of an expired subscription, show a dialog box to inform the user.
     */
    public void checkSubscription(){

        if (isSubscriptionExpired()) {

            showDialog(R.string.subscription_title, R.string.premium_subscription_expired, R.drawable.purchase_section_icon);
            Constants.isSubscriptionValid = false;

        }

    }


    public boolean shouldApplyLimits(){
        return (getAccountType().equals(ACCOUNT_FREE) || subscription_left == 0);
    }





    /**
     * Increment 1 to the TimeLocks limit.
     * @return true when the incrementation is completed.
     */
    public boolean incrementTimeLockToLimit(){
        if (shouldApplyLimits()) {
            timelocks_remaining++;
        }

        Log.i(TAG, "Timelocks Limit Remaining: " + timelocks_remaining);
        return true;
    }





    /**
     * Decrement 1 to the TimeLocks limit if user has a free account.
     * If the limit of time locks has already exceeded, it shows a dialog.
     * @return true when the process is completed, false otherwise.
     */
    public boolean decrementTimeLockFromLimit(){

        if (shouldApplyLimits() && timelocks_remaining <= 0) {
            showDialog(R.string.limit_dialog_title, R.string.limit_timelock_body, R.drawable.limit_reached_icon);
            return false;
        }


        timelocks_remaining--;
        Log.w(TAG, "Timelock Limit Remaining: " + timelocks_remaining);

        return true;
    }





    /**
     * Check if a free user still can create time locks for an specific child.
     * It shows a dialog if the limit has already exceeded.
     * @param child to check the limit.
     * @return false if the limit has exceeded, true otherwise.
     */
    public boolean checkTimeLocksLimit(Child child){

        if (shouldApplyLimits()){

            timelocks_remaining = FREE_TIMELOCK_LIMIT - child.getTimeLocks().size();

            if (timelocks_remaining <= 0) {
                showDialog(R.string.limit_dialog_title, R.string.limit_timelock_body, R.drawable.limit_reached_icon);
                return false;
            }

            return true;
        }

        timelocks_remaining = PREMIUM_LIMIT;

        return true;

    }





    /**
     * Check if a free user still can lock or limit apps for an specific child.
     * It shows a dialog if the limit has already exceeded.
     * @param child to check the limit.
     * @return false if the limit has exceeded, true otherwise.
     */
    public boolean checkAppUsageLimit(Child child, App app){

        if (shouldApplyLimits()){

            if (child.getUsageCount() >= FREE_APP_USAGE_LIMIT){

                if (!app.hasUsageLimit()) {
                    showDialog(R.string.limit_dialog_title, R.string.limit_app_usage_body, R.drawable.limit_reached_icon);
                    return false;
                }

            }

        }

        return true;

    }





    /**
     * Check if a free user still can create geofences for an specific child.
     * It shows a dialog if the limit has already exceeded.
     * @param child to check the limit.
     * @return false if the limit has exceeded, true otherwise.
     */
    public boolean checkGeofenceLimit(Child child){

        if (shouldApplyLimits()){

            if (child.getGeofences().size() >= FREE_GEOFENCE_LIMIT){
                showDialog(R.string.limit_dialog_title, R.string.limit_geofence_body, R.drawable.limit_reached_icon);
                return false;
            }

        }

        return true;

    }





    /**
     * Display a dialog when a free user has reached its limits.
     */
    private void showDialog(int title, int body, int icon){
        ConfirmationDialog.with(context)
                .title(title)
                .message(body)
                .background(R.color.green)
                .icon(icon)
                .positiveButtonLabel(R.string.upgrade_now)
                .negativeButtonLabel(R.string.maybe_later)
                .callback(this)
                .gravity(Gravity.START)
                .show();
    }




    /**
     * Show a dialog asking the user to create a PIN Code before
     * adding the first child profile.
     */
    public void showNoPinCodeDialog(int message){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setTitle(R.string.pin_code_title);
        alertDialogBuilder.setMessage(message);

        alertDialogBuilder.setPositiveButton(context.getString(R.string.text_ok), null);

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        alertDialog.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ManagePinCodeDialog(context).registerPinCode();
                alertDialog.dismiss();
            }
        });

    }



    /**
     * Show a dialog asking the user to complete their profile, if incomplete,
     * when they try to add a child for de 2nd+ time.
     */
    public boolean isParentProfileCompleted(Activity activity) {

        if (WordKik.prefs.getBoolean("showProfileDialog", true) && Constants.hasChildren()) {

            if (Constants.user_age              == null || Constants.user_age.isEmpty()      ||
                    Constants.user_name         == null || Constants.user_name.isEmpty()     ||
                    Constants.user_lastName     == null || Constants.user_lastName.isEmpty() ||
                    Constants.user_gender       == null || Constants.user_gender.isEmpty()   ||
                    Constants.user_phone_number == null || Constants.user_phone_number.isEmpty()) {

                ConfirmationDialog.with(activity)
                        .message(ConfirmationDialog.PROFILE_NOT_COMPLETED)
                        .background(R.color.red)
                        .persistent(true)
                        .tag("COMPLETE_PROFILE")
                        .callback(this)
                        .show();

                return false;
            }

            WordKik.prefs.edit().putBoolean("showProfileDialog", false).apply();
        }

        return true;
    }




    /**
     * Callback for the dialog. If the user clicks on the positive button
     * (Upgrade now!), lead it to the purchase section.
     * @param tag
     */
    @Override
    public void onPositiveButtonClick(String tag) {

        if (tag.equals("COMPLETE_PROFILE")){
            new EditAccountDialog(context).editProfile(this, true);
        }
        else {
            context.finish();
            MainActivity.limit_dialog_upgrade = true;
        }

    }




    @Override
    public void onFinishEditing(Parent parent) {
        Constants.user_age          = parent.getAge();
        Constants.user_name         = parent.getFirstName();
        Constants.user_lastName     = parent.getLastName();
        Constants.user_gender       = parent.getGender();
        Constants.user_phone_number = parent.getPhoneNumber();
    }


}
