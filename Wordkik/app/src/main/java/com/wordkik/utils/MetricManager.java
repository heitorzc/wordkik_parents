package com.wordkik.utils;


import android.app.Application;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.wordkik.BuildConfig;
import com.wordkik.objects.Metric;
import com.wordkik.realm.DbProvider;

import java.util.Date;


public class MetricManager {

    //Only allow metrics if it is a Release Version
    private boolean ALLOW_METRICS = !BuildConfig.DEBUG;

    private Application application;
    private Metric metric;
    private static MetricManager instance;


    private MetricManager(Application application) {
        this.application = application;
    }


    public static MetricManager with(AppCompatActivity activity) {

        if (instance == null) {
            instance = new MetricManager(activity.getApplication());
        }

        return instance;
    }


    public static MetricManager with(Fragment fragment) {

        if (instance == null) {
            instance = new MetricManager(fragment.getActivity().getApplication());
        }

        return instance;
    }



    /**
     * @return the current instance of the class.
     */
    public static MetricManager getInstance() {
        return instance;
    }



    /**
     * Reset the instance to default.
     */
    private void resetInstance(){
        instance = null;
        metric = null;
    }



    /**
     * Create a new metric and set the current time_stamp to it.
     * @param section is the name of the section the user just entered.
     */
    public void createNewMetric(String section){
        if (ALLOW_METRICS) {
            this.metric = new Metric(section, new Date().toString());
        }
    }



    /**
     * Register time_stamp for when the user leaves a section and save
     * the completed metrinc into the DB.
     */
    public void setTimeLeaving(){
        if (metric != null) {
            metric.setTime_leaving(new Date().toString());
            DbProvider.with(application).registerNewMetric(metric);

            resetInstance();

        }
    }

}

