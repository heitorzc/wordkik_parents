package com.wordkik.utils;

import android.content.Context;
import android.os.Handler;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.rollbar.android.Rollbar;
import com.wordkik.Constants;
import com.wordkik.WordKik;

/**
 * Created by heitorzc on 19/09/16.
 */
public class AdManager extends AdListener{


    private InterstitialAd mInterstitialAd;
    private Context context;
    private AdRequest request;

    public AdManager(Context context){
        this.context = context;
    }


    public AdRequest requestAd(){

        request = new AdRequest.Builder()
                //.addTestDevice("5DACB6768486D2CBE03BDE85F3BFBF3E") // Heitor's Samsung Galaxy S6 Edge
                .build();

        return request;
    }


    public void requestInterstitialAd(){

        mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId("ca-app-pub-1336255024132376/5157550046");

        mInterstitialAd.loadAd(requestAd());

        mInterstitialAd.setAdListener(this);
    }

    public void requestBannerAd(AdView adview){

        if (Constants.user_account_type.equals("free") || Constants.user_expiration == 0){
            adview.loadAd(requestAd());
        }
        else {
            adview.setVisibility(View.GONE);
        }

    }

    private boolean isAdLoaded(){
        return mInterstitialAd.isLoaded();
    }


    private boolean shouldShowAd() {

        boolean shouldShowAd = WordKik.prefs.getBoolean("shouldShowAd", true);
        boolean isFreeUser = Constants.user_account_type.equals("free") || Constants.user_expiration == 0;

        return (shouldShowAd && isFreeUser);
    }



    public void showAd(){
        if (isAdLoaded() && shouldShowAd()) {
            mInterstitialAd.show();
        }
        else if (shouldShowAd() && !isAdLoaded()){
            Handler handler = new Handler();

            try {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showAd();
                    }
                }, 3000);
            }
            catch (Exception e){
                Rollbar.reportException(new Exception("AdManager tried to show an interstitialAd an a dead thread."));
            }
        }
    }


    @Override
    public void onAdOpened() {
        WordKik.prefs.edit().putBoolean("shouldShowAd", false).apply();
        super.onAdOpened();
    }


    @Override
    public void onAdClosed() {
        requestInterstitialAd();
        super.onAdClosed();
    }


}
