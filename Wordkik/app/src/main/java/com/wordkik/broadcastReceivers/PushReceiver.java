package com.wordkik.broadcastReceivers;


import android.app.Notification;
import android.content.Intent;
import android.graphics.Color;
import android.content.Context;
import android.media.RingtoneManager;
import android.app.NotificationManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.jiongbull.jlog.JLog;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.activities.ChildLocationActivity;
import com.wordkik.objects.Child;
import com.wordkik.tasks.GeneratePushNotification;

import java.util.Set;

public class PushReceiver extends WakefulBroadcastReceiver implements GeneratePushNotification.GeneratePushNotificationInterface{

    private final String TAG = "PUSHY";
    private Context context;
    private int countNotification = 0;

    public static Child acceptLocation;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;

        JLog.json("FIREBASE", "Push Received");

      /*  Set<String> keys = intent.getExtras().keySet();
        String[] kArray = keys.toArray(new String[keys.size()]);

        for (String aKArray : kArray) {
            Log.i(TAG, aKArray + ": " + intent.getStringExtra(aKArray));
        }*/

        String notification_type = intent.getStringExtra("notification_type");

        if (notification_type != null) {

            if (notification_type.equals(Constants.NOTIFICATION_TYPE_CHAT)) {
                notifyUser(Constants.NOTIFICATION_TYPE_CHAT, intent.getStringExtra("title"), intent.getStringExtra("body"), intent.getStringExtra("child_name"), null);
                return;
            }

            if (notification_type.equals(Constants.NOTIFICATION_TYPE_CMS)) {
                notifyUser(Constants.NOTIFICATION_TYPE_CMS, intent.getStringExtra("notification_category"), intent.getStringExtra("title"), intent.getStringExtra("body"), intent.getStringExtra("child_name"), null);
                return;
            }

            if (notification_type.equals(Constants.NOTIFICATION_TYPE_ALERT)) {

                Constants.alerts_counter++;
                new com.wordkik.tasks.NotificationManager(context).updateCounter(Constants.alerts_counter++);

                String category = intent.getStringExtra("notification_category");

                if (category.equals("geofence_activity")){
                    String geo_title = intent.getStringExtra("geofence_title");
                    int transition = intent.getIntExtra("transition", 999);

                    String title = context.getString(R.string.geofence_activity);
                    String body = (transition == 1) ? context.getString(R.string.just_arrived) : (transition == 0) ? context.getString(R.string.just_departured) : context.getString(R.string.loading);

                    notifyUser(Constants.NOTIFICATION_TYPE_ALERT, category, title, body + geo_title, intent.getStringExtra("child_name"), intent.getStringExtra("child_photo"));
                }
                else {
                    notifyUser(Constants.NOTIFICATION_TYPE_ALERT, intent.getStringExtra("title"), intent.getStringExtra("body"), intent.getStringExtra("child_name"), intent.getStringExtra("child_photo"));
                }


                return;
            }

            if (notification_type.equals(Constants.NOTIFICATION_TYPE_GEOFENCE_RPT)){
                new com.wordkik.tasks.NotificationManager(context).updateCounter(Constants.alerts_counter++);
                notifyUser(Constants.NOTIFICATION_TYPE_GEOFENCE_RPT, context.getString(R.string.location_report_push_title), context.getString(R.string.location_report_push_body), intent.getStringExtra("child_name"), intent.getStringExtra("child_photo"));
                return;
            }

            if (notification_type.equals(Constants.NOTIFICATION_TYPE_LOCK_APPS)) {
                Constants.alerts_counter++;
                new com.wordkik.tasks.NotificationManager(context).updateCounter(Constants.alerts_counter++);

                String app_name = intent.getStringExtra("app_name");
                String child_name = intent.getStringExtra("child_name");
                String title = app_name + " " + context.getString(R.string.unlocked);
                String body = app_name + " " + context.getString(R.string.unlocked_push_body).replace("%", child_name);

                notifyUser(Constants.NOTIFICATION_TYPE_LOCK_APPS, title, body, child_name, intent.getStringExtra("child_photo"));
                return;
            }

            if (notification_type.equals(Constants.NOTIFICATION_TYPE_REQUEST_LOCATION)){
                Log.e("Location", "Received!");
                double lat = intent.getDoubleExtra("lat", 0);
                double lon = intent.getDoubleExtra("lon", 0);
                int child_id =  intent.getIntExtra("child_id", 0);

                if (acceptLocation != null) {

                    Log.w("AccLocation", "Only accept id: " + acceptLocation.getId() + "Got: " + child_id);
                    if (child_id ==  acceptLocation.getId()) {
                        if (ChildLocationActivity.mapView != null) {
                            ChildLocationActivity.generateLocationPoint(lat, lon);
                        }
                    }
                }

                return;
            }

        }
    }


    public void notifyUser(String notification_type, String title, String body, String child_name, String child_photo){
        if (Constants.allow_notifications) {
            new GeneratePushNotification(context, notification_type, title, body, child_name, child_photo, this).execute();
        }
    }

    public void notifyUser(String notification_type, String category, String title, String body, String child_name, String child_photo){
        if (Constants.allow_notifications) {
            new GeneratePushNotification(context, notification_type, category, title, body, child_name, child_photo, this).execute();
        }
    }

    @Override
    public void onNotificationReady(Notification notification) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(countNotification++, notification);
    }

}
