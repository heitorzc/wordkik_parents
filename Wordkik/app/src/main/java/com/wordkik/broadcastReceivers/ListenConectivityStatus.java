package com.wordkik.broadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;

import com.wordkik.WordKik;

/**
 * Created by heitorzc on 05/04/16.
 */
public class ListenConectivityStatus extends BroadcastReceiver {

    Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();

        NetworkInfo info = extras.getParcelable("networkInfo");

        NetworkInfo.State state = info.getState();

        if (state == NetworkInfo.State.CONNECTED && !WordKik.hasInternet) {
            Log.w("GCM_PUSH", "---------------------------------------------------");
            Log.i("GCM_PUSH", "---------------------------------------------------");
            Log.e("GCM_PUSH", "--------------- INTERNET AVAILABLE ----------------");
            Log.i("GCM_PUSH", "---------------------------------------------------");
            Log.w("GCM_PUSH", "---------------------------------------------------");

            WordKik.hasInternet = true;
        }
        else if (state == NetworkInfo.State.DISCONNECTED && WordKik.hasInternet){
            Log.w("GCM_PUSH", "---------------------------------------------------");
            Log.i("GCM_PUSH", "---------------------------------------------------");
            Log.e("GCM_PUSH", "----------------- DEVICE OFFLINE ------------------");
            Log.i("GCM_PUSH", "---------------------------------------------------");
            Log.w("GCM_PUSH", "---------------------------------------------------");

            WordKik.hasInternet = false;
        }

    }

    public void register(Context mContext){
        this.mContext = mContext;

        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        mContext.registerReceiver(this, intentFilter);

        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        WordKik.hasInternet = (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected());

    }

    public void unregister(Context mContext){
        this.mContext = mContext;
        mContext.unregisterReceiver(this);
    }

}

