package com.wordkik.enums;

/**
 * Enum to describe gender
 */
public enum GenderEnum {

    MALE("M"), FEMALE("F");

    private String stringValue;

    private GenderEnum(String stringValue) {
        this.stringValue = stringValue;
    }

    public String getStringValue() {
        return stringValue;
    }

}
