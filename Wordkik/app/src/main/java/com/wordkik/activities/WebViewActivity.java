package com.wordkik.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.wordkik.R;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;

public class WebViewActivity extends AppCompatActivity {
    @BindString(R.string.tvHelp)  String needHelp;
    @Bind(R.id.progressBar)       ProgressBar progressBar;
    @Bind(R.id.toolbar)           Toolbar toolbar;
    @Bind(R.id.webView)           WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(needHelp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String url = getIntent().getStringExtra("url");

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new ChromeWebClient());
        webView.setWebViewClient(new WebViewClient());

        webView.loadUrl(url);
        //webView.loadUrl("javascript:void(Tawk_API.toggle())");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    public class ChromeWebClient extends WebChromeClient {

        public ChromeWebClient(){

        }

        @SuppressLint("NewApi")
        @Override
        public void onProgressChanged(WebView view, int newProgress) {

            progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(newProgress);

            if (newProgress == 100){
                progressBar.setVisibility(View.GONE);
            }

            super.onProgressChanged(view, newProgress);
        }
    }

}
