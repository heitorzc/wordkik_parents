package com.wordkik.activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.fragments.AlertsFragment;
import com.wordkik.fragments.ChildProfilesFragment;
import com.wordkik.fragments.PurchaseFragment;
import com.wordkik.objects.Parent;
import com.wordkik.objects.ResponseParent;
import com.wordkik.preferences.SettingsActivity;
import com.wordkik.tasks.AmazonS3Uploader;
import com.wordkik.tasks.NotificationManager;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;
import com.wordkik.utils.AccountManager;
import com.wordkik.utils.ChildManager;
import com.wordkik.views.ConfirmationDialog;
import com.wordkik.views.FeedbackDialog;
import com.wordkik.views.ManageChildDialog;
import com.wordkik.views.ManagePinCodeDialog;

import java.net.URL;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by heitorzc on 02/02/16.
 */
public class MainActivity extends BaseActivity implements ManageChildDialog.ChildDialogListener, AmazonS3Uploader.AmazonS3UploaderListener, ConfirmationDialog.ConfirmationDialogInterface, ManagePinCodeDialog.PinCodeDialogInterface, NavigationView.OnNavigationItemSelectedListener, TaskManager.TaskListener {
    @Bind(R.id.nav_view)              NavigationView navigationView;
    @Bind(R.id.drawer_layout)         DrawerLayout drawer;
    @Bind(R.id.adView)                AdView adView;

    public static final String TAG                    = "MainActivity";

    public static boolean limit_dialog_upgrade        = false;
    public static boolean subscription_purchased      = false;

    public static Toolbar toolbar;
    CircularImageView ivPhoto;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (Constants.isLoggedIn) {

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, new ChildProfilesFragment(), "childProfiles").commit();

            loadDrawerMenu();

            WordKik.prefs.edit().putInt("launch_count", WordKik.prefs.getInt("launch_count", 0) + 1).apply();

            getApplicationContext().mpSetProfile(Constants.user_id, Constants.user_name + " " + Constants.user_lastName, Constants.user_email);

            new NotificationManager(this).getAlerts(null);

            adManager.requestBannerAd(adView);

            AccountManager.getInstance(this).setAccountData(this, Constants.user_account_type, Constants.user_expiration);
        }
        else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

    }

    public void loadDrawerMenu(){
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        loadNavigationView(navigationView.getHeaderView(0));
    }

    public void loadNavigationView(View v){
        TextView accInfo       = (TextView)   v.findViewById(R.id.accInfo);
        TextView tvUserEmail   = (TextView)   v.findViewById(R.id.tvEmail);
        TextView tvUserName    = (TextView)   v.findViewById(R.id.tvUserName);
        ivPhoto = (CircularImageView) v.findViewById(R.id.ivPhoto);

        tvUserEmail.setText(Constants.user_email);
        tvUserName.setText(Constants.user_name + " " + Constants.user_lastName);
        accInfo.setText(Constants.getAccountTypeString(this, Constants.user_account_type));

        Picasso.with(this).load(Constants.user_photo)
                .error(R.drawable.add_photo_white)
                .placeholder(R.drawable.add_photo_white).into(ivPhoto);

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WordKik.callPhotoPicker(MainActivity.this, String.valueOf(Constants.user_id), ivPhoto, MainActivity.this);
            }
        });

        this.setNavigationView(navigationView);

        //updateAlertIcon();
    }

    /*private void updateAlertIcon(){
        if (getNavigationView() != null) {
            if (Constants.hasNewAlerts){
                getNavigationView().getMenu().getItem(2).setIcon(R.drawable.);
            }
            else{
                getNavigationView().getMenu().getItem(2).setIcon(R.drawable.alert_section_icon);
            }
        }
    }*/

    public void checkRating(){
       final boolean showDialog = WordKik.prefs.getBoolean("show_rating_dialog", true);

        if (showDialog) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {

                        Thread.sleep(5000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int launch_count = WordKik.prefs.getInt("launch_count", 0);
                                if (launch_count >= 10 && Constants.childs.size() > 0) {
                                    ConfirmationDialog.with(MainActivity.this)
                                            .title(R.string.rating_title)
                                            .message(R.string.rating_msg)
                                            .background(R.color.green)
                                            .icon(R.drawable.star_icon)
                                            .negativeButtonLabel(R.string.maybe_later)
                                            .positiveButtonLabel(R.string.rate_now)
                                            .callback(MainActivity.this)
                                            .show();
                                    WordKik.prefs.edit().putInt("launch_count", 0).apply();
                                    getApplicationContext().mpTrack("Saw Rating Dialog");
                                }
                            }
                        });

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            t.start();
        }
    }

    public void rateApp() {
        getApplicationContext().mpTrack("Accepted Rating WordKik");
        try
        {
            Intent rateIntent = rateIntentForUrl("market://details");
            startActivity(rateIntent);
        }
        catch (ActivityNotFoundException e)
        {
            Intent rateIntent = rateIntentForUrl("http://play.google.com/store/apps/details");
            startActivity(rateIntent);
        }
    }

    private Intent rateIntentForUrl(String url) {

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, getPackageName())));
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;

        if (Build.VERSION.SDK_INT >= 21){
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        }
        else {
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }

        intent.addFlags(flags);
        return intent;

    }

    @Override
    protected void onDestroy() {
        Constants.isLoggedIn = false;
        super.onDestroy();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        checkRating();
        //updateAlertIcon();

        if (!Constants.isLoggedIn){
            finish();
            startActivity(new Intent(this, LoginActivity.class));
            return;
        }

        if (Constants.user_pincode == null || Constants.user_pincode.trim().length() == 0){
            AccountManager.getInstance(this).showNoPinCodeDialog(R.string.no_pin_code);
            return;
        }

        if (limit_dialog_upgrade){
            onPositiveButtonClick("PURCHASE");
            limit_dialog_upgrade = false;
            return;
        }

        if (subscription_purchased){
            ConfirmationDialog.with(this)
                    .title(R.string.purchase_dialog_title)
                    .titleColor(R.color.Grey)
                    .message(R.string.purchase_completed_msg)
                    .background(R.color.lightestGrey)
                    .icon(R.drawable.love_icon).show();

            subscription_purchased = false;
            return;
        }

        handleNewIntent();

    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            if (!Constants.hasChildren()) {
                ChildManager.getInstance(this).showCancelFeedbackDialog(this);
                return;
            }

            super.onBackPressed();
        }

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        Fragment fragment = null;
        String TAG = "";

            if (id == R.id.nav_children) {
                fragment = new ChildProfilesFragment();
                TAG = "childProfiles";
            } else if (id == R.id.nav_add_child) {
                fragment = new ChildProfilesFragment();
                Bundle bundle = new Bundle();
                bundle.putString("intent", "add_child");
                fragment.setArguments(bundle);
                TAG = "childProfiles";
            } else if (id == R.id.nav_alerts) {
                if (shouldLoadSection()) {fragment = new AlertsFragment();}
                TAG = "alerts";
            } else if (id == R.id.nav_purchase) {
                fragment = new PurchaseFragment();
                TAG = "purchase";
            } else if (id == R.id.nav_feedback) {
                new FeedbackDialog(this).show();
            } else if (id == R.id.nav_support) {
                startActivity(new Intent(this, WebViewActivity.class).putExtra("url", "http://www.wordkik.com/support"));
            } else if (id == R.id.nav_settings) {
                startActivity(new Intent(this, SettingsActivity.class).putExtra(SettingsActivity.EXTRA_SHOW_FRAGMENT, SettingsActivity.GeneralPreferenceFragment.class.getName())
                        .putExtra(SettingsActivity.EXTRA_NO_HEADERS, true));
        }


        if (fragment != null) {

            adManager.showAd();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment, TAG).commit();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        setTitle(item.getTitle());
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }


    public void reloadChildrenSection(){
        onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_children));
    }


    public boolean shouldLoadSection(){
        if (Constants.childs.size() == 0){

            ConfirmationDialog.with(this)
                    .title(R.string.no_child_dialog_title)
                    .message(R.string.no_child_dialog_body)
                    .background(R.color.red)
                    .gravity(Gravity.LEFT)
                    .show();

            setNavigationItemSelected(R.id.nav_children);

            return false;

        }
        else if (!Constants.isSubscriptionValid){

            ConfirmationDialog.with(this)
                    .title(R.string.subscription_title)
                    .message(R.string.features_paying)
                    .background(R.color.green)
                    .gravity(Gravity.LEFT)
                    .negativeButtonLabel(R.string.maybe_later)
                    .positiveButtonLabel(R.string.lets_do_this)
                    .callback(this)
                    .tag("PURCHASE")
                    .icon(R.drawable.calendar)
                    .show();

            return false;

        }
        else {
            return true;
        }

    }


    public void handleNewIntent(){

        String intent = (getIntent().getStringExtra("intent") != null) ? getIntent().getStringExtra("intent") : Constants.NOTIFICATION_CAT_GENERAL;

        if (intent.equals(Constants.NOTIFICATION_CAT_GEOFENCE)){
            startActivity(new Intent(this, ChildLocationActivity.class).putExtra("intent", intent));
            getIntent().removeExtra("intent");
        }
        else if (intent.equals(Constants.NOTIFICATION_CAT_GENERAL)){
            getIntent().removeExtra("intent");
        }
    }


    public void setNavigationItemSelected(int id){
        navigationView.getMenu().findItem(id).setChecked(true);
    }


    @Override
    protected void onResume() {
        loadNavigationView(navigationView.getHeaderView(0));
        AccountManager.getInstance(this).onResume(this);

        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    public void onDialogDismissed() {
        super.onBackPressed();
    }


    @Override
    public void onPincodeRegistered(String pincode) {
        Constants.user_pincode = pincode;
        Toast.makeText(this, R.string.pin_code_created, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPositiveButtonClick(String tag) {
        if (tag.equals("PURCHASE")){
            onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_purchase));
            navigationView.getMenu().findItem(R.id.nav_purchase).setChecked(true);
        }
        else{
            rateApp();
            WordKik.prefs.edit().putBoolean("show_rating_dialog", false).apply();
        }

    }

    @Override
    public void setPictureURL(URL url, String url2) {
        if (url2 != null) {
            Constants.user_photo = url2;

            Picasso.with(this).load(Constants.user_photo)
                    .error(R.drawable.add_photo_white)
                    .placeholder(R.drawable.add_photo_white).into(ivPhoto);

            Parent parent = new Parent();
            parent.setPhoto(url2);

            new ParentTask(this, this).edit(parent);
        }
    }

    @Override
    public void performTask(Object responseObject, String methodName) {
        ResponseParent responseParent = (ResponseParent) responseObject;

        if (responseParent.isSuccess()){
            Log.w("UpdatePhoto", "OK");
        }
    }


}
