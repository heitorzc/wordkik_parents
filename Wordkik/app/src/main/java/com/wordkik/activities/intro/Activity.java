package com.wordkik.activities.intro;

import android.content.Intent;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro2;
import com.wordkik.R;
import com.wordkik.activities.LoginActivity;
import com.wordkik.utils.ResourceManager;

/**
 * Created by heitorzc on 16/04/16.
 */
public class Activity extends AppIntro2 {

    // Please DO NOT override onCreate. Use init.
    @Override
    public void init(Bundle savedInstanceState) {

        addSlide(new FirstFragment());
        addSlide(new SecondFragment());
        addSlide(new ThirdFragment());
        addSlide(new FourthFragment());

        setProgressButtonEnabled(true);

        getSupportActionBar().hide();
        showStatusBar(false);

        setVibrate(true);
        setVibrateIntensity(20);

        setIndicatorColor(ResourceManager.getInstance().color(R.color.lightestGrey), ResourceManager.getInstance().color(R.color.lightGrey));

    }

    @Override
    public void onDonePressed() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void onSlideChanged() {

    }

    @Override
    public void onNextPressed() {

    }
}

