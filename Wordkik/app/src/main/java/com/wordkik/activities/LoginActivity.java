package com.wordkik.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.jiongbull.jlog.JLog;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.activities.intro.Activity;
import com.wordkik.fragments.LoginStepOneFragment;
import com.wordkik.fragments.LoginStepTwoFragment;
import com.wordkik.objects.Parent;
import com.wordkik.objects.ResponseChild;
import com.wordkik.objects.ResponseParent;
import com.wordkik.tasks.ChildTask;
import com.wordkik.tasks.EncryptionManager;
import com.wordkik.tasks.GetPushyToken;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;
import com.wordkik.views.ConfirmationDialog;

import java.io.File;
import java.io.IOException;
import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;


/**
 * Activity to login
 *
 * @author Denis Heringer
 * @version 1.0.0
 */
public class LoginActivity extends BaseActivity implements ParentTask.TaskListener, EncryptionManager.EncryptionListener, TaskManager.IMethodName, GetPushyToken.PushyTokenInterface {
    @BindString(R.string.encrypt_key)   String ENCRYPT_KEY;
    @BindString(R.string.encrypt_salt)   String ENCRYPT_SALT;
    @BindString(R.string.error)                              String error;
    @Bind(R.id.toolbar)                               Toolbar toolbar;
    @Bind(R.id.loading)                               LinearLayout loading;

    private Parent parentUser;
    String userEmail;
    String userPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen_activity);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().hide();
        deleteUploadedLogFiles();

        showIntro();

        // CHECK IF WORDKIK BROWSER IS INSTALLED
        if (isWordkikBrowserInstalled()) {
            goToLoginStepOne();
            JLog.w("isBrowserInstalled Called");
        } else {
            // TRY TO READ THE CREDENTIALS STORED
            readCredentials();
        }
    }


    public Parent getParentUser() {
        return parentUser;
    }


    public void setParentUser(Parent parentUser) {
        this.parentUser = parentUser;
    }


    public void readCredentials() {

        parentUser = new Parent();

        userEmail     = WordKik.prefs.getString(Constants.USER_EMAIL_KEY, null);
        userPassword  = WordKik.prefs.getString(Constants.USER_PASS_KEY, null);
        JLog.w("Read credentials " + " Email: " + userEmail + " Senha: " + userPassword);

        if (userEmail != null && userPassword != null) {

            new GetPushyToken(this).execute();
            JLog.w("Found credentials");

        }
        else {
            // USER DOES NOT HAVE CREDENTIALS STORED, GO TO STEP ONE
            JLog.w("Credentials not found");
            goToLoginStepOne();
        }

    }

    public void login() {

        loading.setVisibility(View.VISIBLE);
        // IF USER HAS NOT INTERNET GO TO THE LOGIN PAGE TO PUT THE EMAIL AND PASSWORD
        if (!WordKik.hasInternet) {
            goToLoginStepTwo();

            Toast.makeText(this, R.string.emNoInternetConnection, Toast.LENGTH_SHORT).show();
            return;
        }

        // IF USER HAS INTERNET TRY TO LOGIN THE USER - CALL API METHOD
        new ParentTask(this, this).login(parentUser);

    }

    public void showIntro(){
        boolean isFirstStart = WordKik.prefs.getBoolean("firstStart", true);

        if (isFirstStart) {
            Intent i = new Intent(LoginActivity.this, Activity.class);
            startActivity(i);

            WordKik.prefs.edit().putBoolean("firstStart", false).apply();

        }
    }


    private void deleteUploadedLogFiles() {

        File path = new File(Environment.getExternalStorageDirectory(), "WordKik_Logs");

        if (path.exists()) {
            String deleteCmd = "rm -r " + path;
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec(deleteCmd);
            } catch (IOException e) {

            }
        }

    }


    @Override
    public void performTask(Object responseObject, String methodName) {
        JLog.w("Perform Taks - METHOD: " + methodName + " CLASS: " + LoginActivity.class);

        switch (methodName) {
            case LOGIN:
                performLogin(responseObject);
                break;
            case GET_CHILDREN:
                performGetChildren(responseObject);
                break;
            default:
                JLog.w("Perform Taks", "Method not implemented");
                break;
        }
    }


    private void performLogin(final Object responseObject) {
        Log.w("LOGIN", "Success!");
        ResponseParent responseParent = (ResponseParent) responseObject;

        if (responseParent.isSuccess()) {
            // JUST SET LOGGED IN AFTER RESPONSE SUCCESS
            Constants.isLoggedIn = true;

            setParentUser(responseParent.getParent());
            Constants.setContantsParentFields(responseParent.getParent());
            Constants.user_photo = null;

            WordKik.prefs.edit().putBoolean("showUpdateDialog",  true).apply();
            WordKik.prefs.edit().putBoolean("showProfileDialog", true).apply();
            WordKik.prefs.edit().putBoolean("shouldShowAd",      true).apply();

            new ChildTask(this, this).getChildren();

        } else {
            goToLoginStepTwo();

            ConfirmationDialog.with(LoginActivity.this)
                    .message(ConfirmationDialog.PASSWORD_INCORRECT)
                    .background(R.color.red)
                    .show();
        }
    }


    private void performGetChildren(Object responseObject){
        ResponseChild responseChild = (ResponseChild) responseObject;

        if (responseChild.isSuccess()) {
            Constants.childs = ((ResponseChild) responseObject).getChildren();
        }

        handleNewIntent();
        finish();
    }


    @Override
    public void onPushyTokenReady(String token) {
        parentUser.setEmail(userEmail);
        parentUser.setRegistration_id(token);
        new EncryptionManager(this, userPassword, ENCRYPT_KEY, ENCRYPT_SALT, this, EncryptionManager.DECRYPT_PASSWORD).execute();
    }


    @Override
    public void setSafeString(String safeString) {
        parentUser.setPassword(safeString);
        login();
    }


    private boolean isWordkikBrowserInstalled() {
        PackageManager packageManager = this.getPackageManager();
        List<PackageInfo> packageInfoList = packageManager.getInstalledPackages(PackageManager.GET_META_DATA);

        for (PackageInfo packageInfo : packageInfoList) {
            if (packageInfo.packageName.equals(Constants.WORDKIK_BROWSER_PACKAGE_NAME)) {
                Constants.shouldNotProceed = true;
                return true;
            }
        }

        Constants.shouldNotProceed = false;
        return false;
    }


    public void goToLoginStepOne() {
        loading.setVisibility(View.INVISIBLE);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.rlContent, new LoginStepOneFragment())
                .addToBackStack(null)
                .commit();
    }


    public void goToLoginStepTwo() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.rlContent, new LoginStepTwoFragment())
                .addToBackStack(null)
                .commit();
    }


    public void handleNewIntent(){
        String intent = (getIntent().getStringExtra("intent") != null) ? getIntent().getStringExtra("intent") : Constants.NOTIFICATION_CAT_GENERAL;
        startActivity(new Intent(this, MainActivity.class).putExtra("intent", intent));
    }


    @Override
    protected void onResume() {

        Parent parent = Constants.registeredParent;

        if (parent != null && parent.getEmail() != null && !parent.getEmail().trim().equals("")) {

            setParentUser(parent);
            goToLoginStepTwo();

        }

        super.onResume();

    }

}