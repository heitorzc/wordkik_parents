package com.wordkik.activities;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.fragments.ChildProfilesFragment;
import com.wordkik.objects.Child;
import com.wordkik.objects.ResponseChild;
import com.wordkik.tasks.ChildTask;
import com.wordkik.tasks.TaskManager;
import com.wordkik.utils.ChildManager;
import com.wordkik.utils.MetricManager;
import com.wordkik.utils.ResourceManager;
import com.wordkik.views.ConfirmationDialog;
import com.wordkik.views.ParallaxScrollView;
import com.wordkik.views.RemoveChildDialog;

import butterknife.Bind;
import butterknife.BindColor;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author heitorzc
 * @version 1.0.0
 */
public class ChildFeatures extends BaseActivity implements ConfirmationDialog.ConfirmDeleteChild, TaskManager.TaskListener, TaskManager.IMethodName{
    @BindColor(R.color.darkRed)   int darkRed;
    @BindString(R.string.name_device)  String name_device;
    @BindString(R.string.language)  String language;
    @Bind(R.id.toolbar)    Toolbar toolbar;
    @Bind(R.id.tvName)     TextView tvName;
    @Bind(R.id.ivPhoto)    ImageView ivPhoto;
    @Bind(R.id.scroll)     ParallaxScrollView scroll;


    @OnClick(R.id.tvEditProfile)
    public void onClickEditProfile(){
        new ChildManager(this).edit(child);
    }

    @OnClick(R.id.btTimeLock)
    public void onClickTimeLock(){
        startActivity(new Intent(this, ChildTimeLocksActivity.class).putExtra("selected", child));
    }

    @OnClick(R.id.btScreenLimit)
    public void onClickScreenLimit(){
        startActivity(new Intent(this, ChildScreenLimit.class).putExtra("selected", child));
    }

    @OnClick(R.id.btAppUsage)
    public void onClickAppUsage(){
        startActivity(new Intent(this, ChildLockedAppsActivity.class).putExtra("selected", child));
    }

    @OnClick(R.id.btCurrentLocation)
    public void onClickCurrentLocation(){
        startActivity(new Intent(this, ChildLocationActivity.class).putExtra("selected", child).putExtra("intent", ChildLocationActivity.INTENT_LOCATE_CHILD));
    }

    @OnClick(R.id.btGeofence)
    public void onClickGeofence(){
        startActivity(new Intent(this, ChildLocationActivity.class).putExtra("selected", child).putExtra("intent", ChildLocationActivity.INTENT_ENABLE_GEOFENCE_MODE));
    }

    @OnClick(R.id.bbtGeoActivities)
    public void onClickGeoActivities(){
        startActivity(new Intent(this, ChildLocationActivity.class).putExtra("selected", child).putExtra("intent", ChildLocationActivity.INTENT_SHOW_GEOACTIVITIES));
    }

    @OnClick(R.id.btStatistics)
    public void onClickStatistics(){
        startActivity(new Intent(this, ChildStatisticsActivity.class).putExtra("selected", child));
    }

    @OnClick(R.id.btDeleteChild)
    public void onClickDeleteChild(){
        ConfirmationDialog.with(this)
                .title(child.getName())
                .message(getString(R.string.remove_child_dialog_message).replace("%", child.getName()))
                .background(R.color.red)
                .icon(child.getPhoto())
                .child(child)
                .circleIcon(true)
                .negativeButtonLabel(R.string.no)
                .positiveButtonLabel(R.string.yes)
                .callback(this)
                .show();
    }

    private static final String TAG = "Child Features";
    private ColorDrawable toolbarColor;
    private Child child;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        setContentView(R.layout.child_features);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        setStatusBarColor(Color.TRANSPARENT);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adManager.showAd();

        toolbarColor = new ColorDrawable(ResourceManager.getInstance().color(R.color.colorPrimary));
        toolbarColor.setAlpha(0);

        getSupportActionBar().setBackgroundDrawable(toolbarColor);

        getScreenSize();

        child = (Child) getIntent().getSerializableExtra("child");
        setChildData(child);

        scroll.setOnScrollViewListener(new ParallaxScrollView.OnScrollViewListener() {


            @Override
            public void onScrollchanged(int l, int t, int oldl, int oldt) {
                int scrollY = scroll.getScrollY();
                toolbarColor.setAlpha(getAlphaforActionBar(scrollY));
                setStatusBarColor(Color.argb(getAlphaforActionBar(scrollY), 173, 35, 59));

                if (scrollY >= 155){
                    getSupportActionBar().setTitle("Manage Device");
                }
                else {
                    getSupportActionBar().setTitle("");
                }

            }

            private int getAlphaforActionBar(int scrollY) {
                int minDist = 0, maxDist = 300;

                if (scrollY > maxDist) {
                    return 255;
                } else if (scrollY < minDist) {
                    return 0;
                } else {
                    int alpha = 0;
                    alpha = (int) ((255.0 / maxDist) * scrollY);
                    return alpha;
                }
            }
        });

    }


    private void setStatusBarColor(int color){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(color);
        }
    }


    public void getScreenSize(){
        if (Constants.SDK <= 13){
            Constants.screen_width = (getWindowManager().getDefaultDisplay().getWidth());
            Constants.screen_height = (getWindowManager().getDefaultDisplay().getHeight());
        }
        else {
            Point size = new Point();
            getWindowManager().getDefaultDisplay().getSize(size);
            Constants.screen_width = (size.x);
            Constants.screen_height = (size.y);
        }
    }


    public void setChildData(Child child){
        tvName.setText(formatNameString(child.getName()));

        if (child.getPhoto() != null) {
            Picasso.with(this).load(child.getPhoto()).into(ivPhoto);
        }
        else{
            Picasso.with(this).load(R.drawable.profile_placeholder).into(ivPhoto);
        }

    }


    private String formatNameString(String child_name){

        if (language.equals("pt")){
            String gen = (child.getGender().equals("Boy")) ? "do" : "da";
            String s = (child.getDevice().equals("phone")) ? "Celular" : "Tablet";
            return name_device.replace("$device", s).replace("$gen", gen).replace("$name", child_name);
        }
        else {
            return (!child_name.substring(child_name.length() - 1).equals("s")) ? child_name + "'s " + child.getDevice() : child_name + "' " + child.getDevice();
        }

    }


    @Override
    protected void onPostResume() {
        super.onPostResume();

        if (MainActivity.limit_dialog_upgrade){
            finish();
        }

        MetricManager.with(this).createNewMetric(TAG);

    }


    @Override
    protected void onPause() {
        MetricManager.getInstance().setTimeLeaving();
        super.onPause();
    }


    @Override
    public void onConfirmDeleteChild(Child child) {
        new ChildTask(this, this).removeChild(child);
    }


    @Override
    public void performTask(Object responseObject, String methodName) {
        Log.e("PERFORM_TASK", "METHOD: " + methodName + " CLASS: " + RemoveChildDialog.class);

        switch (methodName) {
            case REMOVE_CHILD:
                performRemoveChild(responseObject);
                break;
            default:
                Log.e("PERFORM_TASK", "METHOD NOT IMPLEMENTED");
                break;
        }
    }


    private void performRemoveChild(Object responseObject) {
        ResponseChild responseChild = (ResponseChild) responseObject;

        if (responseChild.isSuccess()) {

            Constants.childs.remove(Constants.getChildById(child.getId()));
            ChildProfilesFragment.hasEditedChild = true;

            finish();
        }

    }


    @Override
    public void onBackPressed() {
        supportFinishAfterTransition();
    }
}