package com.wordkik.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.wordkik.WordKik;
import com.wordkik.services.SendMetrics;
import com.wordkik.utils.AdManager;

import java.io.File;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.tajchert.nammu.Nammu;

/**
 * Base activity
 * Activities common stuff
 * Every activity should extend this
 *
 * @author Denis Heringer
 * @version 1.0.0
 */
public class BaseActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {

    public static NavigationView navigationView;
    AdManager adManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        adManager = new AdManager(this);
        adManager.requestInterstitialAd();
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onPause() {
        getApplicationContext().mpFlush();
        startService(new Intent(this, SendMetrics.class));
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                FragmentManager fm = getSupportFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    Log.i("MainActivity", "popping backstack");
                    fm.popBackStack();
                } else {
                    Log.i("MainActivity", "nothing on backstack, calling super");
                    super.onBackPressed();
                }
                break;
        }

        return true;
    }

    @Override
    public void onBackStackChanged() {
        Log.e("changed", "changed");
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        if (PurchaseActivity.bp != null){
            PurchaseActivity.bp.handleActivityResult(requestCode, resultCode, data);
        }
            super.onActivityResult(requestCode, resultCode, data);

            EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
                @Override
                public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                    Log.d("Photo", e.getMessage().toString());
                    WordKik.amazonS3UploaderListener.setPictureURL(null, null);
                }

                @Override
                public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                    startActivity(new Intent(BaseActivity.this, CropPhoto.class).putExtra("newPhoto", imageFile.getAbsolutePath())
                            .putExtra("file_name", WordKik.file_name));

                }

                @Override
                public void onCanceled(EasyImage.ImageSource source, int type) {
                    WordKik.amazonS3UploaderListener.setPictureURL(null, null);
                }


            });

        WordKik.saveLogcatToFile("BASEACT_96");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void setNavigationView(NavigationView navigationView){
        this.navigationView = navigationView;
    }

    public WordKik getApplicationContext(){
        return (WordKik) super.getApplicationContext();
    }

    public void runOnNewThread(Runnable runnable){
        new Thread(runnable).start();
    }

    public static NavigationView getNavigationView(){
        return navigationView;
    }
}