package com.wordkik.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.fragments.PurchasePaymentFragment;
import com.wordkik.objects.Plan;
import com.wordkik.objects.PurchaseData;
import com.wordkik.objects.ResponsePurchase;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;
import com.wordkik.utils.AccountManager;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;

/**
 * Created by heitorzc on 22/04/16.
 */
public class PurchaseActivity extends BaseActivity implements BillingProcessor.IBillingHandler, TaskManager.TaskListener{
    @BindString(R.string.google_billing_api_key) String GOOGLE_BILLING_API_KEY;
    @Bind(R.id.toolbar)           Toolbar toolbar;

    PurchasePaymentFragment paymentFragment = new PurchasePaymentFragment();

    static BillingProcessor bp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.purchase_activity);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Plan selected_plan = getIntent().getParcelableExtra("selected_plan");

        Bundle bundle = new Bundle();
        bundle.putParcelable("selected_plan", selected_plan);

        paymentFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, paymentFragment, "payment").addToBackStack(null).commit();

        bp = new BillingProcessor(this, GOOGLE_BILLING_API_KEY, this);

    }

    public static void subscribe(Activity activity, String id){
        bp.subscribe(activity, id);
    }

    @Override
    protected void onDestroy() {
        if (bp != null)
            bp.release();
        super.onDestroy();
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {

        PurchaseData purchaseData = new PurchaseData(productId, details.purchaseToken, details.orderId);
        new ParentTask(this, this).sendSubscription(purchaseData);

        Log.w("INAPP", "Purchase Token: "         + details.purchaseToken);
        Log.w("INAPP", "Purchase Order ID: "      + details.orderId);
        Log.w("INAPP", "Purchase Response Data: " + details.purchaseInfo.responseData);
        Log.w("INAPP", "Purchase Signature: "     + details.purchaseInfo.signature);
        Log.w("INAPP", "Purchase PurchaseInfo: "  + details.purchaseInfo.toString());

        getApplicationContext().mpTrack("Google Play Payment Completed");

        WordKik.saveLogcatToFile("PRCH_83");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onPurchaseHistoryRestored() {
        Log.w("INAPP", "Billing History Restored");
    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
        getApplicationContext().mpTrack("Purchased Error or Canceled");
        Log.w("INAPP", "Billing Error: " + errorCode);
    }

    @Override
    public void onBillingInitialized() {
        Log.w("INAPP", "Billing Initialized");
    }

    @Override
    public void performTask(Object responseObject, String methodName) {
        ResponsePurchase responsePurchase = (ResponsePurchase) responseObject;

        if (responsePurchase.isSuccess()){
            Constants.user_account_type = AccountManager.ACCOUNT_PREMIUM;
            Constants.user_expiration = responsePurchase.getSubscription_left();
            Constants.isSubscriptionValid = true;
            AccountManager.getInstance(this).updateAccountInfo();

            MainActivity.subscription_purchased = true;
            finish();
        }
    }
}
