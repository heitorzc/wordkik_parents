package com.wordkik.activities;


import android.content.Context;
import android.graphics.PointF;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.squareup.picasso.Downloader;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.objects.Child;
import com.wordkik.objects.ResponseScreenTime;
import com.wordkik.objects.ScreenTime;
import com.wordkik.tasks.ChildTask;
import com.wordkik.tasks.TaskManager;
import com.wordkik.utils.AccountManager;
import com.wordkik.utils.ChildManager;
import com.wordkik.utils.MetricManager;
import com.wordkik.views.OpenSansLightTextView;
import com.wordkik.views.SeekBarHint;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author heitorzc
 * @version 1.0.0
 */
public class ChildScreenLimit extends BaseActivity implements TaskManager.TaskListener{
    @Bind(R.id.toolbar)          Toolbar toolbar;
    @Bind(R.id.sbWeekdays)       SeekBarHint sbWeekdays;
    @Bind(R.id.sbWeekends)       SeekBarHint sbWeekends;
    @Bind(R.id.tvWeekdaysBubble) TextView tvWeekdaysBubble;
    @Bind(R.id.tvWeekendBubble)  TextView tvWeekendsBubble;

    private final String TAG = "Screen Time Limit";

    private Child child;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppThemePurple);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_time_activity);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        child = (Child) getIntent().getSerializableExtra("selected");
        setTitle(child.getName());


        //TODO: Figure out why the skeeBar doesn't show the bubble if we only set the data
        //TODO: in the listener.
        ScreenTime wd = new ScreenTime(0, "weekdays", "Unlimited");
        ScreenTime we = new ScreenTime(1, "weekends", "Unlimited");

        ArrayList<ScreenTime> limits = new ArrayList<>();

        limits.add(wd);
        limits.add(we);

        child.setDaily_usages(limits);

        sbWeekdays.configureForScreenTime(child, tvWeekdaysBubble, true);
        sbWeekends.configureForScreenTime(child, tvWeekendsBubble, false);

        new ChildTask(this, this).getScreenTimeLimits(child);

    }



    @Override
    protected void onResume() {
        MetricManager.with(this).createNewMetric(TAG);
        AccountManager.getInstance(this).onResume(this);
        getApplicationContext().mpTrack(TAG);
        super.onResume();

    }

    @Override
    protected void onPause() {
        MetricManager.getInstance().setTimeLeaving();
        super.onPause();

    }

    @Override
    public void performTask(Object responseObject, String methodName) {

        ResponseScreenTime responseScreenTime = (ResponseScreenTime) responseObject;

        if (responseScreenTime.isSuccess()){

            ArrayList<ScreenTime> limits = responseScreenTime.getChild().getDaily_usages();

            if (limits.size() > 0) child.setDaily_usages(limits);

            sbWeekdays.configureForScreenTime(child, tvWeekdaysBubble, true);
            sbWeekends.configureForScreenTime(child, tvWeekendsBubble, false);

        }

    }
}