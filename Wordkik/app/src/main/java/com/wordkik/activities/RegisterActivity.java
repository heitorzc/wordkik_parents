package com.wordkik.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;

import com.wordkik.R;
import com.wordkik.fragments.RegisterStepOneFragment;
import com.wordkik.fragments.RegisterStepThreeFragment;
import com.wordkik.objects.Parent;

/**
 * Activity to register a new user
 *
 * @author Denis Heringer
 * @version 1.0.0
 */
public class RegisterActivity extends BaseActivity {

    private static final int IMAGE_PICKER_SELECT = 999;

    private Parent parentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.register_screen_activity);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(this);
        fragmentManager.beginTransaction()
                        .add(R.id.rlContent,
                        new RegisterStepOneFragment())
                        .commit();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.rlContent);

        if (fragment instanceof RegisterStepThreeFragment) {
            finish();
            return;
        }

        super.onBackPressed();
    }

    public Parent getParentUser() {
        return parentUser;
    }

    public void setParentUser(Parent parentUser) {
        this.parentUser = parentUser;
    }

}