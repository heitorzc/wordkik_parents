package com.wordkik.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.wordkik.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by heitorzc on 01/09/16.
 */
public class CalendarActivity extends AppCompatActivity {

    Calendar nextDay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_activity);

        nextDay = Calendar.getInstance();
        nextDay.add(Calendar.DAY_OF_MONTH, +1);

        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.MONTH, -6);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -7);

        ArrayList<Date> dates = new ArrayList<>();
        dates.add(cal.getTime());
        dates.add(new Date());


       /* CalendarPickerView calendar = (CalendarPickerView) findViewById(R.id.calendar_view);


        calendar.init(lastYear.getTime(), nextDay.getTime())
                .inMode(CalendarPickerView.SelectionMode.RANGE)
                .withSelectedDates(dates);


        List<CalendarCellDecorator> decoratorList = new ArrayList<>();
        decoratorList.add(new MonthDecorator());
        calendar.setDecorators(decoratorList);

        calendar.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {

            }

            @Override
            public void onDateUnselected(Date date) {

            }
        });*/

    }

   /* public class MonthDecorator implements CalendarCellDecorator {

        @Override
        public void decorate(CalendarCellView calendarCellView, Date date) {
            if (calendarCellView.isToday()) {
                if (calendarCellView.isSelected()) {
                    calendarCellView.getDayOfMonthTextView().setTextColor(ResourceManager.getInstance().color(R.color.white));
                }
                else{
                    calendarCellView.getDayOfMonthTextView().setTextColor(ResourceManager.getInstance().color(R.color.calendar_text_active));
                }
            }
            if (!calendarCellView.isCurrentMonth()){
                calendarCellView.getDayOfMonthTextView().setTextColor(ResourceManager.getInstance().color(R.color.white));
            }
        }
    }*/

}
