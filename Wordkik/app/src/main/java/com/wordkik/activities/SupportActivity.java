package com.wordkik.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.wordkik.R;
import com.wordkik.adapters.ChatAdapter;
import com.wordkik.objects.ChatMessage;
import com.wordkik.objects.ResponseChatMessages;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SupportActivity extends AppCompatActivity implements TaskManager.TaskListener, TaskManager.IMethodName{
    @Bind(R.id.toolbar)   Toolbar toolbar;
    @Bind(R.id.chat)      ListView chat;
    @Bind(R.id.message)   EditText etMessage;
    @Bind(R.id.send)      ImageView btSend;

    @OnClick(R.id.send)
    public void onClickSend(){
        chatMessage = etMessage.getText().toString();
        ChatMessage message = new ChatMessage();
        message.setContent(etMessage.getText().toString());

        new ParentTask(this, this).sendChatMessage(message);

        messages.add(message);
        adapter.notifyDataSetChanged();

        etMessage.setText("");
    }

    String chatMessage = "";
    ArrayList<ChatMessage> messages = new ArrayList<>();
    ChatAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.support_activity);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        View header = new View(this);
        View footer = new View(this);
        AbsListView.LayoutParams lp_header = new AbsListView.LayoutParams(80, 80);
        AbsListView.LayoutParams lp_footer = new AbsListView.LayoutParams(80, 80);
        header.setLayoutParams(lp_header);
        footer.setLayoutParams(lp_footer);
        chat.addHeaderView(header);
        chat.addFooterView(footer);

        new ParentTask(this, this).getChatMessages();

        adapter = new ChatAdapter(this, messages);
        chat.setAdapter(adapter);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void performTask(Object responseObject, String methodName) {
        if (methodName.equals(GET_CHAT_MESSAGES)){
            ResponseChatMessages responseChatMessages = (ResponseChatMessages) responseObject;

            if (responseChatMessages.isSuccess()){
                messages.addAll(responseChatMessages.getConversation_messages());
                adapter.notifyDataSetChanged();
            }
        }
    }
}
