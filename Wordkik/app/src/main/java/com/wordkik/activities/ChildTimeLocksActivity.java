package com.wordkik.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.android.gms.ads.AdView;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.adapters.TimeLocksAdapter;
import com.wordkik.objects.Child;
import com.wordkik.objects.Metric;
import com.wordkik.objects.ResponseTimeLock;
import com.wordkik.objects.TimeLock;
import com.wordkik.tasks.ParentTask;
import com.wordkik.utils.AccountManager;
import com.wordkik.utils.MetricManager;
import com.wordkik.utils.ResourceManager;
import com.wordkik.views.EditTimeLockDialog;
import com.wordkik.views.TimeLockActionsDialog;
import com.wordkik.views.TimeLockSetupDialog;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.BindColor;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ChildTimeLocksActivity extends BaseActivity implements AdapterView.OnItemClickListener, TabLayout.OnTabSelectedListener, TimeLockActionsDialog.TimeLockActionsInterface, TimeLockSetupDialog.TimeLockSetupDialogInterface, EditTimeLockDialog.EditTimelockInterface, ParentTask.TaskListener {
    @BindColor(R.color.darkGreen)   int darkGreen;
    @BindColor(R.color.green)       int green;
    @Bind(R.id.toolbar)             Toolbar toolbar;
    @Bind(R.id.tabLayout)           TabLayout tabLayout;
    @Bind(R.id.llNoTimeLocks)       LinearLayout llNoTimeLocks;
    @Bind(R.id.adView)              AdView adView;

    @OnClick(R.id.fab)
    public void onClickFab(){
        if (AccountManager.getInstance(this).checkTimeLocksLimit(child)) {
            new TimeLockSetupDialog(ChildTimeLocksActivity.this, getSupportFragmentManager(), adapter).defineType(tabLayout.getSelectedTabPosition());
        }
    }

    Metric metric;
    final String TAG = "List Time Locks";

    public static Child child;
    public static FragmentManager fragmentManager;
    public static EditTimeLockDialog.EditTimelockInterface editListener;

    TimeLocksAdapter adapter;
    public static ListView lvTimeLocks;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppThemeGreen);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.time_lock_2);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editListener = this;
        fragmentManager = getSupportFragmentManager();

        child = (Child) getIntent().getSerializableExtra("selected");
        setTitle(child.getName());

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.wk_sun)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.wk_mon)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.wk_tue)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.wk_wed)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.wk_thu)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.wk_fri)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.wk_sat)));

        tabLayout.addOnTabSelectedListener(this);

        View header_footer = new View(this);
        AbsListView.LayoutParams lp = new AbsListView.LayoutParams(ResourceManager.getInstance().dpToPx(5), ResourceManager.getInstance().dpToPx(5));

        header_footer.setLayoutParams(lp);

        lvTimeLocks = (ListView) findViewById(R.id.lvTimeLocks);
        lvTimeLocks.addHeaderView(header_footer);
        lvTimeLocks.addFooterView(header_footer);
        lvTimeLocks.setOnItemClickListener(this);

        new ParentTask(this, this).listTimeLocks(child);
        adManager.requestBannerAd(adView);


    }

    public void touggleNoTimeLocks() {
        if (adapter.getItems().size() > 0){
            llNoTimeLocks.setVisibility(View.GONE);
        }
        else{
            llNoTimeLocks.setVisibility(View.VISIBLE);
        }

        Log.w("Toggle", "Size: " + adapter.getItems().size());
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (adapter != null) {
            adapter.filterByWeekDay(tab.getPosition());
            Log.w("WEEKDAY", "  " + tab.getPosition());
        }
        else{
            adapter = new TimeLocksAdapter(this, child);
            lvTimeLocks.setAdapter(adapter);
        }

        touggleNoTimeLocks();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        if (adapter != null) {
            adapter.filterByWeekDay(tab.getPosition());
            touggleNoTimeLocks();
        }
        else {
            adapter = new TimeLocksAdapter(this, child);
            lvTimeLocks.setAdapter(adapter);
        }

        touggleNoTimeLocks();
    }

    @Override
    public void onPostResume() {
        MetricManager.with(this).createNewMetric(TAG);
        AccountManager.getInstance(this).onResume(this);
        getApplicationContext().mpTrack(TAG);
        super.onPostResume();
    }

    @Override
    protected void onPause() {
        MetricManager.getInstance().setTimeLeaving();
        super.onPause();
    }

    @Override
    public void onTimeLockCreated() {
        adapter.filterByWeekDay(tabLayout.getSelectedTabPosition());
        touggleNoTimeLocks();
        getApplicationContext().mpTrack("Created TimeLock");
    }

    @Override
    public void onTimeLockEdited() {
        adapter.filterByWeekDay(tabLayout.getSelectedTabPosition());
        touggleNoTimeLocks();
    }

    @Override
    public void onTimeLockDeleted() {
        adapter.filterByWeekDay(tabLayout.getSelectedTabPosition());
        touggleNoTimeLocks();
    }

    @Override
    public void performTask(Object responseObject, String methodName) {
        ResponseTimeLock responseTimeLock = (ResponseTimeLock) responseObject;

        if (responseTimeLock.isSuccess() && responseTimeLock.getTimelocks() != null){
            child.setTimeLocks(responseTimeLock.getTimelocks());
        }

        adapter = new TimeLocksAdapter(this, child);
        lvTimeLocks.setAdapter(adapter);

        WordKik.cur_cal = Calendar.getInstance();
        tabLayout.getTabAt(WordKik.cur_cal.get(Calendar.DAY_OF_WEEK) - 1).select();

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        new TimeLockActionsDialog(this, (TimeLock) adapter.getItem((int)id)).show();
    }
}
