package com.wordkik.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.isseiaoki.simplecropview.CropImageView;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.tasks.AmazonS3Uploader;
import com.wordkik.WordKik;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import butterknife.BindString;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;


public class CropPhoto extends AppCompatActivity {
    @BindString(R.string.amazon_api_key)        String AMAZON_API_KEY;
    @BindString(R.string.amazon_id)             String AMAZON_ID;
    @BindString(R.string.crop_photo)            String CROP_PHOTO;
    @BindString(R.string.crop_photo_rotate)     String ROTATE;
    @BindString(R.string.crop_photo_cut)        String CROP;


    Bitmap.CompressFormat format = (Constants.SDK < 14) ? Bitmap.CompressFormat.JPEG : Bitmap.CompressFormat.WEBP;

    CropImageView cropImageView;
    File newPhoto = null;
    String file_name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cropphoto);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById (R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CROP_PHOTO);

        newPhoto = new File(getIntent().getStringExtra("newPhoto"));
        file_name = getIntent().getStringExtra("file_name");

        Bitmap b = compressFileToBitmap(newPhoto);

        cropImageView = (CropImageView) findViewById(R.id.cropImageView);
        cropImageView.setImageBitmap(b);
        cropImageView.setFrameStrokeWeightInDp(2);
        cropImageView.setGuideStrokeWeightInDp(1);
        cropImageView.setHandleSizeInDp(14);

    }


    private Bitmap compressFileToBitmap(File file){

        return new Compressor.Builder(this)
                .setMaxWidth(640)
                .setMaxHeight(480)
                .setQuality(70)
                .setCompressFormat(format)
                .build()
                .compressToBitmap(file);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, ROTATE).setIcon(R.drawable.rotate).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        menu.add(1, 1, 1, CROP).setIcon(R.drawable.crop).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                WordKik.changePhoto.setImageBitmap(cropImageView.getCroppedBitmap());
                File newCropedPhoto = new File(newPhoto.getAbsolutePath());

                OutputStream os = null;

                try {
                    os = new BufferedOutputStream(new FileOutputStream(newCropedPhoto));
                    cropImageView.getCroppedBitmap().compress(Bitmap.CompressFormat.JPEG, 70, os);
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                new AmazonS3Uploader(this, CropPhoto.this, AMAZON_ID, AMAZON_API_KEY, file_name, newCropedPhoto, WordKik.amazonS3UploaderListener).execute();

                break;
            case 0:
                cropImageView.rotateImage(CropImageView.RotateDegrees.ROTATE_90D);
                break;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        WordKik.amazonS3UploaderListener.setPictureURL(null, null);
        super.onBackPressed();
    }
}

