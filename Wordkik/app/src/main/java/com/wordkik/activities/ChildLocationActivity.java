package com.wordkik.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.google.android.gms.ads.AdView;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.nineoldandroids.animation.ObjectAnimator;
import com.scalified.fab.ActionButton;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.adapters.GeoActivitiesAdapter;
import com.wordkik.broadcastReceivers.PushReceiver;
import com.wordkik.objects.Child;
import com.wordkik.objects.Notification;
import com.wordkik.objects.NotificationData;
import com.wordkik.objects.ParentNotification;
import com.wordkik.objects.ResponseAddGeofence;
import com.wordkik.objects.ResponseDefault;
import com.wordkik.objects.ResponseGeoActivities;
import com.wordkik.objects.ResponseListGeofences;
import com.wordkik.objects.UserGeofence;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;
import com.wordkik.utils.AccountManager;
import com.wordkik.utils.Animate;
import com.wordkik.utils.AnimateFeedback;
import com.wordkik.utils.MetricManager;
import com.wordkik.utils.ResourceManager;
import com.wordkik.views.ConfirmationDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;

public class ChildLocationActivity extends BaseActivity implements Animate.AnimateInterface, OnMapReadyCallback, TaskManager.TaskListener, TaskManager.IMethodName, MapboxMap.OnMapClickListener{
    @BindString(R.string.fetching)         String fetching;
    @BindString(R.string.gps_off_msg)      String gps_off_msg;
    @Bind(R.id.rlTimeLine)                 RelativeLayout rlTimeLine;
    @Bind(R.id.tvDate)                     TextView tvDate;
    @Bind(R.id.rootView)                   RelativeLayout rootView;
    @Bind(R.id.lvTimeLine)                 ListView lvTimeLine;
    @Bind(R.id.topShadow)                  View topShadow;
    @Bind(R.id.toolbar)                    Toolbar toolbar;
    @Bind(R.id.adView)                     AdView adView;


    @OnClick(R.id.btFetch)
    public void onClickFetch(){
        requestChildLocation(child);
    }



    /**********************************************************************************************
     ******************************** End of Butterknife block ************************************
     **********************************************************************************************/


    public static final String INTENT_SHOW_GEOACTIVITIES   = "geofence_activity";
    public static final String INTENT_ENABLE_GEOFENCE_MODE = "geofence_mode";
    public static final String INTENT_LOCATE_CHILD         = "locate_child";


    static Bundle savedInstanceState;
    static Context mContext;
    static OnMapReadyCallback callback;

    static double latitude = 0;
    static double longitude = 0;

    public static MapView mapView;
    static MapboxMap mapboxMap;
    Marker activeMarker;
    static boolean isMapReady = false;
    static boolean isGeofencesReady = false;
    boolean isTimeLineShown = false;

    static int markerCount = 0;

    private ArrayList<UserGeofence> geofences = new ArrayList<>();
    private static ActionButton btFetch;
    private static Snackbar fetchingMessage;
    private static Snackbar tapGeofenceMessage;
    private static Handler handlerErrorDialog;
    private static Runnable ErrorDialogRunnable;
    private static Child child;
    private UserGeofence userGeofence;
    private boolean isInGeofenceMode = false;
    private String descript = "";
    private String cat = "";

    private final String TAG_SECTION  = "Accessed Location Tracking";
    private final String TAG_GEOFENCE = "Enabled Geofence mode";
    private final String TAG_TIMELINE = "Opened the Timeline View";
    private final String TAG_CREATED_GEOFENCE = "Created a Geofence";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        this.savedInstanceState = savedInstanceState;
        setContentView(R.layout.childs_location_mapbox);
        ButterKnife.bind(this);

        mContext = this;
        callback = this;

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setActionbarTextColor(getSupportActionBar(), ResourceManager.getInstance().color(R.color.brownGrey));

        child = (Child) getIntent().getSerializableExtra("selected");

        btFetch = (ActionButton) findViewById(R.id.btFetch);

        adManager.requestBannerAd(adView);

        loadMapView();
        loadTimeLineView();
    }




    /**
     * Methods responsible for setting up views
     * ****************************************
     */

    private void loadTimeLineView() {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        String currentDate = sdf.format(new Date());

        tvDate.setText(currentDate);

        setHeaderAndFooter(lvTimeLine);

        rlTimeLine.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                rlTimeLine.getViewTreeObserver().removeOnPreDrawListener(this);
                rlTimeLine.setTranslationY(rootView.getHeight());

                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, (int)(rootView.getHeight() - getPositionToTimeLine()));
                rlTimeLine.setLayoutParams(lp);
                return false;
            }
        });


        new ParentTask(this, this).getGeoActivities();

    }

    private void setHeaderAndFooter(ListView list){
        View view = new View(this);

        int margin = ResourceManager.getInstance().dpToPx(35);

        AbsListView.LayoutParams header_footer = new AbsListView.LayoutParams(margin, margin);

        view.setLayoutParams(header_footer);

        list.addFooterView(view);
    }

    private void toggleTimeLineView(){
        if (!isTimeLineShown) {
            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(rlTimeLine, "translationY", rootView.getHeight(), getPositionToTimeLine());
            objectAnimator.setDuration(300);
            objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            objectAnimator.start();
            getApplicationContext().mpTrack(TAG_TIMELINE);
        }
        else{
            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(rlTimeLine, "translationY", getPositionToTimeLine(), rootView.getHeight());
            objectAnimator.setDuration(300);
            objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            objectAnimator.start();
        }

        isTimeLineShown ^= true;

    }

    private float getPositionToTimeLine() {
        Rect myViewRect = new Rect();
        toolbar.getGlobalVisibleRect(myViewRect);
        return myViewRect.bottom;
    }

    public static void loadMapView(){
        mapView = (MapView) ((ChildLocationActivity) mContext).findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(callback);
    }


    private void setActionbarTextColor(ActionBar actBar, int color) {

        String title = actBar.getTitle().toString();
        Spannable spannablerTitle = new SpannableString(title);
        spannablerTitle.setSpan(new ForegroundColorSpan(color), 0, spannablerTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        actBar.setTitle(spannablerTitle);

    }



    private void finishActivity(){
        if (isTimeLineShown) {
            toggleTimeLineView();
        }
        else {
            super.onBackPressed();
        }
    }



    public static void showFetchButton(Activity a){
        a.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                fetchingMessage.dismiss();
                btFetch.show();
            }
        });
    }



    public static void showFetchingMessage(Activity a){
        a.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btFetch.hide();

                fetchingMessage = Snackbar.make(mapView, mContext.getString(R.string.fetching).replace("%", child.getName()), Snackbar.LENGTH_INDEFINITE);
                fetchingMessage.setAction(R.string.cancel_caps, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        cancelErrorRunnable();
                        showFetchButton((ChildLocationActivity) mContext);
                    }
                });

                fetchingMessage.show();
            }
        });
    }




    /**
     * Methods responsible for adding the child on the map
     * ***************************************************
     */

    public static void generateLocationPoint(double lat, double lon){
        latitude = lat;
        longitude = lon;

        if (isMapReady && mapboxMap != null) {
            addMarkerOnMap(lat, lon);
        }
        else {
            loadMapView();
        }


    }

    public static void addMarkerOnMap(double lat, double lon){

        if (lat != 0 && lon != 0) {

            if(mapboxMap != null) {

                try {

                    for (final Marker marker : mapboxMap.getMarkers()){
                        if (marker.getSnippet().equals(child.getId() + "")){

                            ((ChildLocationActivity) mContext).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (marker.isInfoWindowShown()){
                                        marker.hideInfoWindow();
                                    }

                                    mapboxMap.removeMarker(marker);

                                }
                            });

                        }
                    }

                    LatLng location = new LatLng(lat, lon);
                    mapboxMap.addMarker(new MarkerOptions().position(location).title(child.getName()).snippet("" + child.getId()));
                    markerCount++;

                    cancelErrorRunnable();
                    showFetchButton((ChildLocationActivity) mContext);

                    // Animate camera if this is the first marker added.
                    if (markerCount == 1) {
                        CameraPosition cp = new CameraPosition.Builder().zoom(15).target(location).build();
                        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp), 5000);


                        // Otherwise, just move the camera to the new location and keep the zoom configuration set by the user.
                    }
                    else {
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(mapboxMap.getCameraPosition().zoom).build();
                        mapboxMap.moveCamera((CameraUpdateFactory.newCameraPosition(cameraPosition)));
                    }

                }

                catch (NullPointerException e){
                    Log.e("LOCATION", "Error: " + e.getMessage());
                }

            }
        }
        else {
            PushReceiver.acceptLocation = null;
            Log.e("LOCATION", "Received  wrong location.");
        }
    }


    public static void cancelErrorRunnable() {

        PushReceiver.acceptLocation = null;

        if (handlerErrorDialog != null) {
            handlerErrorDialog.removeCallbacks(ErrorDialogRunnable);
            handlerErrorDialog = null;
        }
    }


    public void resetTrackingFunction(){

        markerCount = 0;
        latitude = 0;
        longitude = 0;

        if (mapboxMap != null) mapboxMap.removeAnnotations();
    }

    public void requestChildLocation(Child child){

        PushReceiver.acceptLocation = child;

        Notification notification = new Notification();

        NotificationData notificationData = new NotificationData();
        notificationData.setSource("push");
        notificationData.setNotification_type(Constants.NOTIFICATION_TYPE_REQUEST_LOCATION);

        ParentNotification parentNotification = new ParentNotification();
        parentNotification.setData(notificationData);
        parentNotification.setNotification(notification);
        parentNotification.setProfile_id(child.getProfile_id());

        new ParentTask(this, this).sendParentToChildNotification(parentNotification);

        handlerErrorDialog = new Handler();
        ErrorDialogRunnable = gpsErrorRunnable();
        handlerErrorDialog.postDelayed(ErrorDialogRunnable, 60000);

        showFetchingMessage(this);

        WordKik.saveLogcatToFile("MAPB_442");

    }




    /**
     * Methods responsible for Geofences
     * *********************************
     */

    private Icon geofencePinIcon(){
        IconFactory iconFactory = IconFactory.getInstance(this);
        Drawable iconDrawable = ContextCompat.getDrawable(this, R.drawable.geofence_pin_marker);
        return iconFactory.fromDrawable(iconDrawable);
    }


    private void addGeofencePinOnMap(UserGeofence geofence) {
        IconFactory iconFactory = IconFactory.getInstance(this);
        Drawable iconDrawable = ContextCompat.getDrawable(this, R.drawable.geofence_pin_marker);
        Icon icon = iconFactory.fromDrawable(iconDrawable);

        if (mapboxMap != null) {

            try {
                MarkerOptions marker = new MarkerOptions().snippet("" + geofence.getId()).position(new LatLng(geofence.getLat(), geofence.getLon())).icon(icon).title(geofence.getDescription());
                mapboxMap.addMarker(marker);

                if (geofence.getId() == 0) {
                    mapboxMap.selectMarker(marker.getMarker());
                }
            }
            catch (NullPointerException e){
                Toast.makeText(this, "Unfortunately Geofences are not available for your device.", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void createGeofence(double lat, double lon, int radius){
        userGeofence = new UserGeofence(lat, lon, radius);
        userGeofence.setId(0);
        addGeofencePinOnMap(userGeofence);
    }

    private void startGeofenceMode(){

        if (activeMarker != null){
            activeMarker.hideInfoWindow();
        }

        mapboxMap.setStyleUrl(Style.LIGHT);
        ResourceManager.getInstance().setBackgroundDrawable(topShadow, R.drawable.shadow_map_geomode);

        getSupportActionBar().setTitle(R.string.manage_geofences);
        setActionbarTextColor(getSupportActionBar(), ResourceManager.getInstance().color(R.color.brownGrey));

        btFetch.hide();
        tapGeofenceMessage = Snackbar.make(mapView, R.string.tap_map_geofence, Snackbar.LENGTH_INDEFINITE);
        tapGeofenceMessage.setAction(R.string.got_it, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tapGeofenceMessage.dismiss();
            }
        });

        tapGeofenceMessage.show();

        isInGeofenceMode = true;
        getApplicationContext().mpTrack(TAG_GEOFENCE);
    }


    public void addChildGeofence(String category, String title, double lat, double lon, int radius){
        userGeofence = new UserGeofence(child.getProfile_id(), category, title, lat, lon, radius);
    }

    private void pinChildGeofencesOnMap(Child child){

        for (UserGeofence geofence : child.getGeofences()){
            addGeofencePinOnMap(geofence);
        }
    }

    private View loadCreateGeofenceView(){
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.pin_new_geofence, null);

        final RelativeLayout content = (RelativeLayout) v.findViewById(R.id.content);
        final RelativeLayout description = (RelativeLayout) v.findViewById(R.id.description);
        final RelativeLayout category = (RelativeLayout) v.findViewById(R.id.category);
        final EditText  etDescription = (EditText) v.findViewById(R.id.etDescription);
        final TextView  step1  = (TextView)  v.findViewById(R.id.step1);
        final TextView  step2  = (TextView)  v.findViewById(R.id.step2);
        final Spinner   picker = (Spinner)   v.findViewById(R.id.picker);
        final ImageView ivBack = (ImageView) v.findViewById(R.id.ivBack);
        final ImageView ivNext = (ImageView) v.findViewById(R.id.ivNext);
        final ImageView ivSave = (ImageView) v.findViewById(R.id.ivSave);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animate.anime(new View[]{step2, description, category, step1}, 100, 300, new Techniques[]{Techniques.SlideOutDown, Techniques.SlideOutLeft, Techniques.SlideInRight, Techniques.SlideInUp}, null);

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etDescription.getWindowToken(), 0);
            }
        });

        ivNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (picker.getSelectedItemPosition() == 0){
                    AnimateFeedback.anime(picker, 0,500, Techniques.SlideInUp.Shake, null);
                }
                else {
                    Animate.anime(new View[]{step1, category, description, step2}, 100, 300, new Techniques[]{Techniques.SlideOutDown, Techniques.SlideOutRight, Techniques.SlideInLeft, Techniques.SlideInUp}, null);
                    description.requestFocus();

                    if (etDescription.getText().toString().length() == 0) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(etDescription, InputMethodManager.SHOW_IMPLICIT);
                    }
                }
            }
        });

        ivSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int index = mapboxMap.getMarkers().size() - 1;

                descript = etDescription.getText().toString();
                cat = getSelectedCategory(picker.getSelectedItemPosition());

                if (descript.length() > 0) {
                    double lat = mapboxMap.getMarkers().get(index).getPosition().getLatitude();
                    double lon = mapboxMap.getMarkers().get(index).getPosition().getLongitude();
                    addChildGeofence(cat, descript, lat, lon, 150);

                    Animate.anime(new View[]{step2, content}, 200, 400, new Techniques[]{Techniques.SlideOutDown, Techniques.ZoomOut}, ChildLocationActivity.this, 999);

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(description.getWindowToken(), 0);
                }
                else {
                    AnimateFeedback.anime(etDescription, 0,500, Techniques.SlideInUp.Shake, null);
                }
            }
        });

        return v;
    }

    private String getSelectedCategory(int index){
        String[] categories = getResources().getStringArray(R.array.geo_categories_eng);
        return categories[index];
    }

    private View loadPinDetailsView(final Marker marker, boolean showButtons){
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.pin_details, null);

        final RelativeLayout content = (RelativeLayout) v.findViewById(R.id.content);
        final TextView etDescription = (TextView) v.findViewById(R.id.etDescription);
        final ImageView ivDelete     = (ImageView) v.findViewById(R.id.ivDelete);


        if (isMarkerGeofence(marker)){
            etDescription.setText(marker.getTitle());
            if (showButtons) {
                ivDelete.setVisibility(View.VISIBLE);
            }
        }
        else {
            etDescription.setText(child.getName());
        }

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userGeofence = child.getGeofenceById(Integer.parseInt(marker.getSnippet()));
                userGeofence.setProfile_id(child.getProfile_id());

                Animate.anime(new View[]{content}, 200, 400, new Techniques[]{Techniques.ZoomOut}, ChildLocationActivity.this, 998);
            }
        });

        return v;
    }

    private boolean isMarkerGeofence(Marker marker){
        if(marker.getSnippet() != null && Integer.parseInt(marker.getSnippet()) != child.getId()){
            return true;
        }

        return false;
    }

    private void setInfoWindowAdapter(MapboxMap mapboxMap){
        mapboxMap.setInfoWindowAdapter(new MapboxMap.InfoWindowAdapter() {
                @Nullable
                @Override
                public View getInfoWindow(@NonNull Marker marker) {

                    activeMarker = marker;

                    if (isInGeofenceMode && isMarkerGeofence(marker)){
                        if (Integer.parseInt(marker.getSnippet()) == 0){
                            return loadCreateGeofenceView();

                        }
                        else{
                            return loadPinDetailsView(marker, true);
                        }
                    }
                    else {
                        return loadPinDetailsView(marker, false);
                    }
                }
            }
        );
    }

    private boolean isMarkerTooCloseToGeofence(LatLng point){
        Location newMarker = new Location("New Geofence");

        newMarker.setLatitude(point.getLatitude());
        newMarker.setLongitude(point.getLongitude());

        int index = mapboxMap.getMarkers().size() - 1;
        List<Marker> markers = mapboxMap.getMarkers();

        if (index >= 0) {
            markers.remove(index);
        }

        for (Marker marker : markers){
            Location geofence = new Location("");
            geofence.setLatitude(marker.getPosition().getLatitude());
            geofence.setLongitude(marker.getPosition().getLongitude());

            if (newMarker.distanceTo(geofence) < 300 && isMarkerGeofence(marker)){
                return true;
            }
        }

        return false;
    }

    private void removeEmptyMarkers(){
        List<Marker> markers = mapboxMap.getMarkers();
        for (Marker marker : markers){
            if (marker.getTitle()  == null){
                mapboxMap.removeMarker(marker);
            }
        }
    }

    private static Runnable gpsErrorRunnable(){
        return new Runnable() {
            @Override
            public void run() {

                try {
                    ConfirmationDialog.with((ChildLocationActivity) mContext)
                            .title(R.string.gps_not_found)
                            .message(mContext.getString(R.string.gps_off_msg).replace("%", child.getName()))
                            .background(R.color.red)
                            .gravity(Gravity.LEFT)
                            .show();
                }
                catch (WindowManager.BadTokenException e){
                    Log.e("ConfirmationDialog", "Impossible to display dialog. BadTokenException");
                }

                cancelErrorRunnable();
                showFetchButton((ChildLocationActivity) mContext);
            }
        };
    }





    /**
     * Methods responsible for Server Requests
     * ***************************************
     */

    private void performCreateGeofences(Object responseObject){
        ResponseAddGeofence responseAddGeofence = (ResponseAddGeofence) responseObject;

        if (responseAddGeofence.isSuccess()) {
            int index = mapboxMap.getMarkers().size() - 1;
            UserGeofence geofence = responseAddGeofence.getGeofence();

            mapboxMap.getMarkers().get(index).remove();
            mapboxMap.addMarker(new MarkerOptions().position(new LatLng(geofence.getLat(), geofence.getLon())).icon(geofencePinIcon()).title(geofence.getDescription()).snippet("" + geofence.getId()));

            child.getGeofences().add(geofence);
            geofences.add(geofence);

            if (activeMarker != null){
                activeMarker.hideInfoWindow();
            }

            removeEmptyMarkers();
            userGeofence = null;
        }
    }

    private void performListAllGeofences(Object responseObject){
        ResponseListGeofences responseListGeofences = (ResponseListGeofences) responseObject;

        if (responseListGeofences.isSuccess()){
            child.setGeofences(responseListGeofences.getGeofences());
        }

        isGeofencesReady = true;

        pinChildGeofencesOnMap(child);

    }

    private void performGetGeoActivity(Object responseObject){
        ResponseGeoActivities responseGeoActivities = (ResponseGeoActivities) responseObject;

        if (responseGeoActivities.isSuccess()){

            GeoActivitiesAdapter adapter = new GeoActivitiesAdapter(this, responseGeoActivities.getTransitions());
            lvTimeLine.setAdapter(adapter);

            handleNewIntent();
        }
    }

    @Override
    public void performTask(Object responseObject, String methodName) {
        Log.e("PERFORM_TASK", "METHOD: " + methodName + " CLASS: " + ChildLocationActivity.class);

        switch (methodName) {
            case CREATE_GEOFENCE:
                performCreateGeofences(responseObject);
                break;
            case DELETE_GEOFENCE:
                performDeleteGeofence(responseObject);
                break;
            case LIST_ALL_GEOFENCES:
                performListAllGeofences(responseObject);
                break;
            case GET_GEOACTIVITIES:
                performGetGeoActivity(responseObject);
                break;
            default:
                Log.e("PERFORM_TASK", "METHOD NOT IMPLEMENTED");
                break;
        }

        WordKik.saveLogcatToFile("MAPB_818");
    }

    public void performDeleteGeofence(Object responseObject) {
        ResponseDefault responseDefault = (ResponseDefault) responseObject;

        Log.w("GEOFENCE", "Response: " + responseDefault.getMessage());
        mapboxMap.removeMarker(activeMarker);
        geofences.remove(userGeofence);
        child.getGeofences().remove(userGeofence);
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        isMapReady = true;

        setInfoWindowAdapter(mapboxMap);
        mapboxMap.setOnMapClickListener(this);
        addMarkerOnMap(latitude, longitude);

        new ParentTask(this, this).listAllGeofences();

        requestPermissions();
    }

    private void getParentLocation(){
        mapboxMap.setMyLocationEnabled(true);
        mapboxMap.getMyLocationViewSettings().setAccuracyAlpha(15);
        mapboxMap.getMyLocationViewSettings().setAccuracyTintColor(ResourceManager.getInstance().color(R.color.blue));
        Location parentLocation = mapboxMap.getMyLocation();

        if (parentLocation != null) {
            LatLng parentLatLng = new LatLng(parentLocation.getLatitude(), parentLocation.getLongitude());

            CameraPosition cp = new CameraPosition.Builder().zoom(15).target(parentLatLng).build();
            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp), 2000);
        }

    }


    public void requestPermissions(){
        int LOCATION = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (LOCATION   == PackageManager.PERMISSION_GRANTED) {
            getParentLocation();
        }

        else {
            Nammu.init(this);
            Nammu.askForPermission(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    new PermissionCallback() {
                        @Override
                        public void permissionGranted() {
                            getParentLocation();
                        }

                        @Override
                        public void permissionRefused() {

                        }

                    });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }



    /**
     * Method responsible for Handling clicks on the Map
     * *************************************************
     */

    @Override
    public void onMapClick(@NonNull LatLng point) {
        if (isInGeofenceMode) {

            if (AccountManager.getInstance(this).checkGeofenceLimit(child)) {
                if (!isMarkerTooCloseToGeofence(point)) {
                    if (userGeofence != null) {
                        int index = mapboxMap.getMarkers().size() - 1;
                        if (mapboxMap.getMarkers().size() > 0) {
                            if (mapboxMap.getMarkers().get(index).getSnippet().equals("0")) {
                                mapboxMap.getMarkers().get(index).remove();
                            }
                        }
                    }

                    createGeofence(point.getLatitude(), point.getLongitude(), 100);
                    if (tapGeofenceMessage.isShown()) { tapGeofenceMessage.dismiss();}

                } else {
                    Toast.makeText(this, R.string.geofence_close_by, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }




    /**
     * Method responsible for Handling push notification
     * **************************************************
     */

    private void handleNewIntent(){
        String intent = (getIntent().getStringExtra("intent") != null) ? getIntent().getStringExtra("intent") : "none";
        if (intent.equals(INTENT_SHOW_GEOACTIVITIES)){
            toggleTimeLineView();
        }
        else if (intent.equals(INTENT_ENABLE_GEOFENCE_MODE)){
            startGeofenceMode();
        }
        else if (intent.equals(INTENT_LOCATE_CHILD)){
            requestChildLocation(child);
        }
    }



    /**
     * Method responsible for Handling clicks on the menu
     * **************************************************
     */


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finishActivity();
                break;
        }

        return true;
    }




    /**
     * Methods responsible for Activity Life Cycle
     * *******************************************
     */

    @Override
    protected void onPause() {
        MetricManager.getInstance().setTimeLeaving();
        mapView.onPause();

        cancelErrorRunnable();

        super.onPause();
    }

    @Override
    public void onResume() {
        mapView.onResume();
        AccountManager.getInstance(this).onResume(this);

        if (latitude != 0 && longitude != 0){
            addMarkerOnMap(latitude, longitude);
        }

        MetricManager.with(this).createNewMetric(TAG_SECTION);
        getApplicationContext().mpTrack(TAG_SECTION);

        super.onResume();
    }


    @Override
    public void onBackPressed() {
        if (isTimeLineShown) {
            toggleTimeLineView();
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        resetTrackingFunction();
        super.onDestroy();
    }




    @Override
    public void onAnimationFinish(View v, int tag) {
        switch (tag){
            case 999:
                new ParentTask(this, this).createGeofence(userGeofence);
                getApplicationContext().mpTrack(TAG_CREATED_GEOFENCE);
                getApplicationContext().mpTrack("GEOFENCE CATEGORY: " + userGeofence.getCategory());
                break;
            case 998:
                new ParentTask(this, this).deleteGeofence(userGeofence);
                break;
        }

    }

}

