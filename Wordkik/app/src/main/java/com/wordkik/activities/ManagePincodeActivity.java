package com.wordkik.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.MotionEvent;
import android.widget.TextView;
import android.widget.Toast;

import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.utils.MetricManager;
import com.wordkik.views.ManagePinCodeDialog;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;


public class ManagePincodeActivity extends BaseActivity implements ManagePinCodeDialog.PinCodeDialogInterface{
    @Bind(R.id.pinCode)            TextView pinCode;
    @Bind(R.id.toolbar)           Toolbar toolbar;

    private final String TAG = "Manage PIN Code";

    @OnClick(R.id.editPinCode)
    public void onClickEditPinCode(){
        new ManagePinCodeDialog(this).registerPinCode();
    }

    @OnTouch(R.id.viewPinCode)
    public boolean onTouchViewPinCode(MotionEvent event){

        if (event.getAction() == MotionEvent.ACTION_DOWN){
            pinCode.setInputType(InputType.TYPE_CLASS_TEXT);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP){
            pinCode.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            return true;
        }

        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_pincode);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pinCode.setText(Constants.user_pincode);

    }

    @Override
    public void onPincodeRegistered(String pincode) {
        pinCode.setText(pincode);
        Constants.user_pincode = pincode;
        Toast.makeText(this, R.string.pin_code_created, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPostResume() {
        MetricManager.with(this).createNewMetric(TAG);
        super.onPostResume();
    }

    @Override
    protected void onPause() {
        MetricManager.getInstance().setTimeLeaving();
        super.onPause();
    }
}

