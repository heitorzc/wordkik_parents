package com.wordkik.activities.intro;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wordkik.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by heitorzc on 22/04/16.
 */
public class FirstFragment extends Fragment{
    @Bind(R.id.wordkik)    TextView wordkik;
    @Bind(R.id.parenting)  TextView parenting;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.intro_first_fragment, container, false);
        ButterKnife.bind(this, v);

        Typeface light    = Typeface.createFromAsset(getActivity().getAssets(), "fonts/opensanslight.ttf");
        Typeface semibold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/opensanssemibold.ttf");

        wordkik.setTypeface(semibold);
        parenting.setTypeface(light);

        return v;
    }
}
