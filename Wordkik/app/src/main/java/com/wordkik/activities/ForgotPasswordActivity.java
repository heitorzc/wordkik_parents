package com.wordkik.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

import com.wordkik.R;
import com.wordkik.fragments.ForgotPassStepOneFragment;
import com.wordkik.fragments.ForgotPassStepTwoFragment;

/**
 * Activity to forgot password
 *
 * @author Denis Heringer
 * @version 1.0.0
 */
public class ForgotPasswordActivity extends BaseActivity {

    public static String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.forgot_pass_activity);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        getSupportFragmentManager().beginTransaction()
                .add(R.id.rlContent, new ForgotPassStepOneFragment())
                .commit();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.rlContent);

        if (fragment instanceof ForgotPassStepTwoFragment) {
            finish();
            return;
        }

        super.onBackPressed();
    }

}