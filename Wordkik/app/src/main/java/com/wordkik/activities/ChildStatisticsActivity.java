package com.wordkik.activities;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;
import com.wordkik.R;
import com.wordkik.objects.App;
import com.wordkik.objects.Category;
import com.wordkik.objects.Child;
import com.wordkik.objects.Metric;
import com.wordkik.objects.ResponseStatistics;
import com.wordkik.tasks.ChildTask;
import com.wordkik.tasks.TaskManager;
import com.wordkik.utils.Animate;
import com.wordkik.utils.MetricManager;
import com.wordkik.utils.ResourceManager;
import com.wordkik.views.OpenSansLightTextView;
import com.wordkik.views.OpenSansTextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChildStatisticsActivity extends BaseActivity implements TaskManager.TaskListener{
    @Bind(R.id.loading)       RelativeLayout loading;
    @Bind(R.id.rootView)      LinearLayout rootView;
    @Bind(R.id.toolbar)       Toolbar toolbar;
    @Bind(R.id.blocks)        OpenSansLightTextView tvBlocks;
    @Bind(R.id.hours)         OpenSansLightTextView tvScreenTime;
    @Bind(R.id.top_apps)      LinearLayout top_apps;
    @Bind(R.id.legend)        LinearLayout legend;
    @Bind(R.id.usage_chart)   PieChart usage_chart;
    @Bind(R.id.full_hours)    OpenSansLightTextView full_hours;
    @Bind(R.id.hours_desc)    LinearLayout hours_desc;
    @Bind(R.id.no_apps_data)  TextView no_apps_data;
    @Bind(R.id.usage)         LinearLayout usage;
    @Bind(R.id.adView)        AdView adView;



    @OnClick(R.id.hours_desc)
    public void onClickHoursDesc(){
        Animate.anime(new View[]{tvScreenTime, hours_desc, full_hours}, 0, 300, new Techniques[]{Techniques.FadeOut, Techniques.SlideOutRight, Techniques.FadeIn}, null);
        tvScreenTime.setVisibility(View.GONE);
        hours_desc.setVisibility(View.GONE);
    }

    @OnClick(R.id.full_hours)
    public void onClickFullHours(){
        Animate.anime(new View[]{full_hours, tvScreenTime, hours_desc}, 0, 300, new Techniques[]{Techniques.FadeOut, Techniques.FadeIn, Techniques.SlideInRight}, null);
        full_hours.setVisibility(View.GONE);
    }

    @OnClick(R.id.see_all_apps)
    public void onClickSeeAllApps(){

    }

    final String TAG = "Weekly Statistics";

    int app_list_count = 0;
    static Child child;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppThemeOrange);
        setContentView(R.layout.weekly_statistics_2);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadContent();
    }



    public void loadContent(){

        child = (Child) getIntent().getSerializableExtra("selected");

        setTitle(child.getName());
        no_apps_data.setText(getString(R.string.statistics_no_data).replace("%", child.getName()));

        new ChildTask(this, this).getStatistics(child);

        adManager.requestBannerAd(adView);

    }


    public void addAppToList(String nome, String icon, String time){

        if (Integer.parseInt(time.replace(":", "0")) != 0) {
            LinearLayout app = new LinearLayout(this);

            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.statistics_app_item, app);

            ImageView app_icon = (ImageView) view.findViewById(R.id.app_icon);
            TextView app_name = (TextView) view.findViewById(R.id.app_name);
            TextView app_time = (TextView) view.findViewById(R.id.app_time);

            Picasso.with(this).load(icon).placeholder(R.drawable.android).into(app_icon);

            app_name.setText(nome);
            app_time.setText(formatUsageTime(time));

            top_apps.addView(app, 1);
            app_list_count++;
        }

        if (app_list_count == 5){
            no_apps_data.setVisibility(View.GONE);
        }
    }


    public void addChartLegend(int color, String value){

        LinearLayout item = new LinearLayout(this);

        LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.chart_legend_item, item);

        ImageView ivColor = (ImageView) view.findViewById(R.id.color);
        OpenSansTextView tvValue = (OpenSansTextView) view.findViewById(R.id.value);

        Drawable d = ResourceManager.getInstance().drawable(R.drawable.white_button_rounded_150dp);
        d.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);

        ivColor.setImageDrawable(d);
        tvValue.setText(value);

        legend.addView(item);

    }

    private void setChartUsageData(ArrayList<Category> usageDatas) {

        ArrayList<PieEntry> yVals1 = new ArrayList<>();

        for (Category u : usageDatas){
            if (u.getName() != null && u.getUsage() > 1) {
                yVals1.add(new PieEntry(u.getUsage(), (int)u.getUsage() + "% " + u.getName()));
            }
        }

        if (yVals1.size() == 0){
            usage.setVisibility(View.GONE);
        }
        else{
            PieDataSet dataSet = new PieDataSet(yVals1, "");

            ArrayList<Integer> colors = new ArrayList<Integer>();

            for (int c : ColorTemplate.COLORFUL_COLORS)
                colors.add(c);


            for (int c : ColorTemplate.PASTEL_COLORS)
                colors.add(c);

            colors.add(ColorTemplate.getHoloBlue());

            dataSet.setColors(colors);

            PieData data = new PieData(dataSet);
            data.setValueFormatter(new DefaultValueFormatter(0));
            data.setValueTextSize(10f);
            data.setValueTextColor(Color.WHITE);
            data.setDrawValues(false);

            usage_chart.setData(data);


            usage_chart.highlightValues(null);
            usage_chart.setRotationEnabled(false);
            usage_chart.setDrawEntryLabels(false);
            usage_chart.setDescription(null);

            Legend legend = usage_chart.getLegend();

            for (int i = 0; i < legend.getEntries().length; i++){
                String label = legend.getEntries()[i].label;
                addChartLegend(legend.getEntries()[i].formColor, label);
            }

            usage_chart.getLegend().setEnabled(false);
            usage_chart.invalidate();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }

    @Override
    public void onPostResume() {
        MetricManager.with(this).createNewMetric(TAG);
        getApplicationContext().mpTrack(TAG);
        super.onPostResume();
    }

    @Override
    protected void onPause() {
        MetricManager.getInstance().setTimeLeaving();
        super.onPause();
    }

    @Override
    public void performTask(Object responseObject, String methodName) {
        ResponseStatistics responseStatistics = (ResponseStatistics) responseObject;

        if (responseStatistics.isSuccess()){
            tvBlocks.setText("" + responseStatistics.getBlocked_websites());
            full_hours.setText(responseStatistics.getScreen_time());

            String screen_time = responseStatistics.getScreen_time();

            //If screen time is smaller than 10 hours, hide the first 0.
            if (Integer.parseInt(screen_time.replace(":", "")) > 959){
                tvScreenTime.setText(responseStatistics.getScreen_time().substring(0,2));
            }
            else {
                tvScreenTime.setText(responseStatistics.getScreen_time().substring(1,2));
            }



            ArrayList<App> most_used_apps = responseStatistics.getMost_used_apps();
            for (App app : most_used_apps){
                String icon = (app.getIcon() != null && app.getIcon().length() > 0) ? app.getIcon() : "file:///android_asset/apps/android.png";
                addAppToList(app.getName(), icon, app.getUsage_in_time());
            }

            setChartUsageData(responseStatistics.getTop_categories());

            Animate.anime(new View[]{loading, rootView}, 0, 500, new Techniques[]{Techniques.FadeOutDown, Techniques.FadeInUp}, null);
        }
    }

    public String formatUsageTime(String duration){
        String formated = duration;


        if (duration.substring(0, 1).equals("0")){
            formated = duration.substring(1);
        }

        if (formated.substring(0, 1).equals("0")){
            formated = duration.substring(3) + "m";
            if (formated.substring(0,1).equals("0")){
                formated = formated.substring(1);
            }
        }
        else if (formated.substring(2, 4).equals("00")){
            formated = formated.replace(":00", "h");
        }
        else if (formated.substring(3, 4).equals("00")){
            formated = formated.substring(0) + "h";
        }
        else if (formated.substring(3).equals("00")){
            formated = formated.substring(0, 2) + "h";
        }

        else {
            formated = formated.replace(":", "h ") + "m";
        }

        return formated;
    }
}
