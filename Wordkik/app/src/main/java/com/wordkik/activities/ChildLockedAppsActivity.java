package com.wordkik.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdView;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.adapters.RvLockAppsAdapter;
import com.wordkik.objects.App;
import com.wordkik.objects.Child;
import com.wordkik.objects.DailyUsage;
import com.wordkik.objects.ResponseInstalledApps;
import com.wordkik.tasks.ChildTask;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;
import com.wordkik.utils.AccountManager;
import com.wordkik.utils.MetricManager;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ChildLockedAppsActivity extends BaseActivity implements ParentTask.TaskListener, TaskManager.IMethodName {
    @Bind(R.id.lvRemotelyLocks)           RecyclerView lvRemotelyLocks;
    @Bind(R.id.loading)                   LinearLayout loadingIndicatorView;
    @Bind(R.id.adView)                    AdView adView;



    final static String TAG = "Usage :: List Apps";

    public static Child child;
    RvLockAppsAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppThemeBlue);
        setContentView(R.layout.remotely_lock_apps_2);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadContent();

    }

    public void loadContent(){

        lvRemotelyLocks = (RecyclerView) findViewById(R.id.lvRemotelyLocks);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        lvRemotelyLocks.setLayoutManager(layoutManager);

        child = (Child) getIntent().getSerializableExtra("selected");

        setTitle(child.getName());

        new ChildTask(this, this).getInstalledApps(child);

        adManager.requestBannerAd(adView);
    }


    public  void loadAppsList(ArrayList<App> list){
        Log.e(TAG, "Called");

        ArrayList<App> new_list = new ArrayList<>();

        ArrayList<DailyUsage> new_usage = new ArrayList<>();
        new_usage.add(new DailyUsage(0, "Unlocked"));

         for (App app : list){
             if (app.getDaily_usages().size() == 0){
                 ArrayList<DailyUsage> usages = new ArrayList<>();
                 usages.add(new DailyUsage(0, "Unlocked"));
                 usages.add(new DailyUsage(1, "Unlocked"));
                 usages.add(new DailyUsage(2, "Unlocked"));
                 usages.add(new DailyUsage(3, "Unlocked"));
                 usages.add(new DailyUsage(4, "Unlocked"));
                 usages.add(new DailyUsage(5, "Unlocked"));
                 usages.add(new DailyUsage(6, "Unlocked"));
                 app.setDaily_usages(usages);
             }

             App newApp = new App(app.getName(), app.getIcon(), new_usage);
             newApp.setId(app.getId());
             newApp.setLocked(app.isLocked());
             newApp.setCategory(app.getCategory());
             newApp.setPackage_name(app.getPackage_name());
             newApp.setWeekly_usages(app.getWeekly_usages());
             newApp.setDaily_usages(app.getDaily_usages());

             new_list.add(newApp);
         }

        if (child != null) {
            child.setLocked_apps(new_list);

            loadingIndicatorView.setVisibility(View.GONE);

            adapter = new RvLockAppsAdapter(this, child, lvRemotelyLocks);
            lvRemotelyLocks.setAdapter(adapter);

        }

    }


    @Override
    public void performTask(Object responseObject, String methodName) {
        Log.e("PERFORM_TASK", "METHOD: " + methodName + " CLASS: " + ChildLockedAppsActivity.class);

        ResponseInstalledApps responseInstalledApps = (ResponseInstalledApps) responseObject;

        if (responseInstalledApps.isSuccess()) {
            ArrayList<App> list = responseInstalledApps.getApps();
            loadAppsList(list);
        }

        WordKik.saveLogcatToFile("LOKAPP_125");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPostResume() {
        MetricManager.with(this).createNewMetric(TAG);
        AccountManager.getInstance(this).onResume(this);
        getApplicationContext().mpTrack(TAG);

        super.onPostResume();
    }

    @Override
    protected void onPause() {
        MetricManager.getInstance().setTimeLeaving();
        super.onPause();
    }

}