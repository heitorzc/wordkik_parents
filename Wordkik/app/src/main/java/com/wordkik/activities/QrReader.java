package com.wordkik.activities;


import android.content.Context;
import android.graphics.PointF;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.wordkik.R;
import com.wordkik.utils.ChildManager;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author heitorzc
 * @version 1.0.0
 */
public class QrReader extends BaseActivity implements QRCodeReaderView.OnQRCodeReadListener{
    @Bind(R.id.qrdecoderview)   QRCodeReaderView readerView;
    @Bind(R.id.message)         EditText etCode;

    @OnClick(R.id.send)
    public void onClickNext(){
        String code = etCode.getText().toString();

        if (!code.isEmpty()) {
            finish();
            ChildManager.getInstance(this).showAddChildFormDialog(code);
        }
        else{
            Toast.makeText(this, R.string.emptycode, Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);   		 requestWindowFeature(Window.FEATURE_NO_TITLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.qr_reader);
        ButterKnife.bind(this);

        readerView.setOnQRCodeReadListener(this);

    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        readerView.setOnQRCodeReadListener(null);

        ((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).vibrate(100);

        finish();
        ChildManager.getInstance(this).showAddChildFormDialog(text);

    }

    @Override
    public void cameraNotFound() {
        Toast.makeText(this, R.string.qr_camera_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void QRCodeNotFoundOnCamImage() {

    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            readerView.getCameraManager().startPreview();
        }
        catch(Exception e){
            Toast.makeText(this, "Fail to connect camera service.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            readerView.getCameraManager().stopPreview();
        }
        catch(RuntimeException e){
            Toast.makeText(this, "Fail to connect camera service.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackPressed() {
        ChildManager.getInstance(this).showCancelFeedbackDialog(null);
        super.onBackPressed();
    }
}