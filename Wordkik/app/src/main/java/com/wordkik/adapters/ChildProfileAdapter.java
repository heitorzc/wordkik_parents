package com.wordkik.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.activities.ChildFeatures;
import com.wordkik.activities.MainActivity;
import com.wordkik.objects.Child;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import in.srain.cube.views.GridViewWithHeaderAndFooter;

/**
 * Created by heitorzc on 08/02/16.
 */
public class ChildProfileAdapter extends BaseAdapter {

	private ArrayList<Child> items;
	private LayoutInflater mInflater;
	private ViewHolder holder;
	private Context context;
    private GridViewWithHeaderAndFooter gridView;

    Child c;

    LinearLayout empty;

    static class ViewHolder {
        @Nullable @Bind(R.id.ivPhoto)   RoundedImageView ivPhoto;
        @Nullable @Bind(R.id.tvName)    TextView tvName;
        @Nullable @Bind(R.id.shadow)    View shadow;
        @Nullable @Bind(R.id.tvDevice)  TextView  tvDevice;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    /**
     * This method will check the new ArrayList and put all the active profiles into the SparseBooleanArray
     * and then notify the adapter that the content has changed.
     */
    public void update(){
        Constants.childs = items;
        gridView.setAdapter(this);
    }

	public ChildProfileAdapter(Context context, GridViewWithHeaderAndFooter gridView, ArrayList<Child>items, LinearLayout empty) {
		mInflater = LayoutInflater.from(context);
		this.items = items;
		this.context = context;
        this.gridView = gridView;
        this.empty = empty;
	}


    public ArrayList<Child> getItems(){
        return items;
    }
 
	@Override
	public int getCount() {
		return items.size();
	}
 
	@Override
	public Object getItem(int index) {
		return items.get(index);
	}
 
	@Override
	public long getItemId(int index) {
		return index;
	}

    @SuppressLint("NewApi")
    @Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
 
		if (convertView == null) {
            convertView = mInflater.inflate(R.layout.adapter_child_profile_item_new, null);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
 
		} else {

			holder = (ViewHolder) convertView.getTag();

		}


		final Child child = items.get(position);

        if (child.getPhoto() != null) {
            Picasso.with(context).load(child.getPhoto()).into(holder.ivPhoto);
            holder.shadow.setVisibility(View.VISIBLE);
        }
        else{
            Picasso.with(context).load(R.drawable.profile_placeholder).into(holder.ivPhoto);
            holder.shadow.setVisibility(View.GONE);
        }

        holder.tvName.setText(child.getName());

        holder.ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, ChildFeatures.class).putExtra("child", child);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation(((MainActivity) context), view, "child_photo_transition");
                    context.startActivity(intent, options.toBundle());
                }
                else {
                    context.startActivity(intent);
                }

            }
        });


        String device = formatChildDevice(child.getDevice());
        holder.tvDevice.setText(device.toUpperCase());

		return convertView;
	}

    private String formatChildDevice(String device){

        String lang = context.getString(R.string.language);

        if (lang.equals("pt")){
            return (device.equals("phone")) ? "CELULAR" : "TABLET";
        }

        return device;
    }

}