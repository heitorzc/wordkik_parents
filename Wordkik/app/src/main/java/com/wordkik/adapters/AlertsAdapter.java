package com.wordkik.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.objects.Alert;
import com.wordkik.objects.Child;
import com.wordkik.tasks.NotificationManager;
import com.wordkik.utils.AnimateFeedback;
import com.wordkik.utils.ResourceManager;

import java.util.ArrayList;


public class AlertsAdapter extends BaseAdapter implements AnimateFeedback.AnimateInterface{

	private ArrayList<Alert> items;
	private LayoutInflater mInflater;
	private ViewHolder holder;
	private Context context;
    private SparseBooleanArray mapUnreadAlerts;

    Typeface regular;

    static class ViewHolder{
		private CircularImageView ivPhoto;
		private TextView tvName;
		private TextView tvDescription;
		private TextView tvDate;
		private TextView tvTime;
        private ImageView tvRead;
	}

	public AlertsAdapter(Context context, ArrayList<Alert> items) {
		mInflater = LayoutInflater.from(context);
		this.items = items;
		this.context = context;

        this.mapUnreadAlerts = new SparseBooleanArray();

        for (int i = 0; i < items.size(); i++){
            if (items.get(i).getStatus().equals("unread")){
                mapUnreadAlerts.put(i, true);
                Log.e("MAP", i + " True " + items.get(i).getId());
            }
            else {
                Log.e("MAP", i + " False " + items.get(i).getId());
            }
        }
	}

    public void update(ArrayList<Alert> newArray){
        this.items = newArray;
        notifyDataSetChanged();
    }

    @Override
	public int getCount() {
        return items.size();
	}
 
	@Override
	public Object getItem(int index) {
        return items.get(index);
	}

    public ArrayList<Alert> getItems(){
        return items;
    }
 
	@Override
	public long getItemId(int index) {
		return index;
	}

    @Override
    public void onAnimationFinish(View v) {

    }
 
	@SuppressLint("NewApi")
	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {

            convertView = mInflater.inflate(R.layout.adapter_alerts_item, null);
            final View view = convertView;
            holder = new ViewHolder();

            regular  = Typeface.createFromAsset(context.getAssets(), "fonts/opensansregular.ttf");

            holder.ivPhoto = (CircularImageView) convertView.findViewById(R.id.ivPhoto);
            holder.tvName  = (TextView) convertView.findViewById(R.id.tvName);
            holder.tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);
            holder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
            holder.tvTime = (TextView) convertView.findViewById(R.id.tvTime);

            holder.tvRead = (ImageView) convertView.findViewById(R.id.tvAdvice);

            holder.tvName.setTypeface(regular);
            holder.tvDescription.setTypeface(regular);
            holder.tvDate.setTypeface(regular);
            holder.tvTime.setTypeface(regular);

            holder.tvRead.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (items.get(position).getStatus().equals("unread")) {
                        new NotificationManager(context).updateNotificationStatus(items.get(position), false);
                        Log.e("Click", "Clicked: " + position);

                        ((ImageView) view.findViewById(R.id.tvAdvice)).setBackgroundResource(R.drawable.green_button_stroke_rounded_32dp);
                        ((ImageView) view.findViewById(R.id.tvAdvice)).setImageDrawable(ResourceManager.getInstance().drawable(R.drawable.alert_tick));
                        ((TextView) view.findViewById(R.id.tvName)).setTextColor(ResourceManager.getInstance().color(R.color.Grey));
                        ((TextView) view.findViewById(R.id.tvName)).setTypeface(holder.tvName.getTypeface(), Typeface.NORMAL);
                        ((TextView) view.findViewById(R.id.tvDescription)).setTextColor(ResourceManager.getInstance().color(R.color.lightGrey));

                        mapUnreadAlerts.put(position, false);

                    }

                    AnimateFeedback.anime(v, 0, 500, Techniques.FlipInX, null);
                }
            });

            convertView.setTag(holder);

            usingAlertArray(position);

		return convertView;
	}

    private void usingAlertArray(int position){

        Alert a = items.get(position);

        Child child = Constants.getChildByProfileId(a.getChild_profile_id());

        if (child != null) {
            if (child.getPhoto() != null && !child.getPhoto().trim().equals("")) {
                Picasso.with(context).load(child.getPhoto())
                        .error(R.drawable.placeholder_green)
                        .placeholder(R.drawable.placeholder_green)
                        .into(holder.ivPhoto);
            } else {
                Picasso.with(context).load(R.drawable.placeholder_green).into(holder.ivPhoto);
            }

            if (child.getName() != null && !child.getName().trim().equals("")) {
                holder.tvName.setText(child.getName());
            } else {
                holder.tvName.setText("");
            }

        } else {
            Picasso.with(context).load(R.drawable.placeholder_green).into(holder.ivPhoto);
            holder.tvName.setText("");
        }

        String date = a.getReceived();
        String time = date.substring(13, 18);
        date = date.substring(0, 10);

        holder.tvDate.setText(date);
        holder.tvTime.setText(time);

        holder.tvDescription.setText(getDescription(a));

        Log.e("Click", "Loading: " + position);

        if (mapUnreadAlerts.get(position)){
            holder.tvName.setTextColor(ResourceManager.getInstance().color(R.color.blue));
            holder.tvName.setTypeface(null, Typeface.BOLD);

            holder.tvDescription.setTextColor(ResourceManager.getInstance().color(R.color.blue));
            holder.tvRead.setImageDrawable(ResourceManager.getInstance().drawable(R.drawable.eye_see));
            holder.tvRead.setBackgroundResource(R.drawable.green_button_rounded_32dp);
        }
        else{
            holder.tvName.setTextColor(context.getResources().getColor(R.color.Grey));

            holder.tvDescription.setTextColor(ResourceManager.getInstance().color(R.color.lightGrey));
            holder.tvRead.setImageDrawable(ResourceManager.getInstance().drawable(R.drawable.alert_tick));
            holder.tvRead.setBackgroundResource(R.drawable.green_button_stroke_rounded_32dp);
            mapUnreadAlerts.put(position, false);
        }
    }

    public String getDescription(Alert a){

        if (a.getCategory().equals("porn")){
            return context.getString(R.string.alert_porn_msg);
        }
        else if (a.getCategory().equals("violence")){
            return context.getString(R.string.alert_violence_msg);
        }
        else if (a.getCategory().equals("gambling")){
            return context.getString(R.string.alert_gambling_msg);
        }
        else if (a.getCategory().equals("drugs")){
            return context.getString(R.string.alert_drugs_msg);
        }
        else if (a.getCategory().equals("apps")){
            return context.getString(R.string.alert_apps_msg).replace("%", a.getApp_name());
        }
        else if (a.getCategory().equals("geofence_activity")){
            return context.getString(R.string.alert_geofence_msg);
        }
        else {
            return context.getString(R.string.alert_desc_msg);
        }
    }
}