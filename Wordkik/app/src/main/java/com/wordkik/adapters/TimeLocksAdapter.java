package com.wordkik.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Switch;
import android.widget.Toast;

import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.activities.ChildTimeLocksActivity;
import com.wordkik.objects.Child;
import com.wordkik.objects.ResponseTimeLock;
import com.wordkik.objects.TimeLock;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;
import com.wordkik.views.OpenSansLightTextView;
import com.wordkik.views.OpenSansTextView;

import java.util.ArrayList;

public class TimeLocksAdapter extends BaseAdapter implements TaskManager.TaskListener {

	private ArrayList<TimeLock> items;
	private LayoutInflater mInflater;
	private ViewHolder holder;
	private Context context;

    Child child;

    static class ViewHolder{
		private OpenSansTextView tvHours;
		private OpenSansLightTextView tvDescription;
        private OpenSansLightTextView tvTime;
        private Switch   activate;
	}

	public TimeLocksAdapter(Context context, Child child) {
		mInflater = LayoutInflater.from(context);
        this.items = child.getTimeLocks();
		this.context = context;
        this.child = child;
	}

    public void filterByWeekDay(int weekDay){
        ArrayList<TimeLock> newArray = new ArrayList<>();
         for (int i = 0; i < child.getTimeLocks().size(); i++){
             if (child.getTimeLocks().get(i).getWeek_day() == weekDay){
                 newArray.add(child.getTimeLocks().get(i));
             }
         }
        items = newArray;
        ChildTimeLocksActivity.lvTimeLocks.setAdapter(this);
    }
 
	@Override
	public int getCount() {
        return items.size();
	}

    public ArrayList<TimeLock> getItems(){
        return this.items;
    }
 
	@Override
	public Object getItem(int index) {
        return items.get(index);
	}
 
	@Override
	public long getItemId(int index) {
		return index;
	}
 
	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {

        if (convertView == null) {

            convertView = mInflater.inflate(R.layout.adapter_time_lock_item, null);
            holder = new ViewHolder();

            holder.tvDescription = (OpenSansLightTextView) convertView.findViewById(R.id.tvDescription);
            holder.tvHours = (OpenSansTextView) convertView.findViewById(R.id.tvHours);
            holder.tvTime = (OpenSansLightTextView) convertView.findViewById(R.id.tvTime);
            holder.activate = (Switch) convertView.findViewById(R.id.activate);
            convertView.setTag(holder);

        }
        else {

            holder = (ViewHolder) convertView.getTag();

        }

        final TimeLock t = items.get(position);
        t.setProfile_id(child.getProfile_id());

        holder.tvDescription.setText(t.getDescription());
        holder.tvHours.setText(context.getString(R.string.lock_for_adapter).replace("%", formatDuration(t.getDuration())));
        holder.tvTime.setText(t.getStart());

        holder.activate.setChecked(t.isActive());

        holder.activate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (((Switch) v).isChecked()){
                    activate(t);
                }
                else {
                    deactivate(t);
                }
            }
        });

        return convertView;

	}


    public String formatDuration(String duration){
        String formated = duration;


        if (duration.substring(0, 1).equals("0")){
            formated = duration.substring(1);
        }

        if (formated.substring(0, 1).equals("0")){
            formated = duration.substring(3) + " " + context.getString(R.string.minutes);
        }
        else if (formated.substring(2, 4).equals("00")){
            if (Integer.parseInt(formated.charAt(0) + "") > 1){
                formated = formated.replace(":00", " " + context.getString(R.string.hours));
            }
            else {
                formated = formated.replace(":00", " " + context.getString(R.string.hour));
            }
        }
        else if (formated.substring(3, 4).equals("00")){
            formated = formated.substring(0) + "h";
        }
        else if (formated.substring(3).equals("00")){
            formated = formated.substring(0, 2) + " " + context.getString(R.string.hours);
        }

        else {
            formated = formated.replace(":", "h ") + "m";
        }

        return formated;
    }


    public void activate(TimeLock t){
        if (WordKik.hasInternet) {
            t.activate();
            new ParentTask(context, this).editTimeLock(t);
        }
        else {
            Toast.makeText(context, R.string.emNoInternetConnection, Toast.LENGTH_SHORT).show();
        }
    }

    public void deactivate(TimeLock t){
        if (WordKik.hasInternet) {
            t.deactivate();
            new ParentTask(context, this).editTimeLock(t);
        }
        else {
            Toast.makeText(context, R.string.emNoInternetConnection, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void performTask(Object responseObject, String methodName) {
        ResponseTimeLock responseTimeLock = (ResponseTimeLock) responseObject;

        if (responseTimeLock.isSuccess()){
            ChildTimeLocksActivity.child.setTimeLocks(responseTimeLock.getTimelocks());
            Log.w("TIMELOCK", "edited");
        }
    }

}