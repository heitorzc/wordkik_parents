package com.wordkik.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.objects.ChatMessage;
import com.wordkik.utils.AnimateFeedback;

import java.util.ArrayList;


public class ChatAdapter extends BaseAdapter implements AnimateFeedback.AnimateInterface{

	private ArrayList<ChatMessage> items;
	private LayoutInflater mInflater;
	private ViewHolder holder;
	private Context context;



    static class ViewHolder{
		private CircularImageView ivPhoto;
		private TextView tvMessage;
	}

	public ChatAdapter(Context context, ArrayList<ChatMessage> items) {
		mInflater = LayoutInflater.from(context);
		this.items = items;
		this.context = context;
	}

    @Override
	public int getCount() {
        return items.size();
	}
 
	@Override
	public Object getItem(int index) {
        return items.get(index);
	}

    public ArrayList<ChatMessage> getItems(){
        return items;
    }
 
	@Override
	public long getItemId(int index) {
		return index;
	}

    @Override
    public void onAnimationFinish(View v) {

    }
 
	@SuppressLint("NewApi")
	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {

        ChatMessage m = items.get(position);
        int layout = (m.isAdmin_id()) ? R.layout.chat_support_message : R.layout.chat_user_message;

        convertView = mInflater.inflate(layout, null);

        holder = new ViewHolder();

        holder.ivPhoto = (CircularImageView) convertView.findViewById(R.id.photo);
        holder.tvMessage  = (TextView) convertView.findViewById(R.id.message);

        convertView.setTag(holder);

        holder.tvMessage.setText(m.getContent());

		if (layout == R.layout.chat_user_message){
			Picasso.with(context).load(Constants.user_photo).placeholder(R.drawable.placeholder_green).into(holder.ivPhoto);
		}

		return convertView;
	}

}