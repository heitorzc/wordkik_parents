package com.wordkik.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.objects.GeoActivity;
import com.wordkik.utils.ResourceManager;

import java.util.ArrayList;

public class GeoActivitiesAdapter extends BaseAdapter{

	private ArrayList<GeoActivity> activities;
	private ArrayList<String> hours = new ArrayList<>();
	private LayoutInflater mInflater;
	private ViewHolder holder;
	private Context context;


	static class ViewHolder{
		private TextView tvTime;
		private View line;
		private LinearLayout itemPlaceholder;
	}

	public GeoActivitiesAdapter(Context context, ArrayList<GeoActivity> activities) {
		mInflater = LayoutInflater.from(context);
		this.activities = activities;
		this.context = context;

		hours.add("00:00");	hours.add("01:00");	hours.add("02:00");	hours.add("03:00");
		hours.add("04:00");	hours.add("05:00");	hours.add("06:00");	hours.add("07:00");
		hours.add("08:00");	hours.add("09:00");	hours.add("10:00");	hours.add("11:00");
		hours.add("12:00");	hours.add("13:00");	hours.add("14:00");	hours.add("15:00");
		hours.add("16:00");	hours.add("17:00");	hours.add("18:00");	hours.add("19:00");
		hours.add("20:00");	hours.add("21:00");	hours.add("22:00");	hours.add("23:00");

	}

 
	@Override
	public int getCount() {
        return hours.size();

	}
 
	@Override
	public Object getItem(int index) {
        return hours.get(index);

	}
 
	@Override
	public long getItemId(int index) {
		return index;
	}
 
	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		
		convertView = mInflater.inflate(R.layout.geo_activity_item, null);
		holder = new ViewHolder();

		holder.itemPlaceholder = (LinearLayout) convertView.findViewById(R.id.itemsPlaceholder);
		holder.tvTime = (TextView) convertView.findViewById(R.id.tvTime);
		holder.line = convertView.findViewById(R.id.line);

		convertView.setTag(holder);


		String time = hours.get(position);
		holder.tvTime.setText(time);

		int index = Integer.parseInt(holder.tvTime.getText().toString().substring(0, 2));
		getActivitiesForPosition(index, holder.itemPlaceholder, holder.line, holder.tvTime);

		return convertView;
	}

	private void addGeoActivityToTimeLine(LinearLayout placeholder, GeoActivity a){

		RelativeLayout act_layout = new RelativeLayout(context);

		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = layoutInflater.inflate(R.layout.geo_timeline_item, act_layout);

		CircularImageView ivPhoto = (CircularImageView) view.findViewById(R.id.ivPhoto);
		TextView tvTransition = (TextView) view.findViewById(R.id.tvTransiction);
		TextView tvGeoTitle = (TextView) view.findViewById(R.id.tvGeoTitle);
		TextView tvTime = (TextView) view.findViewById(R.id.tvTime);

		Picasso.with(context).load(Constants.getChildById(a.getChild_id()).getPhoto()).placeholder(R.drawable.placeholder_green).into(ivPhoto);

		String transition = (a.getTransition() == 1) ? context.getString(R.string.arrived_at) : context.getString(R.string.departured_from);

		tvTransition.setText(transition);
		tvGeoTitle.setText(a.getGeofence_name());
		tvTime.setText(a.getTimestamp());

		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		placeholder.setLayoutParams(lp);


		placeholder.addView(act_layout);

	}

	private void getActivitiesForPosition(int position, LinearLayout placeholder, View line, TextView tvTime){

		int count = 0;

		for (GeoActivity activity : activities){
			if (Integer.parseInt(activity.getTimestamp().substring(0, 2)) == position){
				addGeoActivityToTimeLine(placeholder, activity);
				setLineColor(activity.getTransition(), line);
				count++;
			}
		}

		if (count == 0){
			line.setVisibility(View.INVISIBLE);
			tvTime.setTextColor(ResourceManager.getInstance().color(R.color.lighterGrey));

		}
		else if (count >= 2){
			line.setBackgroundColor(ResourceManager.getInstance().color(R.color.geo_many));
		}
	}

	private void setLineColor(int transition, View line){
		if (transition == 1){
			line.setBackgroundColor(ResourceManager.getInstance().color(R.color.geo_enter));
		}
		else{
			line.setBackgroundColor(ResourceManager.getInstance().color(R.color.geo_exit));
		}
	}

}