package com.wordkik.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wordkik.R;
import com.wordkik.objects.Plan;

import java.util.ArrayList;

public class PlansAdapter extends BaseAdapter{

	private ArrayList<Plan> plans;
	private LayoutInflater mInflater;
	private ViewHolder holder;
	private Context context;

	static class ViewHolder{
		private TextView tvTitle;
		private TextView tvCurrency;
		private TextView tvPrice;
		private TextView tvSave;
		private TextView tvOldPrice;
	}

	public PlansAdapter(Context context, ArrayList<Plan> plans) {
		mInflater = LayoutInflater.from(context);
		this.plans = plans;
		this.context = context;
	}
 
	@Override
	public int getCount() {
        return plans.size();

	}
 
	@Override
	public Object getItem(int index) {
        return plans.get(index);

	}
 
	@Override
	public long getItemId(int index) {
		return index;
	}
 
	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {

        if (convertView == null) {

            convertView = mInflater.inflate(R.layout.plan_item, null);
            holder = new ViewHolder();

            holder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            holder.tvCurrency = (TextView) convertView.findViewById(R.id.tvSymbol);
            holder.tvPrice = (TextView) convertView.findViewById(R.id.tvPrice);
			holder.tvOldPrice = (TextView) convertView.findViewById(R.id.tvOldPrice);
			holder.tvSave = (TextView) convertView.findViewById(R.id.tvSave);
            convertView.setTag(holder);

        }
        else {

            holder = (ViewHolder) convertView.getTag();

        }

        Plan p = plans.get(position);

        holder.tvTitle.setText(p.getTitle());
        holder.tvCurrency.setText(p.getCurrency());
        holder.tvPrice.setText(p.getPrice());
		holder.tvOldPrice.setText(context.getResources().getString(R.string.price_was).replace("%", p.getOldPrice()));
		holder.tvSave.setText(context.getResources().getString(R.string.save_plan).replace("%", p.getSaving()));

		return convertView;
	}
}