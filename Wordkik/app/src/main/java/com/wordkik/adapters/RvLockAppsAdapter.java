package com.wordkik.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.objects.App;
import com.wordkik.objects.Child;
import com.wordkik.objects.viewHolders.AppChildHolder;
import com.wordkik.objects.viewHolders.AppParentHolder;
import com.wordkik.utils.AccountManager;


/**
 * Created by heitorzc on 29/11/2016.
 */

public class RvLockAppsAdapter extends ExpandableRecyclerViewAdapter<AppParentHolder, AppChildHolder> {

    private Child child;
    private Context context;
    private LayoutInflater mInflator;
    private RecyclerView list;



    public RvLockAppsAdapter(Context context, Child child, RecyclerView list) {
        super(child.getLocked_apps());
        this.context = context;
        this.child = child;
        this.list = list;
        this.mInflator = LayoutInflater.from(context);
    }



    @Override
    public AppParentHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = mInflator.inflate(R.layout.adapter_remotely_lock_app_item, parent, false);
        return new AppParentHolder(view);
    }



    @Override
    public AppChildHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = mInflator.inflate(R.layout.limit_sliders, parent, false);
        return new AppChildHolder(view);
    }



    @Override
    public void onBindChildViewHolder(AppChildHolder holder, int flatPosition,
                                      ExpandableGroup group, int childIndex) {

        final App app = (App) group;
        boolean validLimit = AccountManager.getInstance(context).checkAppUsageLimit(child, app);

        holder.configureSeekBars(this, app, validLimit);
        list.smoothScrollToPosition(flatPosition);



    }



    @Override
    public void onBindGroupViewHolder(AppParentHolder holder, int flatPosition,
                                      ExpandableGroup group) {

        App app = (App) group;

        holder.setAppName(app.getName());
        holder.setAppIcon(context, app.getIcon());
        holder.setAppLimit(app.getWeekdaysUsage(), app.getWeekendsUsage());

    }


}


