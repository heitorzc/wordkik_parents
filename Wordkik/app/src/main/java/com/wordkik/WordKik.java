package com.wordkik;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.jiongbull.jlog.JLog;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.rollbar.android.Rollbar;
import com.wordkik.broadcastReceivers.ListenConectivityStatus;
import com.wordkik.tasks.AmazonS3LogsUploader;
import com.wordkik.tasks.AmazonS3Uploader;
import com.wordkik.tasks.NotificationManager;
import com.wordkik.utils.AccountManager;
import com.wordkik.utils.ResourceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Properties;

import io.realm.Realm;
import me.pushy.sdk.Pushy;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;

/**
 * Created by heitorzc on 03/04/16.
 */
public class WordKik extends Application implements SharedPreferences.OnSharedPreferenceChangeListener{


    public static boolean hasInternet       = false;
    public static Calendar cur_cal          = Calendar.getInstance();


    private String projectToken = "a60ef31de9c5dcf55b30ab5ed328f5d7";

    static Context context;

    //public static AccountManager accManager;
    public static CircularImageView changePhoto;
    public static String file_name;
    public static SharedPreferences prefs;
    public static AmazonS3Uploader.AmazonS3UploaderListener amazonS3UploaderListener;

    ListenConectivityStatus internet;


    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
        ResourceManager.init(this);
        AccountManager.init(this);
        Pushy.listen(this);
        Rollbar.init(this, "aeb3e69c91c64bd592088ee092bbfb20", "production");

        JLog.init(this).setDebug(BuildConfig.DEBUG);

        context = this;

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);

        internet = new ListenConectivityStatus();
        internet.register(this);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

    }


    public void mpTrack(String action){
        MixpanelAPI.getInstance(this, projectToken).track(action);
    }


    public void mpFlush(){
        MixpanelAPI.getInstance(this, projectToken).flush();
    }


    public void mpSetProfile(String id, String name, String email){
        MixpanelAPI.getInstance(this, projectToken).getPeople().identify(id);
        JSONObject profile = new JSONObject();
        try {
            profile.put("$name", name);
            profile.put("$email", email);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        MixpanelAPI.getInstance(this, projectToken).getPeople().set(profile);
    }


    public static String getAppIcon(Context c, String appPackage) {
        Properties properties = new Properties();

        try {
            AssetManager assetManager = c.getAssets();
            InputStream inputStream = assetManager.open("apps/apps.properties");
            properties.load(inputStream);

        } catch (IOException e) {
            Log.e("AssetsPropertyReader", e.toString());
        }

        String app_icon = properties.getProperty(appPackage);

        if (app_icon != null){
            return app_icon;
        }
        else{
         return properties.getProperty("com.android");
        }
    }


    public static void callPhotoPicker(final Activity a, String id, CircularImageView iv, AmazonS3Uploader.AmazonS3UploaderListener listener){
        changePhoto = iv;
        file_name = id + "_" + System.currentTimeMillis();
        amazonS3UploaderListener = listener;

        int WRITE_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(a, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int CAMERA = ContextCompat.checkSelfPermission(a, android.Manifest.permission.CAMERA);

        if (WRITE_EXTERNAL_STORAGE == PackageManager.PERMISSION_GRANTED && CAMERA == PackageManager.PERMISSION_GRANTED) {
            EasyImage.openChooserWithGallery(a, a.getString(R.string.select_photo), 0);
        }
        else {
            Nammu.init(a);
            Nammu.askForPermission(a, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA}, new PermissionCallback() {
                @Override
                public void permissionGranted() {
                    EasyImage.openChooserWithGallery(a, a.getString(R.string.select_photo), 0);
                }

                @Override
                public void permissionRefused() {
                    amazonS3UploaderListener.setPictureURL(null, null);
                }
            });
        }
    }


    public static void saveLogcatToFile(String file_name) {
        WordKik.cur_cal.setTimeInMillis(System.currentTimeMillis());
        String time_stamp = WordKik.cur_cal.getTime().toString().replace(" ", "_");
        String logfilename = Constants.user_id + "_" + file_name + "_" + time_stamp + ".txt";

        try {
            final File path = new File(Environment.getExternalStorageDirectory(), "WordKik_Logs");
            if (!path.exists()) {
                path.mkdir();
            }
            Runtime.getRuntime().exec("logcat  -d -f " + path + File.separator + logfilename);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (WordKik.hasInternet) {
            new AmazonS3LogsUploader(context, logfilename, new File(Environment.getExternalStorageDirectory(), "WordKik_Logs/" + logfilename)).execute();
        }

        Log.e("FILENAME", Environment.getExternalStorageDirectory() + "/WordKik_Logs/" + logfilename);
    }


    @Override
    public void onTerminate() {
        internet.unregister(this);
        super.onTerminate();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("allow_push")){
            Constants.allow_notifications = sharedPreferences.getBoolean(key, true);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
