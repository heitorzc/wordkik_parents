/*
package com.wordkik.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.jiongbull.jlog.JLog;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.activities.ChildLocationActivity;
import com.wordkik.objects.Child;
import com.wordkik.tasks.GeneratePushNotification;

*/
/**
 * Created by heitorzc on 13/08/16.
 *//*

public class FbPushReceiver extends FirebaseMessagingService implements GeneratePushNotification.GeneratePushNotificationInterface {

    int countNotification = 0;
    public static Child acceptLocation;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        JLog.json("FIREBASE", remoteMessage.getData().toString());

        String notification_type = remoteMessage.getData().get("notification_type");

        if (notification_type != null) {

            if (notification_type.equals(Constants.NOTIFICATION_TYPE_CHAT)) {
                notifyUser(Constants.NOTIFICATION_TYPE_CHAT, remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), remoteMessage.getData().get("child_name"), null);
                return;
            }

            if (notification_type.equals(Constants.NOTIFICATION_TYPE_CMS)) {
                notifyUser(Constants.NOTIFICATION_TYPE_CMS, remoteMessage.getData().get("notification_category"), remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), remoteMessage.getData().get("child_name"), null);
                return;
            }

            if (notification_type.equals(Constants.NOTIFICATION_TYPE_ALERT)) {
                Constants.alerts_counter++;
                new com.wordkik.tasks.NotificationManager(this).updateCounter(Constants.alerts_counter++);

                String category = remoteMessage.getData().get("notification_category");

                if (category.equals("geofence_activity")){
                    String geo_title = remoteMessage.getData().get("geofence_title");
                    int transition = Integer.parseInt(remoteMessage.getData().get("transition"));

                    String title = getString(R.string.geofence_activity);
                    String body = (transition == 1) ? getString(R.string.just_arrived) : getString(R.string.just_departured);

                    notifyUser(Constants.NOTIFICATION_TYPE_ALERT, category, title, body + geo_title, remoteMessage.getData().get("child_name"), remoteMessage.getData().get("child_photo"));
                }
                else {
                    notifyUser(Constants.NOTIFICATION_TYPE_ALERT, remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), remoteMessage.getData().get("child_name"), remoteMessage.getData().get("child_photo"));
                }


                return;
            }

            if (notification_type.equals(Constants.NOTIFICATION_TYPE_GEOFENCE_RPT)){
                new com.wordkik.tasks.NotificationManager(this).updateCounter(Constants.alerts_counter++);
                notifyUser(Constants.NOTIFICATION_TYPE_GEOFENCE_RPT, getString(R.string.location_report_push_title), getString(R.string.location_report_push_body), remoteMessage.getData().get("child_name"), remoteMessage.getData().get("child_photo"));
                return;
            }

            if (notification_type.equals(Constants.NOTIFICATION_TYPE_LOCK_APPS)) {
                Constants.alerts_counter++;
                new com.wordkik.tasks.NotificationManager(this).updateCounter(Constants.alerts_counter++);

                String app_name = remoteMessage.getData().get("app_name");
                String child_name = remoteMessage.getData().get("child_name");
                String title = app_name + " " + getString(R.string.unlocked);
                String body = app_name + " " + getString(R.string.unlocked_push_body).replace("%", child_name);

                notifyUser(Constants.NOTIFICATION_TYPE_LOCK_APPS, title, body, child_name, remoteMessage.getData().get("child_photo"));
                return;
            }

            if (notification_type.equals(Constants.NOTIFICATION_TYPE_REQUEST_LOCATION)){
                Log.e("Location", "Received!");
                String lat = remoteMessage.getData().get("lat");
                String lon = remoteMessage.getData().get("lon");
                String child_id = remoteMessage.getData().get("child_id");

                if (acceptLocation != null) {

                    Log.w("AccLocation", "Only accept id: " + acceptLocation.getId() + "Got: " + child_id);
                    if (Integer.parseInt(child_id) ==  acceptLocation.getId()) {
                        if (ChildLocationActivity.mapView != null) {
                            ChildLocationActivity.generateLocationPoint(Double.parseDouble(lat), Double.parseDouble(lon));
                        }
                    }
                }

                return;
            }

        }
    }


    public void notifyUser(String notification_type, String title, String body, String child_name, String child_photo){
        if (Constants.allow_notifications) {
            new GeneratePushNotification(this, notification_type, title, body, child_name, child_photo, this).execute();
        }
    }

    public void notifyUser(String notification_type, String category, String title, String body, String child_name, String child_photo){
        if (Constants.allow_notifications) {
            new GeneratePushNotification(this, notification_type, category, title, body, child_name, child_photo, this).execute();
        }
    }

    @Override
    public void onNotificationReady(Notification notification) {
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(countNotification++, notification);
    }

}
*/
