package com.wordkik.services;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.wordkik.WordKik;
import com.wordkik.objects.Metric;
import com.wordkik.objects.ResponseMetric;
import com.wordkik.objects.UserMetric;
import com.wordkik.realm.DbProvider;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by heitorzc on 05/04/16.
 */
public class SendMetrics extends Service implements ParentTask.TaskListener, TaskManager.IMethodName {

    private final String TAG = "METRICS";

    private boolean isRuningTask = false;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "Metric Service Created");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (WordKik.hasInternet) {
            if (!isRuningTask) {
                startServiceActivity();
            } else {
                Log.i(TAG, "Metrics Service already has an activity running.");
            }
        }
        else {
            Log.e(TAG, "No internet connection. Trying again in 10 minutes.");
        }

        return super.onStartCommand(intent, flags, startId);

    }



    private void startServiceActivity(){
        Log.e(TAG, "Metric Service just started activity");

        if (WordKik.hasInternet) {
            if (DbProvider.with(this).hasStoredMetrics()) {

                isRuningTask = true;

                Realm realm = DbProvider.with(this).getRealm();

                RealmResults<Metric> results = DbProvider.with(this).getStoredMetrics();
                ArrayList<Metric> metrics = new ArrayList<>(realm.copyFromRealm(results));

                UserMetric userMetric = new UserMetric();
                userMetric.setMetrics(metrics);

                new ParentTask(SendMetrics.this, this).upladoMetrics(userMetric);

                return;

            }
        }

        Log.w(TAG, "There are no metrics to send.");

    }



    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e(TAG, "Metrics Service Task Removed");
        super.onTaskRemoved(rootIntent);
    }


    @Override
    public void onDestroy() {
        Log.i(TAG, "Metrics Service Destroyed");
        super.onDestroy();
    }




    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void performTask(Object responseObject, String methodName) {
        Log.e(TAG, "CALLBACK - Method: " + methodName + " Class: " + SendMetrics.class);

        switch (methodName) {
            case SEND_METRICS:
                performSendMetrics(responseObject);
                break;
            default:
                Log.e(TAG, "CALLBACK: Method not implemented: " + methodName);
                break;
        }
    }


    private void performSendMetrics(Object responseObject) {
        ResponseMetric responseMetric = (ResponseMetric) responseObject;

        if (responseMetric.isSuccess()){

            DbProvider.with(this).clearStoredMetrics();
            Log.d(TAG, "Metrics sent successfully.");

        }
        else{
            Log.d(TAG, "Error sending metrics.");
        }

        isRuningTask = false;
        stopSelf();

    }

}


