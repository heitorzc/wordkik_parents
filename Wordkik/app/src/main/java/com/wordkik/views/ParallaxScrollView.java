package com.wordkik.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.wordkik.Constants;
import com.wordkik.R.styleable;

import java.util.ArrayList;
import java.util.Iterator;

public class ParallaxScrollView extends ScrollView {
    private static final int DEFAULT_PARALLAX_VIEWS = 1;
    private static final float DEFAULT_INNER_PARALLAX_FACTOR = 1.9F;
    private static final float DEFAULT_PARALLAX_FACTOR = 1.9F;
    private static final float DEFAULT_ALPHA_FACTOR = -1.0F;
    private int numOfParallaxViews = 1;
    private float innerParallaxFactor = 1.9F;
    private float parallaxFactor = 1.9F;
    private float alphaFactor = -1.0F;
    private ArrayList<ParallaxedView> parallaxedViews = new ArrayList();

    private OnScrollViewListener mOnScrollViewListener;

    public ParallaxScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.init(context, attrs);
    }

    public ParallaxScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context, attrs);
    }

    public ParallaxScrollView(Context context) {
        super(context);
    }

    protected void init(Context context, AttributeSet attrs) {
        TypedArray typeArray = context.obtainStyledAttributes(attrs, styleable.ParallaxScroll);
        this.parallaxFactor = typeArray.getFloat(0, 1.9F);
        this.alphaFactor = typeArray.getFloat(1, -1.0F);
        this.innerParallaxFactor = typeArray.getFloat(2, 1.9F);
        this.numOfParallaxViews = typeArray.getInt(3, 1);
        typeArray.recycle();
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.makeViewsParallax();
    }

    private void makeViewsParallax() {
        if(this.getChildCount() > 0 && this.getChildAt(0) instanceof ViewGroup) {
            ViewGroup viewsHolder = (ViewGroup)this.getChildAt(0);
            int numOfParallaxViews = Math.min(this.numOfParallaxViews, viewsHolder.getChildCount());

            for(int i = 0; i < numOfParallaxViews; ++i) {
                ScrollViewParallaxedItem parallaxedView = new ScrollViewParallaxedItem(viewsHolder.getChildAt(i));
                this.parallaxedViews.add(parallaxedView);
            }
        }

    }

    public void setOnScrollViewListener(OnScrollViewListener l) {
        this.mOnScrollViewListener = l;
    }

    public interface OnScrollViewListener {
        void onScrollchanged(int l, int t, int oldl, int oldt);
    }

    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        float parallax = this.parallaxFactor;
        float alpha = this.alphaFactor;

        ParallaxedView parallaxedView;
        for(Iterator i$ = this.parallaxedViews.iterator(); i$.hasNext(); parallaxedView.animateNow()) {
            parallaxedView = (ParallaxedView)i$.next();
            parallaxedView.setOffset((float)t / parallax);
            parallax *= this.innerParallaxFactor;
            if(alpha != -1.0F) {
                float fixedAlpha = t <= 0?1.0F:100.0F / ((float)t * alpha);
                parallaxedView.setAlpha(fixedAlpha);
                alpha /= this.alphaFactor;
            }
        }
        mOnScrollViewListener.onScrollchanged(l, t, oldl, oldt );

    }

    private float getAlphaForView(int position) {
        int diff = 0;
        float minAlpha = 0.4f, maxAlpha = 1.f;
        float alpha = minAlpha; // min alpha
        if (position > Constants.screen_height)
            alpha = minAlpha;
        else if (position + (Constants.screen_height / 2) < Constants.screen_height)
            alpha = maxAlpha;
        else {
            diff = Constants.screen_height - position;
            alpha += ((diff * 1f) / (Constants.screen_height / 2))* (maxAlpha - minAlpha); // 1f and 0.4f are maximum and min
            // alpha
            // this will return a number betn 0f and 0.6f
        }
        // System.out.println(alpha+" "+screenHeight +" "+locationImageInitialLocation+" "+position+" "+diff);
        return alpha;
    }

    protected class ScrollViewParallaxedItem extends ParallaxedView {
        public ScrollViewParallaxedItem(View view) {
            super(view);
        }

        protected void translatePreICS(View view, float offset) {
            view.offsetTopAndBottom((int)offset - this.lastOffset);
            this.lastOffset = (int)offset;
        }
    }
}