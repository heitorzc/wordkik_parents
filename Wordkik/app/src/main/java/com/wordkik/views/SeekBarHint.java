package com.wordkik.views;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jiongbull.jlog.JLog;
import com.wordkik.R;
import com.wordkik.activities.ChildLockedAppsActivity;
import com.wordkik.adapters.RvLockAppsAdapter;
import com.wordkik.objects.App;
import com.wordkik.objects.Child;
import com.wordkik.objects.RequestScreenLimit;
import com.wordkik.objects.RequestSendAppList;
import com.wordkik.objects.ResponseDefault;
import com.wordkik.objects.ResponseNotification;
import com.wordkik.tasks.ChildTask;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;

import java.util.ArrayList;


public class SeekBarHint extends SeekBar implements SeekBar.OnSeekBarChangeListener, TaskManager.TaskListener, TaskManager.IMethodName {

    private boolean screenTimeMode = false;
    private String[] daily_usages;
    private String[] daily_usages_formatted;

    private Handler handler = new Handler();
    private RvLockAppsAdapter adapter;
    private Context mContext;
    private TextView bubble;
    private String days;
    private App app;
    private Child child;



    public SeekBarHint(Context context) {
        super(context);
        this.mContext = context;
    }

    public SeekBarHint(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public SeekBarHint(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
    }



    public void configureForScreenTime(Child child, TextView bubble, boolean isWeekdays){
        this.screenTimeMode         = true;
        this.child                  = child;
        this.bubble                 = bubble;
        this.daily_usages           = getResources().getStringArray(R.array.screen_limit);
        this.daily_usages_formatted = getResources().getStringArray(R.array.screen_limit_formatted);
        this.setMax(daily_usages.length - 1);

        days = (isWeekdays) ? "weekdays" : "weekends";

        initHintPopup(bubble);
        attachSeekBar();

        setProgress(getProgressFromScreenLimits());


    }



    private int getProgressFromScreenLimits(){


        for (int i = 0; i < daily_usages_formatted.length; i++){

            if (daily_usages_formatted[i].equals(child.getScreenLimitByWeekday(days).getUsage())){
                return i;
            }

        }

        return 0;

    }



    public void configure(RvLockAppsAdapter adapter, App app, TextView bubble, boolean isWeekdays){
        this.adapter                = adapter;
        this.app                    = app;
        this.bubble                 = bubble;
        this.daily_usages           = getResources().getStringArray(R.array.daily_usage);
        this.daily_usages_formatted = getResources().getStringArray(R.array.daily_usage_formatted);
        this.setMax(daily_usages.length - 1);

        initHintPopup(bubble);
        attachSeekBar();

        if (isWeekdays){
            days = "weekdays";
            setProgress(getProgressFromUsage(app.getWeekdaysUsage()));
        }
        else {
            days = "weekend";
            setProgress(getProgressFromUsage(app.getWeekendsUsage()));
        }

    }



    private int getProgressFromUsage(String usage){

        for (int i = 0; i < daily_usages_formatted.length; i++){

            if (daily_usages_formatted[i].equals(usage)){
                return i;
            }

        }

        return 0;

    }



    private void initHintPopup(TextView bubble) {
        this.bubble = bubble;
        this.setOnSeekBarChangeListener(this);
    }



    private void attachSeekBar() {

        final ViewTreeObserver.OnGlobalLayoutListener layoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (getVisibility() != View.VISIBLE) hidePopup();
                else showPopup();
            }
        };

        this.addOnAttachStateChangeListener(new OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {
                getViewTreeObserver().addOnGlobalLayoutListener(layoutListener);
            }

            @Override
            public void onViewDetachedFromWindow(View v) {

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    getViewTreeObserver().removeGlobalOnLayoutListener(layoutListener);
                }
                else {
                    getViewTreeObserver().removeOnGlobalLayoutListener(layoutListener);
                }

                hidePopup();

            }
        });

    }



    public void showPopup() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                bubble.setX(getHorizontalOffset(SeekBarHint.this.getProgress()));
                bubble.setText(daily_usages[SeekBarHint.this.getProgress()]);
                bubble.setVisibility(View.VISIBLE);
            }
        });
    }



    public void hidePopup() {
        handler.removeCallbacksAndMessages(null);
        bubble.setVisibility(View.INVISIBLE);
    }



    private int getHorizontalOffset(int progress) {
        int max= this.getMax();

        float offset = (float) (this.getThumbOffset() * 4);

        float percent = ((float) progress) / (float) max;

        float width = this.getWidth();

        float progXwidth = progress * width;

        float result = (progXwidth / max) - (offset * percent);

        float bubble_position = this.getX() + result - (bubble.getWidth() / 2) + (float)(this.getThumbOffset() * 2);

        bubble_position = bubble_position + ((bubble.getWidth() / 2) * ((max - progress) / max));

        return (int) bubble_position;
    }



    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        bubble.setText(daily_usages[progress]);
        bubble.setX(getHorizontalOffset(this.getProgress()));
    }



    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }



    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

        if (!screenTimeMode) {
            app.updateWeekdaysUsage(days, daily_usages_formatted[seekBar.getProgress()]);

            RequestSendAppList requestSendAppList = new RequestSendAppList();
            requestSendAppList.setProfile_id(ChildLockedAppsActivity.child.getProfile_id());

            ArrayList<App> apps = new ArrayList<>();
            apps.add(app);

            requestSendAppList.setApps(apps);

            new ParentTask(mContext, SeekBarHint.this).sendLockedAndUnlockedApps(requestSendAppList);
            return;
        }


        child.getScreenLimitByWeekday(days).setUsage(daily_usages_formatted[seekBar.getProgress()]);

        RequestScreenLimit screen_request = new RequestScreenLimit(child.getProfile_id(), child.getDaily_usages());
        new ChildTask(getContext(), this).updateScreenLimit(screen_request);

    }


    private void performUpdateScreenTime(Object responseObject){

        ResponseDefault response = (ResponseDefault) responseObject;

        if (response.isSuccess()){
            Log.i("SCREENTIME", "New screen time limit setup.");
        }

    }


    private void performLockunLockApps(Object responseObject){

        ResponseNotification response = (ResponseNotification) responseObject;

        if (response.isSuccess()){
            adapter.notifyDataSetChanged();
        }

    }


    @Override
    public void performTask(Object responseObject, String methodName) {
        JLog.json(new Gson().toJson(responseObject));

        if (methodName.equals(CREATE_SCREEN_LIMIT)){
            performUpdateScreenTime(responseObject);
        }
        else if (methodName.equals(SEND_LOCKED_AND_UNLOCKED_APPS)){
            performLockunLockApps(responseObject);
        }

    }

}