package com.wordkik.views;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.wordkik.R;
import com.wordkik.objects.Feedback;
import com.wordkik.objects.ResponseFeedback;
import com.wordkik.tasks.ParentTask;
import com.wordkik.utils.AnimateFeedback;
import com.wordkik.utils.ResourceManager;

/**
 * Created by heitorzc on 09/02/16.
 */
public class FeedbackDialog implements View.OnClickListener, AnimateFeedback.AnimateInterface, ParentTask.TaskListener{

    Activity context;
    ImageView sad;
    ImageView neutral;
    ImageView happy;

    String satisfaction = "";
    String message = "";

    AlertDialog alertDialog;

    public FeedbackDialog(Context context) {
        this.context = (Activity) context;
    }

    public void show(){

        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.feedback_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(dialoglayout);

        alertDialogBuilder.setTitle(context.getString(R.string.feedback_title));

        final TableLayout tlSatisfaction = (TableLayout) dialoglayout.findViewById(R.id.satisfaction);
        final EditText etFeedback = (EditText) dialoglayout.findViewById(R.id.etFeedback);

        sad = (ImageView) dialoglayout.findViewById(R.id.sad);
        neutral = (ImageView) dialoglayout.findViewById(R.id.neutral);
        happy = (ImageView) dialoglayout.findViewById(R.id.happy);

        sad.setOnClickListener(this);
        neutral.setOnClickListener(this);
        happy.setOnClickListener(this);

        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setNegativeButton(context.getString(R.string.text_cancel), null);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.send), null);

        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        alertDialog.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                message = etFeedback.getText().toString();

                if (satisfaction.length() == 0) {
                    YoYo.with(Techniques.Shake).duration(600).playOn(tlSatisfaction);
                }
                else if (message.length() == 0){
                    YoYo.with(Techniques.Shake).duration(600).playOn(etFeedback);
                }
                else {
                    Feedback feedback = new Feedback(satisfaction, message);
                    new ParentTask(context, FeedbackDialog.this).sendFeedback(feedback);
                }

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sad:
                AnimateFeedback.anime(v, 0, 400, Techniques.Tada, this);
                break;
            case R.id.neutral:
                AnimateFeedback.anime(v, 0, 400, Techniques.Tada, this);
                break;
            case R.id.happy:
                AnimateFeedback.anime(v, 0, 400, Techniques.Tada, this);
                break;
        }
    }

    @Override
    public void onAnimationFinish(View v) {

        switch (v.getId()){
            case R.id.sad:
                ((ImageView) v).setImageDrawable(ResourceManager.getInstance().drawable(R.drawable.sad_small));
                neutral.setImageDrawable(ResourceManager.getInstance().drawable(R.drawable.indifferent_big));
                happy.setImageDrawable(ResourceManager.getInstance().drawable(R.drawable.happy_big));
                satisfaction = "Sad";
                break;
            case R.id.neutral:
                ((ImageView) v).setImageDrawable(ResourceManager.getInstance().drawable(R.drawable.indifferent_small));
                sad.setImageDrawable(ResourceManager.getInstance().drawable(R.drawable.sad_big));
                happy.setImageDrawable(ResourceManager.getInstance().drawable(R.drawable.happy_big));
                satisfaction = "Neutral";
                break;
            case R.id.happy:
                ((ImageView) v).setImageDrawable(ResourceManager.getInstance().drawable(R.drawable.happy_small));
                neutral.setImageDrawable(ResourceManager.getInstance().drawable(R.drawable.indifferent_big));
                sad.setImageDrawable(ResourceManager.getInstance().drawable(R.drawable.sad_big));
                satisfaction = "Happy";
                break;
        }

    }

    @Override
    public void performTask(Object responseObject, String methodName) {
        ResponseFeedback responseFeedback = (ResponseFeedback) responseObject;

        if (responseFeedback.isSuccess()) {

            ConfirmationDialog.with(context)
                    .title(R.string.feedback_received_title)
                    .message(R.string.feedback_sent)
                    .background(R.color.green)
                    .icon(R.drawable.tick).show();

            alertDialog.dismiss();

        }
        else {

            Toast.makeText(context, "Feedback Failure", Toast.LENGTH_SHORT).show();

        }
    }
}
