package com.wordkik.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.utils.AccountManager;
import com.wordkik.utils.AnimateFeedback;
import com.wordkik.utils.ResourceManager;


/**
 * Created by heitorzc on 22/03/16.
 */
public class WeekDayTextView extends TextView implements View.OnClickListener, AnimateFeedback.AnimateInterface{

    boolean isSelected = false;
    int defaultPadding;
    int pressedPadding;


    public WeekDayTextView(Context context) {
        super(context);
        getPaddings();
        setBackground();
        this.setOnClickListener(this);
    }


    public WeekDayTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getPaddings();
        setBackground();
        this.setOnClickListener(this);
    }


    public WeekDayTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getPaddings();
        setBackground();
        this.setOnClickListener(this);
    }


    @SuppressLint("NewApi")
    private void setBackground() {

        AnimateFeedback.anime(this, 0, 400, Techniques.Tada, this);

        if (isSelected) {
            ResourceManager.getInstance().setBackgroundDrawable(this, R.drawable.green_button_grey_stroke__rounded_4dp);
            this.setTextColor(ResourceManager.getInstance().color(R.color.white));
        }
        else {
            ResourceManager.getInstance().setBackgroundDrawable(this, R.drawable.lightgrey_button_grey_stroke__rounded_4dp);
            this.setTextColor(ResourceManager.getInstance().color(R.color.Grey));
        }
    }


    public void getPaddings(){
        defaultPadding = this.getPaddingTop();
        pressedPadding = defaultPadding - 5;
    }

    public void select(){
        this.isSelected = true;
        setBackground();
        AccountManager.getInstance(getContext()).decrementTimeLockFromLimit();

    }



    @Override
    public void onClick(View v) {

        boolean allow = (isSelected) ? AccountManager.getInstance(getContext()).incrementTimeLockToLimit() : AccountManager.getInstance(getContext()).decrementTimeLockFromLimit();

        if (allow) {
            isSelected ^= true;
            setBackground();
        }

    }


    @Override
    public void onAnimationFinish(View v) {
        if (isSelected) {
            this.setPadding(0, pressedPadding, 0, pressedPadding);
        }
        else {
            this.setPadding(0, defaultPadding, 0, defaultPadding);
        }
    }
}
