package com.wordkik.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.TextView;

import com.wordkik.R;

import static android.view.animation.Animation.RELATIVE_TO_SELF;


/**
 * Created by heitorzc on 22/03/16.
 */
public class OpenSansLightTextView extends TextView {

    Animation blink;

    public OpenSansLightTextView(Context context) {
        super(context);
        setTypeFace(context);
    }

    public OpenSansLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setTypeFace(context);
    }

    public OpenSansLightTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setTypeFace(context);
    }


    private void setTypeFace(Context context){
        Typeface regular  = Typeface.createFromAsset(context.getAssets(), "fonts/opensanslight.ttf");
        this.setTypeface(regular);

        blink  = AnimationUtils.loadAnimation(context, R.anim.blink);

    }

    public void blink(){
        this.startAnimation(blink);
    }

    public void stopBlinking(){
        this.clearAnimation();
    }

    public void setTextAnimated(String text){
        this.setText(text);

        AlphaAnimation fadeIn = new AlphaAnimation(0.0f, 1.0f);
        fadeIn.setDuration(500);
        fadeIn.setFillAfter(true);

        this.setAnimation(fadeIn);
    }


}
