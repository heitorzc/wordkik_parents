package com.wordkik.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


/**
 * Created by heitorzc on 22/03/16.
 */
public class OpenSansSemiTextView extends TextView {


    public OpenSansSemiTextView(Context context) {
        super(context);
        setTypeFace(context);
    }

    public OpenSansSemiTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setTypeFace(context);
    }

    public OpenSansSemiTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeFace(context);
    }


    private void setTypeFace(Context context){
        Typeface regular  = Typeface.createFromAsset(context.getAssets(), "fonts/opensanssemibold.ttf");
        this.setTypeface(regular);
    }


}
