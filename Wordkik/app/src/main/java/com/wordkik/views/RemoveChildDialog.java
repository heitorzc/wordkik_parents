package com.wordkik.views;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.adapters.ChildProfileAdapter;
import com.wordkik.objects.Child;
import com.wordkik.objects.ResponseChild;
import com.wordkik.tasks.ChildTask;
import com.wordkik.tasks.TaskManager;

/**
 * Created by heitorzc on 09/02/16.
 */
public class RemoveChildDialog implements ChildTask.TaskListener, TaskManager.IMethodName {

    Activity context;
    Child child;
    ChildProfileAdapter adapter;
    TextView empty;

    public RemoveChildDialog(Context context, Child child, ChildProfileAdapter adapter, TextView empty) {
        this.context = (Activity) context;
        this.child = child;
        this.adapter = adapter;
        this.empty = empty;
    }

    public void show(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setTitle(context.getString(R.string.delete_profile));
        alertDialogBuilder.setMessage(context.getString(R.string.remove_child_dialog_message).replace("%", child.getName()));
        alertDialogBuilder.setPositiveButton(context.getString(R.string.text_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new ChildTask(context, RemoveChildDialog.this).removeChild(child);

            }
        });

        alertDialogBuilder.setNegativeButton(context.getString(R.string.text_cancel), null);

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void performTask(Object responseObject, String methodName) {
        Log.e("PERFORM_TASK", "METHOD: " + methodName + " CLASS: " + RemoveChildDialog.class);

        switch (methodName) {
            case REMOVE_CHILD:
                performRemoveChild(responseObject);
                break;
            default:
                Log.e("PERFORM_TASK", "METHOD NOT IMPLEMENTED");
                break;
        }
    }

    private void performRemoveChild(Object responseObject) {
        ResponseChild responseChild = (ResponseChild) responseObject;

        if (responseChild.isSuccess()) {
            adapter.getItems().remove(child);
            adapter.update();
        }

        if (Constants.childs.size() > 0){
            empty.setVisibility(View.INVISIBLE);
        } else {
            empty.setVisibility(View.VISIBLE);
        }
    }
}
