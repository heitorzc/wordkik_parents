package com.wordkik.views;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.wordkik.R;

/**
 * Created by heitorzc on 09/02/16.
 */
public class UploadDialog {

    Activity context;
    AlertDialog alertDialog;
    ProgressBar progressBar;

    public UploadDialog(Context context) {
        this.context = (Activity) context;
    }


    /**
     * Method called to read child`s QR Code
     */
    public void show(){

        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.upload_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(dialoglayout);

        progressBar = (ProgressBar) dialoglayout.findViewById(R.id.progressBar);

        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }

    public void setProgress(int progress){
        progressBar.setProgress(progress);
    }

    public void dismiss(){
        alertDialog.dismiss();
    }
}
