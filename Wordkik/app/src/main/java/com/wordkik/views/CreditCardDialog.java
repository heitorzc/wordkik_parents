package com.wordkik.views;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.devmarvel.creditcardentry.library.CreditCard;
import com.devmarvel.creditcardentry.library.CreditCardForm;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.activities.MainActivity;
import com.wordkik.objects.CardPurchase;
import com.wordkik.objects.Plan;
import com.wordkik.objects.ResponsePurchase;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;
import com.wordkik.utils.AccountManager;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by heitorzc on 09/02/16.
 */
public class CreditCardDialog implements TaskManager.TaskListener{
    @Bind(R.id.loading)             RelativeLayout loading;
    @Bind(R.id.errorMessage)        TextView errorMessage;
    @Bind(R.id.credit_card_form)    CreditCardForm card_form;
    @Bind(R.id.tvPlanName)          TextView tvPlanName;
    @Bind(R.id.tvDiscount)          TextView tvDiscount;
    @Bind(R.id.tvTotal)             TextView tvTotal;
    @Bind(R.id.tvPlanPrice)         TextView tvPlanPrice;
    @Bind(R.id.bottomNote)          TextView bottomNote;

    Activity context;
    AlertDialog alertDialog;

    Plan userPlan;
    String promo_code;

    Button positive;
    Button negative;

    public CreditCardDialog(Context context, Plan userPlan, String promo_code) {
         this.context = (Activity) context;
         this.userPlan = userPlan;
         this.promo_code = promo_code;

    }


    public void show(){

        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.credit_card_info, null);
        ButterKnife.bind(this, dialoglayout);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(dialoglayout);
        alertDialogBuilder.setTitle(R.string.payment_dialog_title);

        calculateDiscount();
        bottomNote.setText(bottomNote.getText().toString().replace("%", getCurrency()));

        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setNegativeButton(context.getString(R.string.text_cancel), null);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.text_ok), null);

        alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        positive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
        negative = alertDialog.getButton(Dialog.BUTTON_NEGATIVE);

        positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLoading();
                validateCardWithStripe();
            }
        });

        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }

    private void calculateDiscount(){
        float planPrice = Float.parseFloat(userPlan.getPrice().replace(",", "."));
        int   discount =  (promo_code.length() > 0) ? Integer.parseInt(promo_code.replaceAll("\\D+","")) : 0;
        String discountString = (discount > 0) ? discount + "%" : "--";
        float discountPrice = planPrice - ((discount * planPrice) / 100);

        tvPlanName.setText("WordKik " + userPlan.getTitle());
        tvPlanPrice.setText(userPlan.getPrice());
        tvDiscount.setText(discountString);
        tvTotal.setText(String.format("%.2f", discountPrice));
    }

    private void validateCardWithStripe(){
        CreditCard creditCard = card_form.getCreditCard();
        Card card = new Card(creditCard.getCardNumber(), creditCard.getExpMonth(), creditCard.getExpYear(),creditCard.getSecurityCode());


        try {
            Stripe stripe = new Stripe("pk_live_wkAjxRMjz3zArPtsfkrY5n8B");
            stripe.createToken(card, StripeTokenCallback());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getCurrency(){
        String symbol = userPlan.getCurrency();
        if (symbol.equals("R$")){
            return context.getString(R.string.brazilian_real);
        }
        else if (symbol.equals("CAD")){
            return context.getString(R.string.canadian_dollar);
        }
        else if (symbol.equals("AUD")){
            return context.getString(R.string.australian_dollar);
        }
        else if (symbol.equals("US$")){
            return context.getString(R.string.american_dollar);
        }
        else{
            return context.getString(R.string.currency_euro);
        }
    }

    private TokenCallback StripeTokenCallback(){
        return new TokenCallback() {
            public void onSuccess(Token token) {
                CardPurchase purchase;

                String coupon = (promo_code != null && promo_code.length() > 0) ? promo_code : null;
                purchase  = new CardPurchase(token.getId(), userPlan.getStripe_id(), coupon);

                new ParentTask(context, CreditCardDialog.this).sendStripeSubscription(purchase);
            }

            public void onError(Exception error) {
                stopLoading();
                Toast.makeText(context, R.string.error_validate_card, Toast.LENGTH_LONG).show();
            }

        };
    }

    private void startLoading(){
        alertDialog.setTitle("");
        positive.setVisibility(View.INVISIBLE);
        negative.setVisibility(View.INVISIBLE);
        loading.setVisibility(View.VISIBLE);
    }

    private void stopLoading(){
        alertDialog.setTitle(R.string.payment_dialog_title);
        positive.setVisibility(View.VISIBLE);
        negative.setVisibility(View.VISIBLE);
        loading.setVisibility(View.INVISIBLE);
        errorMessage.setVisibility(View.VISIBLE);
        card_form.clearForm();
    }


    @Override
    public void performTask(Object responseObject, String methodName) {
        ResponsePurchase responsePurchase = (ResponsePurchase) responseObject;

        if (responsePurchase.isSuccess()) {
            Constants.user_account_type = AccountManager.ACCOUNT_PREMIUM;
            Constants.user_expiration = responsePurchase.getSubscription_left();
            Constants.isSubscriptionValid = true;
            AccountManager.getInstance(context).updateAccountInfo();

            ((WordKik) context.getApplicationContext()).mpTrack("Stripe Payment Completed");
            this.alertDialog.dismiss();

            MainActivity.subscription_purchased = true;
            context.finish();
        }
        else{
            stopLoading();
        }
    }
}
