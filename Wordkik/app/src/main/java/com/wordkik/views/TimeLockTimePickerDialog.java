package com.wordkik.views;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.widget.TextView;

import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

/**
 * Created by heitorzc on 09/02/16.
 */
public class TimeLockTimePickerDialog implements RadialTimePickerDialogFragment.OnTimeSetListener{

    Activity context;
    FragmentManager fragmentManager;
    TextView tv;

    public TimeLockTimePickerDialog(Context context, FragmentManager fragmentManager, TextView tv) {
        this.context = (Activity) context;
        this.fragmentManager = fragmentManager;
        this.tv = tv;
    }

    public void setDateTime(){
        RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                .setOnTimeSetListener(TimeLockTimePickerDialog.this)
                .setForced24hFormat();

        if (tv.getText().toString().contains(":")){
            rtpd.setStartTime(Integer.parseInt(tv.getText().toString().substring(0, 2)), Integer.parseInt(tv.getText().toString().substring(3, 5)));
        }

        rtpd.show(fragmentManager, null);
    }

    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {

        String hour = (hourOfDay < 10) ? "0" + hourOfDay : ""+ hourOfDay;
        String min = (minute < 10) ? "0" + minute : "" + minute;

        tv.setText(hour + ":" + min);
    }
}
