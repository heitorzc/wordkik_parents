package com.wordkik.views;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.objects.Child;
import com.wordkik.tasks.AmazonS3Uploader;

import java.net.URL;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by heitorzc on 10/10/16.
 */
public class ManageChildDialog extends AlertDialog.Builder implements AmazonS3Uploader.AmazonS3UploaderListener{
    @Nullable @Bind(R.id.ivPhoto)     CircularImageView ivPhoto;
    @Nullable @Bind(R.id.etFullName)  EditText etName;
    @Nullable @Bind(R.id.spAge)       Spinner spAge;
    @Nullable @Bind(R.id.rgGender)    RadioGroup rgGender;


    @Nullable @OnClick(R.id.ivPhoto)
    public void onClickPickPhoto(){
        WordKik.callPhotoPicker((Activity) context, qr_code, ivPhoto, this);
    }

    ChildDialogListener listener;
    private static ManageChildDialog instance;

    private Context context;
    private String qr_code;
    private String photo;

    private View customView;

    public interface ChildDialogListener{
        void onDialogDismissed();
    }



    public ManageChildDialog(@NonNull Context context) {
        super(context);
        this.context = context;
        instance = this;
    }



    /**
     * Generates a new dialog to be shown to the user.
     * @param layoutId is the layout to be used on the dialog.
     * @param positiveText is the text for the positive button.
     * @param negativeText is the text for the negative button.
     * @return an AlertDialog object.
     */
    public AlertDialog buildDialog(int layoutId, int positiveText, int negativeText){

        View dialoglayout = View.inflate(context, layoutId, null);
        this.customView = dialoglayout;
        this.setView(dialoglayout);
        ButterKnife.bind(this, dialoglayout);

        this.setCancelable(true);
        this.setNegativeButton(negativeText, null);
        this.setPositiveButton(positiveText, null);

        this.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (listener != null){
                    listener.onDialogDismissed();
                }
            }
        });

        return this.create();

    }



    /**
     * @return The instance of the current dialog.
     */
    public static ManageChildDialog getInstance(){
        return instance;
    }



    public View getCustomView(){
        return customView;
    }



    /**
     * @return the child's QR Code (profile_id)
     */
    public String getQRCode(){
        return (qr_code != null) ? qr_code : "";
    }



    /**
     * Sets a new profile_id for the child.
     * @param code is the QR Code to be used as profile_id.
     */
    public void setQRCode(String code){
        this.qr_code = code;
    }



    public void setOnDialogDismissListener(ChildDialogListener listener){
        this.listener = listener;
    }



    /**
     * Populate all the child's information on the layout
     * to allow the user to edit them.
     */
    public void loadChildInfo(Child child){

        qr_code = child.getProfile_id();
        photo = child.getPhoto();

        Picasso.with(context).load(photo)
                .error(R.drawable.add_photo_green)
                .placeholder(R.drawable.add_photo_green).into(ivPhoto);

        etName.setText(child.getName());
        spAge.setSelection(getChildAge(child.getAge()));

        if (child.getGender().equals("Boy")){
            rgGender.check(R.id.rbBoy);
        }
        else {
            rgGender.check(R.id.rbGirl);
        }
    }



    /**
     * @return The name of the child.
     */
    public String getName() {
        return etName.getText().toString();
    }



    /**
     * @return The age group of the child.
     */
    private String getAge() {
        return getSelectedAge(spAge);
    }



    /**
     * @return The gender of the child.
     */
    private String getGender() {
        return checkGender(rgGender);
    }



    /**
     * @return a new Child object using the info displayed in the dialog.
     */
    public Child getNewChild() {

        String new_child_id     = getQRCode();
        String new_child_name   = getName();
        String new_child_photo  = getPhoto();
        String new_child_age    = getAge();
        String new_child_gender = getGender();

        if (!checkNullValues(new_child_name, spAge, new_child_gender)) {
            return new Child(new_child_id, new_child_name, new_child_photo, new_child_gender, new_child_age);
        }

        return null;
    }



    /**
     * @return The photo's URL of the child.
     */
    public String getPhoto() {
        return photo;
    }



    /**
     * Set a new photo for the child.
     * @param photo is the URL to be used. (Returned by Amazon)
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }



    /**
     * Callback for Amazon request returning the URL of the uploaded picture.
     */
    @Override
    public void setPictureURL(URL url, String url2) {
        setPhoto(url2);
    }



    /**
     * Check if child has any null or empty attribute before sending the edit
     * request to the API.
     */
    private boolean checkNullValues(String name, Spinner age, String gender){

        if (name == null || name.trim().isEmpty() || name.equals("")){
            Toast.makeText(context, R.string.child_name_blank, Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (age.getSelectedItemPosition() <= 0){
            Toast.makeText(context, R.string.age_blank, Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (gender == null || gender.trim().isEmpty() || gender.equals("")){
            Toast.makeText(context, R.string.gender_blank, Toast.LENGTH_SHORT).show();
            return true;
        }
        else {
            return false;
        }
    }



    /**
     * The following methods are used to convert the data received from the API
     * into data readable to the user on it's own language and vice-versa.
     */
    private int getChildAge(String age){
        if (age.equals("Less than 5")){
            return 1;
        }
        else if (age.equals("5 to 8")){
            return 2;
        }

        else if (age.equals("8 to 11")){
            return 3;
        }

        else if (age.equals("11 to 13")){
            return 4;
        }

        else {
            return 5;
        }
    }


    private String getSelectedAge(Spinner spinner){
        int selected = spinner.getSelectedItemPosition();
        String age = "";

        switch (selected){
            case 1:
                age = "Less than 5";
                break;
            case 2:
                age = "5 to 8";
                break;
            case 3:
                age = "8 to 11";
                break;
            case 4:
                age = "11 to 13";
                break;
            case 5:
                age = "More than 13";
                break;
        }

        return age;
    }


    private String checkGender(RadioGroup rgGender){
        String gender = null;
        switch (rgGender.getCheckedRadioButtonId()){
            case R.id.rbBoy:
                gender =  "Boy";
                break;
            case R.id.rbGirl:
                gender =  "Girl";
                break;
        }
        return gender;
    }


}
