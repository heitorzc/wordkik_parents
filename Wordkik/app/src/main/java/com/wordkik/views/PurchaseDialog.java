package com.wordkik.views;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

import com.wordkik.R;
import com.wordkik.activities.PurchaseActivity;

/**
 * Created by heitorzc on 09/02/16.
 */
public class PurchaseDialog {

    public static final int PURCHASE_STATUS_TRIAL_ACTIVE = 0;
    public static final int PURCHASE_STATUS_TRIAL_ENDED = 1;
    public static final int PURCHASE_STATUS_SUBSCRIPTION_ENDED = 2;

    Activity context;

    public PurchaseDialog(Context context) {
        this.context = (Activity) context;
    }

    public void show(int status){

        String title = null;
        String customMessage = null;
        String defaultMessage = context.getResources().getString(R.string.defaultMessage);

        switch (status){
            case PURCHASE_STATUS_TRIAL_ACTIVE :
                title = context.getString(R.string.title_trial_active);
                customMessage = context.getString(R.string.customMessage_trial_active);
                break;
            case PURCHASE_STATUS_TRIAL_ENDED :
                title = context.getString(R.string.title_trial_ended);
                customMessage = context.getString(R.string.customMessage_trial_ended);
                break;
            case PURCHASE_STATUS_SUBSCRIPTION_ENDED :
                title = context.getString(R.string.title_subscription_ended);
                customMessage = context.getString(R.string.customMessage_subscription_ended);
                break;
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(customMessage + defaultMessage);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.purchase_dialog_button_positive), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                buyNow();
            }
        });

        alertDialogBuilder.setNegativeButton(context.getString(R.string.purchase_dialog_button_negative), null);

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void buyNow(){
        context.startActivity(new Intent(context, PurchaseActivity.class));
    }
}
