package com.wordkik.views;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.widget.TextView;

import com.wordkik.R;
import com.wordkik.utils.ResourceManager;


/**
 * Created by heitorzc on 22/03/16.
 */
public class DailyUsageTextView extends TextView {

    String[] daily_usages_formatted = getResources().getStringArray(R.array.daily_usage_formatted);
    String[] daily_usages           = getResources().getStringArray(R.array.daily_usage);


    public DailyUsageTextView(Context context) {
        super(context);

    }



    public DailyUsageTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

    }



    public DailyUsageTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }



    private void setNoUsageLimit(){
        this.setText(R.string.tap_create_limit);
    }



    public void setColorByUsage(String wkUsage, String weUsage){

        if (hasUsageLimit(wkUsage, weUsage)) {

            Spannable span;

            if (isLockedAllWeek(wkUsage, weUsage)){
                String word = getResources().getString(R.string.locked_everyday);
                span = new SpannableString(word);

                span.setSpan(new ForegroundColorSpan(ResourceManager.getInstance().color(R.color.red)), 0, span.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            else{

                String word = getResources().getString(R.string.weekdays_weekends).replace("%D", getLabelForUsage(wkUsage)).replace("%E", getLabelForUsage(weUsage));
                span = new SpannableString(word);

                int d_start = 0;
                int d_end = word.indexOf(":") + 1;

                int e_start = word.indexOf(getResources().getString(R.string.weekends));
                int e_end = word.indexOf(":", e_start) + 1;


                span.setSpan(new StyleSpan(Typeface.BOLD), d_start, d_end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                span.setSpan(new StyleSpan(Typeface.BOLD), e_start, e_end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

            this.setText(span);
        }
        else {
            setNoUsageLimit();
        }

    }



    private boolean hasUsageLimit(String wkUsage, String weUsage){
        if (wkUsage.equals("Unlocked") && weUsage.equals("Unlocked")){
            return false;
        }

        return true;
    }



    private boolean isLockedAllWeek(String wkUsage, String weUsage){
        if (wkUsage.equals("Locked") && weUsage.equals("Locked")){
            return true;
        }

        return false;
    }



    private String getLabelForUsage(String usage){
        for (int i = 0; i < daily_usages_formatted.length; i++){

            if (daily_usages_formatted[i].equals(usage)){
                String label = (usage.equals("Locked") || usage.equals("Unlocked")) ? daily_usages[i] : daily_usages[i] + " " + getResources().getString(R.string.per_day);
                return label;
            }

        }

        return usage;
    }

}
