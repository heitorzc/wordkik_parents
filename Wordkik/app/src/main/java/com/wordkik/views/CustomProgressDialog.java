package com.wordkik.views;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;

import com.wordkik.R;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by heitorzc on 03/02/16.
 */
public class CustomProgressDialog extends ProgressDialog {
    @Bind(R.id.tvText)    TextView tvText;

    String text;

    public CustomProgressDialog(Context context) {
        super(context);
    }

    public CustomProgressDialog(Context context, String text) {
        super(context);
        this.text = text;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progress_dialog);
        ButterKnife.bind(this);

        tvText.setText(text);
    }

    @Override
    public void show() {
        super.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        super.show();
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        }
        catch (IllegalArgumentException e){
            Log.e("CustomProgress", "Impossible to dismiss");
        }
    }
}
