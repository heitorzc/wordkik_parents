package com.wordkik.views;

import android.app.Activity;
import android.app.Dialog;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.triggertrap.seekarc.SeekArc;
import com.wordkik.R;
import com.wordkik.activities.ChildTimeLocksActivity;
import com.wordkik.adapters.TimeLocksAdapter;
import com.wordkik.objects.ResponseTimeLock;
import com.wordkik.objects.TimeLock;
import com.wordkik.tasks.ParentTask;

import java.util.Arrays;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by heitorzc on 09/02/16.
 */
public class TimeLockSetupDialog implements TextView.OnClickListener, ParentTask.TaskListener{
    @Bind(R.id.tvLockTime)    TextView tvLockTime;
    @Bind(R.id.tvUnlockTime)  TextView tvUnlockTime;
    @Bind(R.id.sun)           WeekDayTextView sun;
    @Bind(R.id.mon)           WeekDayTextView mon;
    @Bind(R.id.tue)           WeekDayTextView tue;
    @Bind(R.id.wed)           WeekDayTextView wed;
    @Bind(R.id.thu)           WeekDayTextView thu;
    @Bind(R.id.fri)           WeekDayTextView fri;
    @Bind(R.id.sat)           WeekDayTextView sat;

    Activity context;
    TimeLockSetupDialogInterface listener;
    FragmentManager fragmentManager;
    TimeLocksAdapter adapter;
    TimeLock timeLock;
    String type = "";

    String TAG = "Created Time Lock";

    AlertDialog alertDialog;

    public TimeLockSetupDialog(Activity context, FragmentManager fragmentManager, TimeLocksAdapter adapter) {
        this.context = (Activity) context;
        this.fragmentManager = fragmentManager;
        this.adapter = adapter;
        this.listener = (TimeLockSetupDialogInterface) context;

        timeLock = new TimeLock();
    }

    public interface TimeLockSetupDialogInterface{
        void onTimeLockCreated();
    }

    public void defineType(final int activeTab){
        final RadioGroup rgType;
        final EditText etCustom;

        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.time_lock_add_type_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(dialoglayout);
        alertDialogBuilder.setTitle(context.getResources().getString(R.string.defineTypeTitle));

        etCustom = (EditText) dialoglayout.findViewById(R.id.etCustom);

        rgType = (RadioGroup) dialoglayout.findViewById(R.id.rgLockType);
        rgType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbHomework:
                        type = context.getResources().getString(R.string.tvHomework);
                        etCustom.setVisibility(View.GONE);
                        break;
                    case R.id.rbDinner:
                        type = context.getResources().getString(R.string.tvDinner);
                        etCustom.setVisibility(View.GONE);
                        break;
                    case R.id.rbBed:
                        type = context.getResources().getString(R.string.tvBed);
                        etCustom.setVisibility(View.GONE);
                        break;
                    case R.id.rbCustom:
                        type = "";
                        etCustom.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setNegativeButton(context.getString(R.string.text_cancel), null);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.tvNext), null);

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        alertDialog.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("")){
                    if (etCustom.getText().toString().length() == 0){
                        Toast.makeText(context, R.string.timelock_type_msg, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        type = etCustom.getText().toString();
                        defineTime(activeTab);
                        alertDialog.dismiss();
                    }
                }
                else {
                    defineTime(activeTab);
                    alertDialog.dismiss();
                }
            }
        });
    }

    public void defineTime(int activeTab){

        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.time_lock_add_time_dialog, null);
        ButterKnife.bind(this, dialoglayout);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(dialoglayout);
        alertDialogBuilder.setTitle(type);

        tvLockTime.setOnClickListener(this);
        tvUnlockTime.setOnClickListener(this);

        selectWeekDayFromTabPosition(activeTab);

        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setNegativeButton(context.getString(R.string.text_cancel), null);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.text_ok), null);

        alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        alertDialog.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lockTime = tvLockTime.getText().toString();
                String unlockTime = tvUnlockTime.getText().toString().replace("h ", ":").replace("m", "");

                Log.w("TIMELOCK", "Lock at: " + lockTime + " Lock for: " + unlockTime);

                if (checkTimes(lockTime, unlockTime)) {
                    createTimeLocks(lockTime);
                }
            }
        });
    }

    public void selectWeekDayFromTabPosition(int activeTab){

        switch (activeTab) {
            case 0:
                sun.select();
                break;
            case 1:
                mon.select();
                break;
            case 2:
                tue.select();
                break;
            case 3:
                wed.select();
                break;
            case 4:
                thu.select();
                break;
            case 5:
                fri.select();
                break;
            case 6:
                sat.select();
                break;
        }

    }

    public void createTimeLocks(String lockTime)  {

        timeLock.activate();
        timeLock.setStart(lockTime);
        timeLock.setDescription(type);
        timeLock.setProfile_id(ChildTimeLocksActivity.child.getProfile_id());

        if (sun.isSelected) {
            timeLock.getDays().add(0);
        }

        if (mon.isSelected) {
            timeLock.getDays().add(1);
        }

        if (tue.isSelected) {
            timeLock.getDays().add(2);
        }

        if (wed.isSelected) {
            timeLock.getDays().add(3);
        }

        if (thu.isSelected) {
            timeLock.getDays().add(4);
        }

        if (fri.isSelected) {
            timeLock.getDays().add(5);
        }

        if (sat.isSelected) {
            timeLock.getDays().add(6);
        }

        new ParentTask(context, this).createTimeLock(timeLock);

    }

    public void defineDuration(final TextView textView){

        final String[] lock_for = context.getResources().getStringArray(R.array.lock_for);
        final String[] lock_for_time_formated = context.getResources().getStringArray(R.array.lock_for_time_formated);

        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.time_lock_add_duration_dialog, null);

        final SeekArc seekArc = (SeekArc) dialoglayout.findViewById(R.id.seekArc);
        final TextView value = (TextView) dialoglayout.findViewById(R.id.value);

        seekArc.setProgress(0);
        value.setText(lock_for[0]);

        seekArc.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {
            @Override
            public void onProgressChanged(SeekArc seekArc, int i, boolean b) {
                value.setText(lock_for[i]);
            }

            @Override
            public void onStartTrackingTouch(SeekArc seekArc) {

            }

            @Override
            public void onStopTrackingTouch(SeekArc seekArc) {

            }
        });

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(dialoglayout);
        alertDialogBuilder.setTitle(type);

        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setNegativeButton(context.getString(R.string.text_cancel), null);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.text_ok), null);

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        alertDialog.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(value.getText().toString());
                timeLock.setDuration(lock_for_time_formated[seekArc.getProgress()]);
                alertDialog.dismiss();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvLockTime:
                new TimeLockTimePickerDialog(context, fragmentManager, (TextView) v).setDateTime();
                break;
            case R.id.tvUnlockTime:
                defineDuration((TextView) v);
                break;
        }

    }

    @Override
    public void performTask(Object responseObject, String methodName) {
        ResponseTimeLock responseTimeLock = (ResponseTimeLock) responseObject;

        if (responseTimeLock.isSuccess()){

            if (responseTimeLock.getMessage().equals("Overlap detected")) {
                ConfirmationDialog.with(context)
                        .title(R.string.timelock_overlap_title)
                        .message(R.string.timelock_overlap_msg)
                        .background(R.color.red)
                        .show();
            }

            else if (responseTimeLock.getMessage().equals("Created time locks with overlap detected")){

                String[] weekdaysArray = context.getResources().getStringArray(R.array.weekdays);
                String[] weekdays = new String[responseTimeLock.getOverlapping().size()];

                for (int i = 0; i < responseTimeLock.getOverlapping().size(); i++){
                    weekdays[i] = weekdaysArray[responseTimeLock.getOverlapping().get(i).getWeek_day()];
                }

                ConfirmationDialog.with(context)
                        .title(R.string.timelock_overlap_title)
                        .message(context.getString(R.string.timelock_overlap_created_msg)
                                .replace("%", Arrays.toString(weekdays).replaceAll("[\\[\\]]", "").replace(",", " /")))
                        .background(R.color.red)
                        .show();

                alertDialog.dismiss();

            }

            ChildTimeLocksActivity.child.setTimeLocks(responseTimeLock.getTimelocks());
            alertDialog.dismiss();

            if (listener != null) {
                listener.onTimeLockCreated();
            }

        }
        else {
            Toast.makeText(context, R.string.timelock_error_msg, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkTimes(String lockTime, String unlockTime){

        Pattern regex = Pattern.compile("[1234567890]");
        if (lockTime.contains(":") && regex.matcher(unlockTime).find()){
            return true;
        }
        else{
            Toast.makeText(context, context.getResources().getString(R.string.timeNull), Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
