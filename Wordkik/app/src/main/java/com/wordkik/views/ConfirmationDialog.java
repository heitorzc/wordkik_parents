package com.wordkik.views;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.wordkik.R;
import com.wordkik.objects.Child;
import com.wordkik.utils.ResourceManager;


/**
 * Created by heitorzc on 09/02/16.
 *
 * Basic structure to show a dialog message:
 * ConfirmationDialog.with(CONTEXT)
 *     .title(Could be int or String here)
 *     .message(Could be int or String here as well)
 *     .background(R.color.your_color)
 *     .icon(R.drawable.your_icon) //Could be an URL as well
 *     .show();
 *     -----------------------------------------------------
 *     Extra Options:
 *     .callback(There are 3 types of callbacks)
 *     .negativeButtonLabel(Could be int or String here)
 *     .positiveButtonLabel(Could be int or String here as well)
 *     .gravity(int text align)
 *     .circleIcon(boolean)
 *     .tag(String to identify your dialog on callback)
 */
public class ConfirmationDialog {

    public static final int EMAIL_TAKEN                 = 100;
    public static final int EMAIL_NOT_FOUND             = 110;
    public static final int PASSWORD_INCORRECT          = 120;
    public static final int EMAIL_NOT_ACTIVATED         = 130;
    public static final int REGISTER_COMPLETED          = 140;
    public static final int REGISTER_FAIL               = 150;
    public static final int PROFILE_NOT_COMPLETED       = 160;
    public static final int SHOULD_UNINSTALL_BROWSER    = 170;

    private Activity context;
    private ConfirmationDialogInterface listener;
    private ConfirmDeleteChild delete_listener;
    private ConfirmSaveAppChanges save_listener;
    private ConfirmCouponCode coupon_listener;

    private int text_align             = 0;

    private Child child;

    private int title                  = 0;
    private int message                = 0;
    private int bg_color               = 0;
    private int title_color            = 0;
    private int icon                   = 0;
    private int negative_label         = 0;
    private int positive_label         = 0;

    private String tag                 = "";
    private String url_icon            = "";
    private String text_title          = "";
    private String text_message        = "";

    private boolean persistent         = false;
    private boolean showEditText       = false;
    private boolean showPositiveButton = false;
    private boolean userCircleIcon     = false;
    private boolean useHTML            = false;

    private AlertDialog alertDialog;


    public ConfirmationDialog(Context context){
        this.context = (Activity) context;
    }


    public interface ConfirmationDialogInterface{
        void onPositiveButtonClick(String tag);
    }


    public interface ConfirmDeleteChild{
        void onConfirmDeleteChild(Child child);
    }


    interface ConfirmSaveAppChanges{
        void onPositiveButtonClick();
        void onNegativeButtonClick(ConfirmationDialog dialog);
    }


    public interface ConfirmCouponCode{
        void onCouponEntered(String coupon);
    }


    public void show(){

        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.feedback_confirmation_dialog, null);

        LinearLayout topbar = (LinearLayout) dialoglayout.findViewById(R.id.topbar);
        ImageView tvIcon = (ImageView) dialoglayout.findViewById(R.id.icon);

        OpenSansTextView tvTitle = (OpenSansTextView) dialoglayout.findViewById(R.id.title);
        OpenSansTextView tvMessage = (OpenSansTextView) dialoglayout.findViewById(R.id.message);

        final EditText editText = (EditText) dialoglayout.findViewById(R.id.editText);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(dialoglayout);

        topbar.setBackgroundColor(ResourceManager.getInstance().color(bg_color));

        if (showEditText){
            editText.setVisibility(View.VISIBLE);
        }

        if (positive_label == 0){
            positive_label = R.string.text_ok;
        }

        if (negative_label == 0){
            negative_label = R.string.text_ok;
        }

        if (icon != 0) {
            if (userCircleIcon){
                tvIcon.setVisibility(View.GONE);
                CircularImageView tvCircleIcon = (CircularImageView) dialoglayout.findViewById(R.id.circle_icon);
                tvCircleIcon.setImageDrawable(ResourceManager.getInstance().drawable(icon));
                tvCircleIcon.setVisibility(View.VISIBLE);
            }
            else{
                tvIcon.setImageDrawable(ResourceManager.getInstance().drawable(icon));
            }

        }
        else if (url_icon != null && url_icon.length() > 0){
            if (userCircleIcon){
                tvIcon.setVisibility(View.GONE);
                CircularImageView tvCircleIcon = (CircularImageView) dialoglayout.findViewById(R.id.circle_icon);
                Picasso.with(context).load(url_icon).placeholder(R.drawable.tick).into(tvCircleIcon);
                tvCircleIcon.setVisibility(View.VISIBLE);
            }
            else{
                Picasso.with(context).load(url_icon).placeholder(R.drawable.tick).into(tvIcon);
            }

        }
        else {
            tvIcon.setVisibility(View.GONE);
        }

        if (title != 0){
            tvTitle.setText(context.getString(title));
        }
        else if (text_title.length() > 0){
            tvTitle.setText(text_title);
        }

        if (message != 0){
            if (useHTML) {
                tvMessage.setText(Html.fromHtml(context.getString(message)));
            }
            else{
                tvMessage.setText(context.getString(message));
            }
        }
        else if (text_message.length() > 0){
            if (useHTML){
                tvMessage.setText(Html.fromHtml(text_message));
            }
            else{
                tvMessage.setText(text_message);
            }
        }

        if (text_align != 0){
            tvMessage.setGravity(text_align);
        }

        if (title_color != 0){
            tvTitle.setTextColor(ResourceManager.getInstance().color(title_color));
        }

        alertDialogBuilder.setCancelable(true);

        if (showPositiveButton){
            alertDialogBuilder.setNegativeButton(context.getString(negative_label), null);
            alertDialogBuilder.setPositiveButton(context.getString(positive_label), null);

            alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            alertDialog.getButton(Dialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (save_listener != null) { save_listener.onNegativeButtonClick(ConfirmationDialog.this);}
                    alertDialog.dismiss();
                }
            });

            alertDialog.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (child != null){
                        delete_listener.onConfirmDeleteChild(child);
                    }
                    else {
                        if (save_listener != null) { save_listener.onPositiveButtonClick();}
                        if (listener != null) { listener.onPositiveButtonClick(tag);}
                        if (coupon_listener != null) { coupon_listener.onCouponEntered(editText.getText().toString());}
                    }

                    alertDialog.dismiss();
                }
            });
        }
        else {
            alertDialogBuilder.setNegativeButton(context.getString(negative_label), null);


            alertDialog = alertDialogBuilder.create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();

            alertDialog.getButton(Dialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (save_listener != null) { save_listener.onNegativeButtonClick(ConfirmationDialog.this);}
                    if (listener != null) { listener.onPositiveButtonClick(tag);}
                    persistent = false;
                    alertDialog.dismiss();
                }
            });
        }

        int button_color = (bg_color == R.color.lightestGrey) ? R.color.darkGrey : bg_color;

        alertDialog.getButton(Dialog.BUTTON_NEGATIVE).setTextColor(ResourceManager.getInstance().color(button_color));
        alertDialog.getButton(Dialog.BUTTON_POSITIVE).setTextColor(ResourceManager.getInstance().color(button_color));

        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        YoYo.with(Techniques.FadeIn).delay(0).duration(800).playOn(tvIcon);

        if (persistent){
            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    if (persistent){
                        alertDialog.show();
                    }
                }
            });
        }

    }


    public static ConfirmationDialog with(Activity context){
        return new ConfirmationDialog(context);
    }


    public ConfirmationDialog tag(String tag){
        this.tag = tag;
        return this;
    }


    public String getTag(){
        return this.tag;
    }


    public ConfirmationDialog title(int title){
        this.title = title;
        return this;
    }


    public ConfirmationDialog title(String title){
        this.text_title = title;
        return this;
    }


    public ConfirmationDialog titleColor(int color){
        this.title_color = color;
        return this;
    }


    public ConfirmationDialog message(int message){

        switch (message){
            case EMAIL_TAKEN:
                this.title =   R.string.emailTakenTitle;
                this.message = R.string.emailTakenError;
                break;
            case EMAIL_NOT_FOUND:
                this.title =   R.string.invalidEmailTitle;
                this.message = R.string.invalidEmailError;
                break;
            case PASSWORD_INCORRECT:
                this.title =   R.string.invalidEmailTitle;
                this.message = R.string.invalidEmailError;
                break;
            case EMAIL_NOT_ACTIVATED:
                this.title =   R.string.emailUnconfirmedTitle;
                this.message = R.string.emailUnconfirmed;
                break;
            case REGISTER_COMPLETED:
                this.title =   R.string.registerCompletedTitle;
                this.message = R.string.registerCompleted;
                break;
            case REGISTER_FAIL:
                this.title =   R.string.registerFailedTitle;
                this.message = R.string.registerFailed;
                break;
            case PROFILE_NOT_COMPLETED:
                this.title =   R.string.profile_not_completed_title;
                this.message = R.string.profile_not_completed_msg;
                break;
            case SHOULD_UNINSTALL_BROWSER:
                this.icon = R.drawable.wk_browser;
                this.title =   R.string.should_uninstall_browser_title;
                this.message = R.string.should_uninstall_browser_msg;
                break;
        }

        if (this.message == 0) {
            this.message = message;
        }

        return this;
    }


    public ConfirmationDialog message(String message){
        this.text_message = message;
        return this;
    }


    public ConfirmationDialog useHTML(boolean use){
        this.useHTML = use;
        return this;
    }


    public ConfirmationDialog gravity(int gravity){
        this.text_align = gravity;
        return this;
    }


    public ConfirmationDialog icon(int icon){
        this.icon = icon;
        return this;
    }


    public ConfirmationDialog icon(String url){
        if (url != null) {
            this.url_icon = url;
        }
        return this;
    }


    public ConfirmationDialog background(int res){
        this.bg_color = res;

        return this;
    }


    public ConfirmationDialog callback(ConfirmationDialogInterface callback){
        this.listener = callback;
        return this;
    }


    public ConfirmationDialog callback(ConfirmDeleteChild callback){
        this.showPositiveButton = true;
        this.delete_listener = callback;
        return this;
    }


    public ConfirmationDialog callback(ConfirmSaveAppChanges callback){
        this.showPositiveButton = true;
        this.save_listener = callback;
        return this;
    }


    public ConfirmationDialog callback(ConfirmCouponCode callback){
        this.showEditText = true;
        this.showPositiveButton = true;
        this.coupon_listener = callback;
        return this;
    }


    public ConfirmationDialog child(Child child){
        this.child = child;
        return this;
    }


    public ConfirmationDialog negativeButtonLabel(int label){
        this.negative_label = label;
        return this;
    }


    public ConfirmationDialog positiveButtonLabel(int label){
        this.showPositiveButton = true;
        this.positive_label = label;
        return this;
    }


    public ConfirmationDialog circleIcon(boolean opt){
        this.userCircleIcon = opt;
        return this;
    }


    public ConfirmationDialog persistent(boolean persistent){
        this.persistent = persistent;
        return this;
    }


    public boolean isShown(){
        if (alertDialog == null || !alertDialog.isShowing()){
            return false;
        }
        else {
            return true;
        }
    }


}
