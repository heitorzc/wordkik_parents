package com.wordkik.views;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.wordkik.R;
import com.wordkik.objects.Parent;
import com.wordkik.objects.ResponseParent;
import com.wordkik.tasks.ChildTask;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;

/**
 * Created by heitorzc on 09/02/16.
 */
public class ManagePinCodeDialog implements ChildTask.TaskListener, TaskManager.IMethodName {

    Activity context;
    String pincode;
    AlertDialog alertDialog;
    PinCodeDialogInterface listener;

    public interface PinCodeDialogInterface {
        void onPincodeRegistered(String pincode);
    }


    public ManagePinCodeDialog(Context context){
        this.context = (Activity) context;
        this.listener = (PinCodeDialogInterface) context;
    }


    public void registerPinCode(){
        final EditText pin1, pin2, pin3, pin4, pin5;

        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.pincode, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(dialoglayout);
        
        pin1 = (EditText) dialoglayout.findViewById(R.id.pin1);
        pin2 = (EditText) dialoglayout.findViewById(R.id.pin2);
        pin3 = (EditText) dialoglayout.findViewById(R.id.pin3);
        pin4 = (EditText) dialoglayout.findViewById(R.id.pin4);
        pin5 = (EditText) dialoglayout.findViewById(R.id.pin5);

        pin1.addTextChangedListener(watcher(pin2));
        pin2.addTextChangedListener(watcher(pin3));
        pin3.addTextChangedListener(watcher(pin4));
        pin4.addTextChangedListener(watcher(pin5));
        pin5.addTextChangedListener(watcher(null));

        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setNegativeButton(context.getString(R.string.text_cancel), null);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.text_ok), null);
        alertDialogBuilder.setNeutralButton(context.getString(R.string.text_clear), null);

        alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        alertDialog.getButton(Dialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pin5.setText("");
                pin4.setText("");
                pin3.setText("");
                pin2.setText("");
                pin1.setText("");
                pin1.requestFocus();
                alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        });

        alertDialog.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pincode = pin1.getText().toString() + pin2.getText().toString() +
                        pin3.getText().toString() + pin4.getText().toString() + pin5.getText().toString();

                if(pincode.length() == 5) {
                    Parent parent = new Parent();
                    parent.setPincode(pincode);
                    new ParentTask(context, ManagePinCodeDialog.this).edit(parent);
                }
                else {
                    Toast.makeText(context, R.string.pin_code_5_digits, Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    private TextWatcher watcher(final EditText et){
        TextWatcher w = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et != null) {
                    et.requestFocus();
                }
            }
        };

        return w;
    }


    @Override
    public void performTask(Object responseObject, String methodName) {
        Log.e("PERFORM_TASK", "METHOD: " + methodName + " CLASS: " + ManagePinCodeDialog.class);

        switch (methodName) {
            case EDIT_PARENT:
                performEditParent(responseObject);
                break;
            default:
                Log.e("PERFORM_TASK", "METHOD NOT IMPLEMENTED");
                break;
        }
    }

    private void performEditParent(Object responseObject) {
        ResponseParent responseParent = (ResponseParent) responseObject;

        if (responseParent.isSuccess()){
            if (listener != null){
                listener.onPincodeRegistered(pincode);
            }
            alertDialog.dismiss();
        }
    }
}
