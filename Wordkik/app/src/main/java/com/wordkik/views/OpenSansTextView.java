package com.wordkik.views;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.widget.TextView;

import com.wordkik.R;
import com.wordkik.utils.ResourceManager;


/**
 * Created by heitorzc on 22/03/16.
 */
public class OpenSansTextView extends TextView {


    public OpenSansTextView(Context context) {
        super(context);
        setTypeFace(context);
    }

    public OpenSansTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeFace(context);
    }

    public OpenSansTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeFace(context);
    }


    private void setTypeFace(Context context){
        Typeface regular  = Typeface.createFromAsset(context.getAssets(), "fonts/opensansregular.ttf");
        this.setTypeface(regular);
        adjustTextSize();
    }


    private void adjustTextSize(){

        if (getText().toString().contains("%12%")) {

            int size = ResourceManager.getInstance().spToPx(12);
            int index = getText().toString().indexOf("%12%");
            String text = getText().toString().replace("%12%", "");

            applySpan(text, size, index);

        }
        else if (getText().toString().contains("%14%")){

            int size = ResourceManager.getInstance().spToPx(14);
            int index = getText().toString().indexOf("%14%");
            String text = getText().toString().replace("%14%", "");

            applySpan(text, size, index);

        }

    }

    private void applySpan(String text, int size, int index){

        int color = ResourceManager.getInstance().color(R.color.lightGrey);

        Spannable span = new SpannableString(text);
        span.setSpan(new AbsoluteSizeSpan(size), index, span.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new ForegroundColorSpan(color), index, span.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        this.setText(span);
    }

}
