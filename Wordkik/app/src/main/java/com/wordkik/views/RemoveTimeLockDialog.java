package com.wordkik.views;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.wordkik.R;
import com.wordkik.activities.ChildTimeLocksActivity;
import com.wordkik.objects.TimeLock;

import java.util.ArrayList;

/**
 * Created by heitorzc on 09/02/16.
 */
public class RemoveTimeLockDialog {

    Activity context;
    ArrayList<TimeLock> times;
    TimeLock timeLock;
  //  TimeLocksAdapter adapter;

    public RemoveTimeLockDialog(Context context, ArrayList<TimeLock> times, TimeLock timeLock) {
        this.context = (Activity) context;
        this.times = times;
        this.timeLock = timeLock;
        //this.adapter = adapter;
    }

    public void show(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setTitle(context.getString(R.string.remove_timeLock_dialog_title));
        alertDialogBuilder.setMessage(context.getString(R.string.remove_timeLock_dialog_message));
        alertDialogBuilder.setPositiveButton(context.getString(R.string.text_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //TODO: Implement remove on server.
                times.remove(timeLock);
                ChildTimeLocksActivity.child.getTimeLocks().remove(timeLock);
                //adapter.update(times);
            }
        });

        alertDialogBuilder.setNegativeButton(context.getString(R.string.text_cancel), null);

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
