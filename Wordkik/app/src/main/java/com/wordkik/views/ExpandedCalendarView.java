/*
package com.wordkik.Views;

*/
/**
 * Created by heitorzc on 01/09/16.
 *//*


import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;



public class ExpandedCalendarView extends CalendarPickerView {

    private android.view.ViewGroup.LayoutParams params;
    private int old_count = 0;

    public ExpandedCalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (getCount() != old_count) {
            old_count = getCount();
            params = getLayoutParams();
            params.height = getCount() * (old_count > 0 ? getChildAt(0).getHeight() : 0);
            setLayoutParams(params);
        }

        super.onDraw(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST));
    }

}
*/
