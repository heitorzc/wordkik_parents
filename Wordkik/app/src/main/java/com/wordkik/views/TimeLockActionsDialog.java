package com.wordkik.views;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.wordkik.R;
import com.wordkik.activities.ChildTimeLocksActivity;
import com.wordkik.objects.ResponseTimeLock;
import com.wordkik.objects.TimeLock;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;


/**
 * Created by heitorzc on 09/02/16.
 */
public class TimeLockActionsDialog implements TaskManager.TaskListener{

    Context mContext;
    TimeLock timeLock;
    TimeLockActionsInterface listener;

    public interface TimeLockActionsInterface{
        void onTimeLockDeleted();
    }

    public TimeLockActionsDialog(Context mContext, TimeLock timeLock) {
        this.mContext = mContext;
        this.timeLock = timeLock;
        this.listener = (TimeLockActionsInterface) mContext;
    }

    public void show(){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(timeLock.getDescription() + ": " + timeLock.getStart());
        builder.setItems(new CharSequence[]
                        {mContext.getString(R.string.text_edit), mContext.getString(R.string.text_delete)},
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which) {
                            case 0:
                                new EditTimeLockDialog((Activity) mContext, ChildTimeLocksActivity.fragmentManager, timeLock).defineTime();
                                dialog.dismiss();
                                break;
                            case 1:
                                new ParentTask(mContext, TimeLockActionsDialog.this).deleteTimeLock(timeLock);
                                dialog.dismiss();
                                break;
                        }
                    }
                });

        builder.create().show();
    }

    @Override
    public void performTask(Object responseObject, String methodName) {
        ResponseTimeLock responseTimeLock = (ResponseTimeLock) responseObject;

        if (responseTimeLock.isSuccess()){
            ChildTimeLocksActivity.child.setTimeLocks(responseTimeLock.getTimelocks());

            if (listener != null){
                listener.onTimeLockDeleted();
            }
        }
    }
}
