package com.wordkik.views;

import android.app.Activity;
import android.app.Dialog;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.triggertrap.seekarc.SeekArc;
import com.wordkik.R;
import com.wordkik.activities.ChildTimeLocksActivity;
import com.wordkik.objects.ResponseTimeLock;
import com.wordkik.objects.TimeLock;
import com.wordkik.tasks.ParentTask;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by heitorzc on 09/02/16.
 */
public class EditTimeLockDialog implements TextView.OnClickListener, ParentTask.TaskListener{
    @Bind(R.id.tvLockTime)    TextView tvLockTime;
    @Bind(R.id.tvUnlockTime)  TextView tvUnlockTime;
    @Bind(R.id.description)   EditText etDescription;

    Activity context;
    EditTimelockInterface listener;
    FragmentManager fragmentManager;
    TimeLock timeLock;
    String type = "";

    AlertDialog alertDialog;

    public EditTimeLockDialog(Activity context, FragmentManager fragmentManager, TimeLock timeLock) {
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.timeLock = timeLock;
        this.listener = ChildTimeLocksActivity.editListener;
    }

    public interface EditTimelockInterface{
        void onTimeLockEdited();
    }

    public void defineTime(){

        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.time_lock_edit_time_dialog, null);
        ButterKnife.bind(this, dialoglayout);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(dialoglayout);
        alertDialogBuilder.setTitle(type);

        tvLockTime.setOnClickListener(this);
        tvUnlockTime.setOnClickListener(this);

        etDescription.setText(timeLock.getDescription());
        tvLockTime.setText(timeLock.getStart());
        tvUnlockTime.setText(formatDuration(timeLock.getDuration()));

        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setNegativeButton(context.getString(R.string.text_cancel), null);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.text_ok), null);

        alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        alertDialog.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lockTime = tvLockTime.getText().toString();

                editTimeLock(lockTime);

            }
        });
    }

    public void editTimeLock(String lockTime)  {

        timeLock.setDescription(etDescription.getText().toString());
        timeLock.setStart(lockTime);
        timeLock.setProfile_id(ChildTimeLocksActivity.child.getProfile_id());

        new ParentTask(context, this).editTimeLock(timeLock);

    }

    public void defineDuration(final TextView textView){

        final String[] options = context.getResources().getStringArray(R.array.lock_for);
        final String[] options_time_formated = context.getResources().getStringArray(R.array.lock_for_time_formated);
        int selected = 0;

        for (int i = 0; i < options.length; i++){
            if (options[i].equals(textView.getText().toString())){
                selected = i;
            }
        }

        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.time_lock_add_duration_dialog, null);

        final SeekArc seekArc = (SeekArc) dialoglayout.findViewById(R.id.seekArc);
        final TextView value = (TextView) dialoglayout.findViewById(R.id.value);

        seekArc.setProgress(selected);
        value.setText(options[selected]);

        seekArc.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {
            @Override
            public void onProgressChanged(SeekArc seekArc, int i, boolean b) {
                value.setText(options[i]);
            }

            @Override
            public void onStartTrackingTouch(SeekArc seekArc) {

            }

            @Override
            public void onStopTrackingTouch(SeekArc seekArc) {

            }
        });

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(dialoglayout);
        alertDialogBuilder.setTitle(type);

        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setNegativeButton(context.getString(R.string.text_cancel), null);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.text_ok), null);

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        alertDialog.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(value.getText().toString());
                timeLock.setDuration(options_time_formated[seekArc.getProgress()]);
                alertDialog.dismiss();
            }
        });

    }

    public String formatDuration(String duration){
        String formated = duration;


        if (duration.substring(0, 1).equals("0")){
            formated = duration.substring(1);
        }

        if (formated.substring(0, 1).equals("0")){
            formated = duration.substring(3) + "m";
        }
        else if (formated.substring(2, 4).equals("00")){
            formated = formated.replace(":00", "h");
        }
        else if (formated.substring(3, 4).equals("00")){
            formated = formated.substring(0) + "h";
        }
        else if (formated.substring(3).equals("00")){
            formated = formated.substring(0, 2) + "h";
        }

        else {
            formated = formated.replace(":", "h ") + "m";
        }

        return formated;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvLockTime:
                new TimeLockTimePickerDialog(context, fragmentManager, (TextView) v).setDateTime();
                break;
            case R.id.tvUnlockTime:
                defineDuration((TextView) v);
                break;
        }

    }

    @Override
    public void performTask(Object responseObject, String methodName) {
        ResponseTimeLock responseTimeLock = (ResponseTimeLock) responseObject;

        if (responseTimeLock.isSuccess()){
            if (responseTimeLock.getMessage().equals("Overlap detected")){
                ConfirmationDialog.with(context)
                        .title(R.string.timelock_overlap_title)
                        .message(R.string.timelock_overlap_msg)
                        .background(R.color.red)
                        .show();
            }
            else {
                ChildTimeLocksActivity.child.setTimeLocks(responseTimeLock.getTimelocks());

                if (listener != null){
                    listener.onTimeLockEdited();
                }

                alertDialog.dismiss();
            }
        }

        else {
            Toast.makeText(context, R.string.timelock_error_msg, Toast.LENGTH_SHORT).show();
        }
    }

}
