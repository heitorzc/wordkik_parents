package com.wordkik.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.activities.BaseActivity;
import com.wordkik.activities.MainActivity;
import com.wordkik.activities.WebViewActivity;
import com.wordkik.adapters.ChildProfileAdapter;
import com.wordkik.objects.Child;
import com.wordkik.utils.AccountManager;
import com.wordkik.utils.ChildManager;
import com.wordkik.utils.MetricManager;
import com.wordkik.views.ConfirmationDialog;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.BindColor;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.srain.cube.views.GridViewWithHeaderAndFooter;

/**
 * Created by heitorzc on 08/02/16.
 */
public class ChildProfilesFragment extends BaseFragment implements View.OnClickListener, ConfirmationDialog.ConfirmationDialogInterface {
    @BindColor(R.color.showcaseblue)            int blue;
    @Nullable @Bind(R.id.empty_content)         LinearLayout empty;
    @Nullable @Bind(R.id.gvProfiles)            GridViewWithHeaderAndFooter gvProfiles;
    @Nullable @Bind(R.id.footerView)            LinearLayout footerView;



    @OnClick(R.id.btScanCode)
    public void onClickScanCode(){
        if (AccountManager.getInstance(getActivity()).isParentProfileCompleted(getActivity())){
            new ChildManager(getActivity()).startQrReader();
            getApplicationContext().mpTrack("Clicked on Scan/Enter QR Code button");
        }
    }


    @OnClick(R.id.btLearnMore)
    public void onClickLearnMore(){
        startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("url", "http://www.wordkik.com/setting-up-your-app"));
        getApplicationContext().mpTrack("Clicked on Learn More button");
    }


    @OnClick(R.id.btSendLink)
    public void onClickSendLink(){
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");

        share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.install_wk_browser));
        share.putExtra(Intent.EXTRA_TEXT, getString(R.string.wb_share_intent_text));

        startActivity(Intent.createChooser(share, getString(R.string.send_link_via)));

        getApplicationContext().mpTrack("Clicked on Send Link button");
    }



    private final String TAG = "Childs Profiles";
    public static boolean hasEditedChild = false;

    ChildProfileAdapter adapter;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        NAVIGATION_ITEM = R.id.nav_children;
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v;

        if (Constants.hasChildren()) {
            v = inflater.inflate(R.layout.childs_profiles_fragment, container, false);
            ButterKnife.bind(this, v);

            checkChildVersion();
            loadChildrenList();

            AccountManager.getInstance(getActivity()).isParentProfileCompleted(getActivity());
            empty.setVisibility(View.GONE);

        }
        else {
            v = inflater.inflate(R.layout.no_child_help_fragment, container, false);
            ButterKnife.bind(this, v);
        }


        return v;
    }


    private void checkChildVersion(){
        boolean showUpdateDialog = WordKik.prefs.getBoolean("showUpdateDialog", false);
        Log.w("UPDATE", "Show Dialog: " + showUpdateDialog);

        if (showUpdateDialog) {
            ArrayList<Child> toUpdate = new ArrayList<>();

            for (Child child : Constants.childs) {
                if (!child.isApp_up_to_date()) {
                    toUpdate.add(child);
                    Log.w("UPDATE", "Update" + child.getName());
                }
            }

            if (toUpdate.size() == 0 && Constants.user_up_to_date){
                return;
            }
            else {

                int msg; int title; int icon;

                if (Constants.user_up_to_date){
                    msg = (toUpdate.size() > 1) ? R.string.update_browser_children : R.string.update_browser_child;
                    title = R.string.update_browser_title;
                    icon = R.drawable.wk_browser;
                }
                else {
                    msg = (toUpdate.size() > 1) ? R.string.update_apps_children : (toUpdate.size() < 1) ?  R.string.update_app_parent : R.string.update_apps_child;
                    title = R.string.update_apps_title;
                    icon = R.drawable.upgrade_google_play_icon;
                }

                ConfirmationDialog.with(getActivity())
                        .title(title)
                        .message(msg)
                        .background(R.color.lightestGrey)
                        .icon(icon)
                        .titleColor(R.color.Grey)
                        .gravity(Gravity.LEFT)
                        .show();

                WordKik.prefs.edit().putBoolean("showUpdateDialog", false).apply();

            }
        }
    }





    private void loadChildrenList(){

        View header = new View(getActivity());

        AbsListView.LayoutParams lp_header = new AbsListView.LayoutParams(10, 10);

        header.setLayoutParams(lp_header);

        gvProfiles.addHeaderView(header);

        adapter = new ChildProfileAdapter(getContext(), gvProfiles, Constants.childs, empty);
        gvProfiles.setAdapter(adapter);

        updateSmartFooter();

        if (getArguments() != null) {
            String intent = getArguments().getString("intent");

            if (intent != null && intent.equals("add_child")) {
                gvProfiles.smoothScrollToPosition(gvProfiles.getCount());
            }
        }



    }


    private void updateSmartFooter() {
        gvProfiles.post(new Runnable() {

            public void run() {

                if (Constants.childs.size() > 1){
                    View footer = View.inflate(getActivity(), R.layout.add_child_buttons, null);

                    TextView btSendLink  = (TextView) footer.findViewById(R.id.btSendLink);
                    TextView btScanCode  = (TextView) footer.findViewById(R.id.btScanCode);
                    TextView btLearnMore = (TextView) footer.findViewById(R.id.btLearnMore);

                    btSendLink.setOnClickListener(ChildProfilesFragment.this);
                    btScanCode.setOnClickListener(ChildProfilesFragment.this);
                    btLearnMore.setOnClickListener(ChildProfilesFragment.this);

                    gvProfiles.addFooterView(footer);
                }
                else{
                    footerView.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    @Override
    public void onResume() {
        MetricManager.with(this).createNewMetric(TAG);
        getApplicationContext().mpTrack(TAG);
        AccountManager.getInstance(getActivity()).onResume(getActivity());

        ((BaseActivity) getActivity()).getSupportActionBar().setTitle(R.string.children_title);

        if (hasEditedChild){
            hasEditedChild = false;
            ((MainActivity) getActivity()).reloadChildrenSection();
        }

        super.onResume();
    }

    @Override
    public void onPause() {
        MetricManager.getInstance().setTimeLeaving();
        super.onPause();
    }


    @Override
    public void onPositiveButtonClick(String tag) {
        new ChildManager(getActivity()).startQrReader();
        getApplicationContext().mpTrack("Clicked on Add Child button");
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btSendLink:
                onClickSendLink();
                break;
            case R.id.btScanCode:
                onClickScanCode();
                break;
            case R.id.btLearnMore:
                onClickLearnMore();
                break;
        }

    }
}
