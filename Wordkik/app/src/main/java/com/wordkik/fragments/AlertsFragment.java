package com.wordkik.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.activities.BaseActivity;
import com.wordkik.adapters.AlertsAdapter;
import com.wordkik.objects.Metric;
import com.wordkik.tasks.NotificationManager;
import com.wordkik.utils.MetricManager;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AlertsFragment extends BaseFragment implements PullRefreshLayout.OnRefreshListener, NotificationManager.NotificationListener {
    @Bind(R.id.lvChildsAlerts)        ListView lvAlerts;
    @Bind(R.id.swipeRefreshLayout)    PullRefreshLayout pullToRefresh;

    Metric metric;
    final String TAG = "Alerts";

    public static TextView empty;

    AlertsAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        NAVIGATION_ITEM = R.id.nav_alerts;
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.alerts_1, container, false);
        ButterKnife.bind(this, v);

        View header = new View(getActivity());
        AbsListView.LayoutParams lp_header = new AbsListView.LayoutParams(30, 30);
        header.setLayoutParams(lp_header);
        lvAlerts.addHeaderView(header);

        empty = (TextView) v.findViewById(R.id.empty_content);

        pullToRefresh.setOnRefreshListener(this);

        return v;
    }

    @Override
    public void onResume() {
        new NotificationManager(getActivity()).getAlerts(this);

        /*WordKik.cur_cal.setTimeInMillis(System.currentTimeMillis());
        metric = new Metric(TAG, WordKik.cur_cal.getTime().toString());*/
        MetricManager.with(this).createNewMetric(TAG);

        getApplicationContext().mpTrack(TAG);

        ((BaseActivity) getActivity()).getSupportActionBar().setTitle(R.string.notifications_title);

        super.onResume();
    }


    @Override
    public void onPause() {
        /*WordKik.setTimeLeavingMetric(metric);*/
        MetricManager.getInstance().setTimeLeaving();

        super.onPause();
    }

    @Override
    public void onRefresh() {
        new NotificationManager(getActivity(), pullToRefresh, lvAlerts, adapter).getAlerts(this);
    }


    @Override
    public void performNotificationTask() {
        adapter = new AlertsAdapter(getActivity(), Constants.alerts);
        lvAlerts.setAdapter(adapter);

        if (Constants.alerts.size() > 0){
            empty.setVisibility(View.GONE);
        }
        else {
            empty.setVisibility(View.VISIBLE);
        }
    }
}
