package com.wordkik.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.activities.LoginActivity;
import com.wordkik.activities.RegisterActivity;
import com.wordkik.objects.Parent;
import com.wordkik.objects.ResponseParent;
import com.wordkik.tasks.GetPushyToken;
import com.wordkik.tasks.GetUserEmail;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;
import com.wordkik.views.ConfirmationDialog;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

//import com.wordkik.Tasks.GetGcmId;

/**
 * Fragment to register step one
 *
 * @author Denis Heringer
 * @version 1.0.0
 */
public class RegisterStepOneFragment extends Fragment implements ParentTask.TaskListener, GetUserEmail.UserEmailListener, TaskManager.IMethodName, ConfirmationDialog.ConfirmationDialogInterface, GetPushyToken.PushyTokenInterface {
    @Bind(R.id.etFirstName)   EditText etFirstName;
    @Bind(R.id.etLastName)    EditText etLastName;
    @Bind(R.id.etEmail)       EditText etEmail;
    @Bind(R.id.etPassword)    EditText etPassword;


    @OnClick(R.id.tvNext)
    public void onClickNext(){

        if (validate()) {

            String name = getString(etFirstName) + " " + getString(etLastName);
            String email = getString(etEmail);

            ConfirmationDialog.with(getActivity())
                    .title(R.string.confirm_details)
                    .background(R.color.red)
                    .message(getString(R.string.details_msg).replace("$name", name).replace("$email", email))
                    .useHTML(true)
                    .callback(this)
                    .gravity(Gravity.LEFT)
                    .negativeButtonLabel(R.string.text_cancel)
                    .positiveButtonLabel(R.string.text_ok)
                    .show();
        }

    }


    @OnTouch(R.id.showPassword)
    public boolean onTouchShowPassword(MotionEvent event){

        if (event.getAction() == MotionEvent.ACTION_DOWN){
            etPassword.setInputType(InputType.TYPE_CLASS_TEXT);
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP){
            etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            return true;
        }

        return false;
    }

    String TAG = "Registration - Form";

    private Parent parent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register_screen_1, container, false);
        ButterKnife.bind(this, view);

                ((RegisterActivity) getActivity()).getSupportActionBar().setTitle(R.string.tbRegister);
        ((RegisterActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        new GetUserEmail(getActivity(), this).execute();

        return view;
    }


    /**
     * Function to validate data input
     */
    private boolean validate() {
        if ("".equals(etFirstName.getText().toString().trim())) {
            Toast.makeText(getContext(), R.string.emFirstNameCannotBeBlank, Toast.LENGTH_SHORT).show();
            return false;
        }

        if ("".equals(etLastName.getText().toString().trim())) {
            Toast.makeText(getContext(), R.string.emLastNameCannotBeBlank, Toast.LENGTH_SHORT).show();
            return false;
        }

        if ("".equals(etEmail.getText().toString().trim())) {
            Toast.makeText(getContext(), R.string.emEmailCannotBeBlank, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
            Toast.makeText(getContext(), R.string.emEmailInvalid, Toast.LENGTH_SHORT).show();
            return false;
        }

        if ("".equals(etPassword.getText().toString().trim())) {
            Toast.makeText(getContext(), R.string.emPasswordCannotBeBlank, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (etPassword.getText().toString().length() < Integer.valueOf(getContext().getString(R.string.PASSWORD_MIN_LENGTH))) {
            Toast.makeText(getContext(), R.string.emPasswordIsTooShort, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!WordKik.hasInternet) {
            Toast.makeText(getContext(), R.string.emNoInternetConnection, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    private void checkEmail() {
        parent = new Parent();

        parent.setFirstName(getString(etFirstName));
        parent.setLastName(getString(etLastName));
        parent.setEmail(getString(etEmail));
        parent.setPassword(getString(etPassword));

        new ParentTask(getContext(), this).checkEmail(parent);
    }


    public String getString(EditText et){
        return et.getText().toString();
    }


    @Override
    public void performTask(Object responseObject, String methodName) {
        Log.e("PERFORM_TASK", "METHOD: " + methodName + " CLASS: " + RegisterStepOneFragment.class);

        switch (methodName) {
            case CHECK_EMAIL:
                performCheckEmail(responseObject);
                break;
            case REGISTER_PARENT:
                performRegisterParent(responseObject);
                break;
            default:
                Log.e("PERFORM_TASK", "METHOD NOT IMPLEMENTED");
                break;
        }
    }


    private void performCheckEmail(Object responseObject) {
        ResponseParent responseParent = (ResponseParent) responseObject;

        if (responseParent.isSuccess()) {

            new GetPushyToken(getActivity(), this).execute();

        }
        else {

            if(responseParent.getMessage().equals("Email not available")){
                ConfirmationDialog.with(getActivity())
                        .message(ConfirmationDialog.EMAIL_TAKEN)
                        .background(R.color.red)
                        .show();
            }

        }

    }


    private void performRegisterParent(Object responseObject) {
        ResponseParent responseParent = (ResponseParent) responseObject;

        if (responseParent.isSuccess()) {
            Constants.setContantsParentFields(responseParent.getParent());

            parent = responseParent.getParent();

            ((RegisterActivity) getActivity()).setParentUser(parent);

            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.rlContent, new RegisterStepThreeFragment())
                    .addToBackStack(null)
                    .commit();
        } else {
            ConfirmationDialog.with(getActivity())
                    .message(ConfirmationDialog.REGISTER_FAIL)
                    .background(R.color.red)
                    .show();
        }
    }


    @Override
    public void onPushyTokenReady(String token) {
        parent.setRegistration_id(token);
        registerParent();
    }


    /**
     * Function to process data input
     */
    private void registerParent() {
        new ParentTask(getContext(), RegisterStepOneFragment.this).registerParent(parent);
    }


    @Override
    public void onResume() {
        ((WordKik) getActivity().getApplicationContext()).mpTrack(TAG);
        super.onResume();
    }


    @Override
    public void onPositiveButtonClick(String tag) {
        checkEmail();
    }


    @Override
    public void onEmailReceived(String email) {
        etEmail.setText(email);
    }


}