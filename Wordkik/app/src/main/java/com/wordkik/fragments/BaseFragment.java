package com.wordkik.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.wordkik.WordKik;
import com.wordkik.activities.MainActivity;

public class BaseFragment extends Fragment {

    int NAVIGATION_ITEM = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Override
    public void onResume() {
        ((MainActivity) getActivity()).setNavigationItemSelected(NAVIGATION_ITEM);
        super.onResume();
    }

    public WordKik getApplicationContext(){
        return (WordKik) getActivity().getApplicationContext();
    }
}
