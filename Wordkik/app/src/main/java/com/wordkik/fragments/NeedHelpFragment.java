package com.wordkik.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wordkik.R;
import com.wordkik.activities.LoginActivity;
import com.wordkik.activities.WebViewActivity;

import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Fragment to need help
 *
 * @author Denis Heringer
 * @version 1.0.0
 */
public class NeedHelpFragment extends Fragment {
    @BindString(R.string.tvHelp) String help;

    String url = "http://wordkik.herokuapp.com";

    @OnClick(R.id.setupInstructions)
    public void onClickSetupInstructions(){
        startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("url", url));
    }

    @OnClick(R.id.creatingProfile)
    public void onClickCreatingProfile(){
        startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("url", url));
    }

    @OnClick(R.id.linkingApps)
    public void onClickLinkingApps(){
        startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("url", url));
    }

    @OnClick(R.id.forgotPassword)
    public void onClickForgotPassword(){
        startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("url", url));
    }

    @OnClick(R.id.browserSetup)
    public void onClickBrowserSetup(){
        startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("url", url));
    }

    @OnClick(R.id.howItWorks)
    public void onCloickHowItWorkd(){
        startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("url", url));
    }

    @OnClick(R.id.uninstalling)
    public void onClickUninstalling(){
        startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("url", url));
    }

    @OnClick(R.id.wordkikParental)
    public void onClickWordkikParental(){
        startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("url", url));
    }

    @OnClick(R.id.wordkidBrowser)
    public void onClickWordkikBrowser(){
        startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("url", url));
    }

    @OnClick(R.id.policy)
    public void onClickPolicy(){
        startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("url", url));
    }

    @OnClick(R.id.terms)
    public void onClickTerms(){
        startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("url", url));
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.need_help, container, false);
        ButterKnife.bind(this, view);

        ((LoginActivity) getActivity()).getSupportActionBar().setTitle(help);
        ((LoginActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        return view;
    }
}