package com.wordkik.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.activities.BaseActivity;
import com.wordkik.activities.PurchaseActivity;
import com.wordkik.adapters.PlansAdapter;
import com.wordkik.objects.Metric;
import com.wordkik.objects.Plan;
import com.wordkik.utils.MetricManager;
import com.wordkik.utils.ResourceManager;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PurchaseFragment extends BaseFragment implements AdapterView.OnItemClickListener{
    @Nullable @Bind(R.id.listPlans)      ListView listPlans;
   // @Nullable @Bind(R.id.choosen_plan)   TextView choosen_plan;
   // @Nullable @Bind(R.id.payment_method) TextView payment_method;
    @Nullable @Bind(R.id.days_left)      TextView days_left;

    Metric metric;
    ArrayList<Plan> plans = new ArrayList<>();
    final String TAG = "Purchase";

    String acc_type;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        NAVIGATION_ITEM = R.id.nav_purchase;
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v;  int layout;
        acc_type = Constants.user_account_type;

        if (acc_type.equals("paying")){
            layout = R.layout.subscription_information;
            v = inflater.inflate(layout, container, false);
            loadSubscriptionInfo(v);
        }
        else {
            layout = R.layout.purchase;
            v = inflater.inflate(layout, container, false);
            loadPlans(v);
        }

        return v;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Plan selected_plan = plans.get(position - 1);
        startActivity(new Intent(getActivity(), PurchaseActivity.class).putExtra("selected_plan", selected_plan));
    }

    @Override
    public void onResume() {
       /* WordKik.cur_cal.setTimeInMillis(System.currentTimeMillis());
        metric = new Metric(TAG, WordKik.cur_cal.getTime().toString());*/
        MetricManager.with(this).createNewMetric(TAG);
        getApplicationContext().mpTrack(TAG);

        ((BaseActivity) getActivity()).getSupportActionBar().setTitle(R.string.purchase_title);

        if (!Constants.user_account_type.equals(acc_type)){
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new PurchaseFragment(), TAG).commit();
        }

        super.onResume();
    }

    @Override
    public void onPause() {
        /*WordKik.setTimeLeavingMetric(metric);*/
        MetricManager.getInstance().setTimeLeaving();
        super.onPause();
    }

    private void loadSubscriptionInfo(View v){
        ButterKnife.bind(this, v);

        String payment = WordKik.prefs.getString("payment_method", "Google Play");
        String selected;

       /* choosen_plan.setText("Esse é o plano");
        payment_method.setText("Pagou com a bunda");*/
        days_left.setText(Constants.user_expiration + " " + "days");
    }

    private void loadPlans(View v){
        ButterKnife.bind(this, v);

        setHeaderAndFooter();

        if (plans.size() == 0){
            addPlanstoList();
        }

        PlansAdapter adapter = new PlansAdapter(getActivity(), plans);
        listPlans.setAdapter(adapter);
        listPlans.setOnItemClickListener(this);
    }

    private void setHeaderAndFooter(){
        int margin = ResourceManager.getInstance().dpToPx(10);

        View header = new View(getActivity());
        View footer = new View(getActivity());

        AbsListView.LayoutParams lp_header = new AbsListView.LayoutParams(margin, margin);
        AbsListView.LayoutParams lp_footer = new AbsListView.LayoutParams(margin, margin);

        header.setLayoutParams(lp_header);
        footer.setLayoutParams(lp_footer);

        listPlans.addHeaderView(header);
        listPlans.addFooterView(footer);
    }

    private void addPlanstoList(){
        String userCountry = Constants.user_country.toLowerCase();
        String monthly_plan = getActivity().getResources().getString(R.string.monthly_plan);
        String yearly_plan = getActivity().getResources().getString(R.string.yearly_plan);

        if (userCountry.equals("brazil")){
            plans.add(new Plan(yearly_plan,  "R$", "39,99", "wk_yearly_1899", "wk_yearly_3999", "95,99", "60%"));
            plans.add(new Plan(monthly_plan, "R$", "4,99", "wk_monthly_199", "wk_monthly_499", "9,99", "50%"));
        }
        else if (userCountry.equals("canada")){
            plans.add(new Plan(yearly_plan,  "CAD", "27.99", "wk_yearly_1899", "wk_yearly_1899", "69.99", "60%"));
            plans.add(new Plan(monthly_plan, "CAD", "2.89", "wk_monthly_199", "wk_monthly_199", "6.99", "60%"));
        }
        else if (userCountry.equals("australia")){
            plans.add(new Plan(yearly_plan,  "AUD", "27.99", "wk_yearly_1899", "wk_yearly_1899", "74.99", "60%"));
            plans.add(new Plan(monthly_plan, "AUD", "2.89", "wk_monthly_199", "wk_monthly_199", "7.49", "60%"));
        }
        else if (userCountry.equals("united states")){
            plans.add(new Plan(yearly_plan,  "US$", "20.99", "wk_yearly_1899", "wk_yearly_1899", "49.99", "60%"));
            plans.add(new Plan(monthly_plan, "US$", "1.99", "wk_monthly_199", "wk_monthly_199", "4.99", "60%"));
        }
        else {
            plans.add(new Plan(yearly_plan,  "€", "18.99", "wk_yearly_1899", "wk_yearly_1899", "49.99", "60%"));
            plans.add(new Plan(monthly_plan, "€", "1.99", "wk_monthly_199", "wk_monthly_199", "4.99", "60%"));
        }
    }
}
