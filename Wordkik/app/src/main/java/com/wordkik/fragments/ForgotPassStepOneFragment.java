package com.wordkik.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.activities.ForgotPasswordActivity;
import com.wordkik.objects.Parent;
import com.wordkik.objects.ResponseParent;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;

/**
 * Fragment to forgot password step one
 *
 * @author Denis Heringer
 * @version 1.0.0
 */
public class ForgotPassStepOneFragment extends Fragment implements View.OnClickListener, ParentTask.TaskListener, TaskManager.IMethodName {

    private EditText etEmail;
    private TextView tvNext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forgot_pass_1, container, false);

        ((ForgotPasswordActivity) getActivity()).getSupportActionBar().setTitle(R.string.tbForgotPassword);
        ((ForgotPasswordActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etEmail = (EditText) view.findViewById(R.id.etEmail);
        tvNext = (TextView) view.findViewById(R.id.tvNext);

        tvNext.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvNext:
                tvNextOnClick();
                break;
        }
    }

    /**
     * Function to handle tvNext on click event
     */
    public void tvNextOnClick() {
        if (!validate()) {
            return;
        }

        process();
    }

    /**
     * Function to validate data input
     */
    private boolean validate() {
        if ("".equals(etEmail.getText().toString().trim())) {
            Toast.makeText(getContext(), R.string.emEmailCannotBeBlank, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
            Toast.makeText(getContext(), R.string.emEmailInvalid, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!WordKik.hasInternet) {
            Toast.makeText(getContext(), R.string.emNoInternetConnection, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    /**
     * Function to process data input
     */
    private void process() {
        Parent parent = new Parent();
        parent.setEmail(etEmail.getText().toString());

        ForgotPasswordActivity.email = parent.getEmail();

        new ParentTask(getContext(), this).resetPassword(parent);
    }

    @Override
    public void performTask(Object responseObject, String methodName) {
        Log.e("PERFORM_TASK", "METHOD: " + methodName + " CLASS: " + ForgotPassStepOneFragment.class);

        switch (methodName) {
            case RESET_PASSWORD:
                performResetPassword(responseObject);
                break;
            default:
                Log.e("PERFORM_TASK", "METHOD NOT IMPLEMENTED");
                break;
        }
    }

    private void performResetPassword(Object responseObject) {
        ResponseParent responseParent = (ResponseParent) responseObject;



        if (responseParent.isSuccess()) {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.rlContent, new ForgotPassStepTwoFragment())
                    .addToBackStack(null)
                    .commit();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(responseParent.getMessage());
            AlertDialog alertDialog = builder.create();
            builder.setPositiveButton(R.string.text_ok, null);
            alertDialog.show();

        }


    }

    @Override
    public void onPause() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
        catch (NullPointerException e){
            Log.e("KEYBOARD", "Attempt to invoke virtual method 'android.os.IBinder android.view.View.getWindowToken()' on a null object reference");
        }
        super.onPause();
    }
}