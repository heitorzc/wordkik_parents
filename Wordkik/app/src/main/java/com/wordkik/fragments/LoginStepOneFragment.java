package com.wordkik.fragments;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.activities.LoginActivity;
import com.wordkik.activities.RegisterActivity;
import com.wordkik.objects.Metric;
import com.wordkik.views.ConfirmationDialog;

import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;

/**
 * Fragment to login step one
 *
 * @author Denis Heringer
 * @version 1.0.0
 */
public class LoginStepOneFragment extends Fragment {

    Metric metric;

    @OnClick(R.id.tvLogin)
    public void onClickLogin(){
        if (Constants.shouldNotProceed) {
            showShouldUninstallBrowserDialog();
        } else {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.rlContent, new LoginStepTwoFragment())
                    .addToBackStack(null)
                    .commit();
        }
    }

    @OnClick(R.id.tvRegister)
    public void onClickRegister(){
        if (Constants.shouldNotProceed) {
            showShouldUninstallBrowserDialog();
        } else {
            requestPermissions();
        }
    }

    /* @OnClick(R.id.tvHelp)
    public void onClickHelp(){
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.rlContent, new NeedHelpFragment())
                .addToBackStack(null)
                .commit();
    } */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_screen_1, container, false);
        ButterKnife.bind(this, view);

        ((LoginActivity) getActivity()).getSupportActionBar().setTitle(R.string.tbWelcome);
        ((LoginActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((LoginActivity) getActivity()).getSupportActionBar().show();

        if (Constants.shouldNotProceed) {
            showShouldUninstallBrowserDialog();
        }
        
        return view;
    }

    private void showShouldUninstallBrowserDialog() {
        ConfirmationDialog.with(getActivity())
                .message(ConfirmationDialog.SHOULD_UNINSTALL_BROWSER)
                .background(R.color.lightestGrey)
                .show();
    }

    @Override
    public void onResume() {
        super.onResume();

        ((LoginActivity) getActivity()).getSupportActionBar().hide();
    }

    public void requestPermissions(){
        int GET_ACCOUNTS = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.GET_ACCOUNTS);

        if (GET_ACCOUNTS   == PackageManager.PERMISSION_GRANTED) {
            startActivity(new Intent(getActivity(), RegisterActivity.class));
        }

        else {
            Nammu.init(getActivity());
            Nammu.askForPermission(getActivity(), new String[]{android.Manifest.permission.GET_ACCOUNTS},
                    new PermissionCallback() {
                        @Override
                        public void permissionGranted() {
                            startActivity(new Intent(getActivity(), RegisterActivity.class));
                        }

                        @Override
                        public void permissionRefused() {
                            startActivity(new Intent(getActivity(), RegisterActivity.class));
                        }

                    });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}