package com.wordkik.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wordkik.R;
import com.wordkik.activities.ForgotPasswordActivity;

/**
 * Fragment to forgot password step two
 *
 * @author  Denis Heringer
 * @version  1.0.0
 */
public class ForgotPassStepTwoFragment extends Fragment implements View.OnClickListener {

    private TextView tvCheckEmail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forgot_pass_2, container, false);

        ((ForgotPasswordActivity) getActivity()).getSupportActionBar().setTitle(R.string.tbConfirmation);
        ((ForgotPasswordActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvCheckEmail = (TextView) view.findViewById(R.id.tvCheckEmail);

        tvCheckEmail.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case android.R.id.home:
                getActivity().finish();
                break;
            case R.id.tvCheckEmail:
                tvCheckEmailOnClick();
                break;
        }
    }

    /**
     * Function to handle tvCheckEmail on click event
     */
    public void tvCheckEmailOnClick() {
        String url = ForgotPasswordActivity.email;
        url = "http://www." + url.substring(url.indexOf("@") + 1);

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);

        getActivity().finish();
    }

}
