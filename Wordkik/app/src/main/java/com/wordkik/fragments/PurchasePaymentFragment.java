package com.wordkik.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.activities.PurchaseActivity;
import com.wordkik.objects.Coupon;
import com.wordkik.objects.Metric;
import com.wordkik.objects.Plan;
import com.wordkik.objects.ResponseCoupon;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;
import com.wordkik.utils.MetricManager;
import com.wordkik.utils.ResourceManager;
import com.wordkik.views.ConfirmationDialog;
import com.wordkik.views.CreditCardDialog;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.facebook.FacebookSdk.getApplicationContext;

public class PurchasePaymentFragment extends Fragment implements ConfirmationDialog.ConfirmCouponCode, TaskManager.TaskListener{
    @Bind(R.id.rootView)                           LinearLayout rootView;
    @Bind(R.id.tvDate)                             TextView tvDate;
    @Bind(R.id.tvDateTitle)                        TextView tvDateTitle;
    @Bind(R.id.tvUserName)                         TextView tvUserName;
    @Bind(R.id.tvUserEmail)                        TextView tvUserEmail;
    @Bind(R.id.tvTitle)                            TextView tvTitle;
    @Bind(R.id.tvPrice)                            TextView tvPrice;
    @Bind(R.id.tvSymbol)                           TextView tvSymbol;


    @OnClick(R.id.pay_google)
    public void onClickPayGoogle(){
        ((WordKik) getApplicationContext()).mpTrack("Selected Payment via Google Play");
        PurchaseActivity.subscribe(getActivity(), choosenPlan.getPlay_id());

    }

    @OnClick(R.id.pay_card)
    public void onClickPayCard(){
         showPromoCodeDialog(this, R.string.do_you_promo_code);
    }

    final String TAG = "Purchase - Payment Method";


    Plan choosenPlan;
    String coupon;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.purchase_payment_fragment, container, false);
        ButterKnife.bind(this, v);

        ((PurchaseActivity) getActivity()).getSupportActionBar().setTitle(R.string.make_payment);

        choosenPlan = getArguments().getParcelable("selected_plan");

        loadViews(choosenPlan);

        return v;
    }

    @SuppressWarnings("WrongConstant")
    private void loadViews(Plan plan){
        tvTitle.setText(plan.getTitle());
        tvPrice.setText(plan.getPrice());
        tvSymbol.setText(plan.getCurrency());

        tvUserEmail.setText(Constants.user_email);
        tvUserName.setText(Constants.user_name + " " + Constants.user_lastName);

        WordKik.cur_cal.setTimeInMillis(System.currentTimeMillis());

        Date date = WordKik.cur_cal.getTime();
        String formatedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);

        tvDate.setText(formatedDate);

        Snackbar.make(getActivity().findViewById(android.R.id.content), R.string.promo_snack_bar, 5000)
                .setAction(R.string.redeem_it, snackBarActionClick())
                .setActionTextColor(ResourceManager.getInstance().color(R.color.red))
                .show();
    }

    private void showPromoCodeDialog(ConfirmationDialog.ConfirmCouponCode callback, int message){
        ConfirmationDialog.with(getActivity())
                .title(R.string.promotional_code)
                .message(message)
                .background(R.color.green)
                .icon(R.drawable.com_mixpanel_android_ic_coin)
                .callback(callback)
                .positiveButtonLabel(R.string.checkout)
                .negativeButtonLabel(R.string.text_cancel)
                .gravity(Gravity.LEFT)
                .show();
    }

    @Override
    public void onResume() {
        MetricManager.with(this).createNewMetric(TAG);
        ((WordKik) getApplicationContext()).mpTrack(TAG);
        super.onResume();
    }

    @Override
    public void onPause() {
        MetricManager.getInstance().setTimeLeaving();
        super.onPause();
    }

    @Override
    public void onCouponEntered(String coupon) {
        this.coupon = coupon;

        if (coupon.length() > 0) {
            new ParentTask(getActivity(), this).checkCoupon(new Coupon(coupon));
        }
        else {
            goToCheckout(coupon);
        }
    }

    private void goToCheckout(String code){
        ((WordKik) getApplicationContext()).mpTrack("Selected Payment via Stripe");
        new CreditCardDialog(getActivity(), choosenPlan, code).show();
    }

    public View.OnClickListener snackBarActionClick(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPromoCodeDialog(PurchasePaymentFragment.this, R.string.type_promotional_code);
            }
        };
    }

    @Override
    public void performTask(Object responseObject, String methodName) {

        ResponseCoupon responseCoupon = (ResponseCoupon) responseObject;

        if (responseCoupon.isSuccess()){
            goToCheckout(coupon);
        }
        else {
            showPromoCodeDialog(PurchasePaymentFragment.this, R.string.type_promotional_code);
            Toast.makeText(getActivity(), R.string.promo_invalid, Toast.LENGTH_SHORT).show();
        }
    }
}
