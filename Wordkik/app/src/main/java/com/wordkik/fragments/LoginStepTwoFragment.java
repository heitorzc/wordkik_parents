package com.wordkik.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.activities.ForgotPasswordActivity;
import com.wordkik.activities.LoginActivity;
import com.wordkik.activities.MainActivity;
import com.wordkik.objects.Parent;
import com.wordkik.objects.ResponseChild;
import com.wordkik.objects.ResponseParent;
import com.wordkik.tasks.ChildTask;
import com.wordkik.tasks.EncryptionManager;
import com.wordkik.tasks.GetPushyToken;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;
import com.wordkik.utils.Animate;
import com.wordkik.views.ConfirmationDialog;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Fragment to login step two
 *
 * @author Denis Heringer
 * @version 1.0.0
 */
public class LoginStepTwoFragment extends Fragment implements ParentTask.TaskListener, EncryptionManager.EncryptionListener, TaskManager.IMethodName, GetPushyToken.PushyTokenInterface {
    @BindString(R.string.encrypt_key)   String ENCRYPT_KEY;
    @BindString(R.string.encrypt_salt)   String ENCRYPT_SALT;
    @Bind(R.id.etEmail)                 EditText etEmail;
    @Bind(R.id.etPassword)              EditText etPassword;
    @Bind(R.id.login_form)              RelativeLayout login_form;
    @Bind(R.id.loading)                 LinearLayout loading;

    @OnClick(R.id.tvLogin)
    public void onClickLogin(){
        if (validate()) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

            Animate.anime(new View[]{login_form, loading}, 0, 300, new Techniques[]{Techniques.SlideOutRight, Techniques.SlideInLeft}, null);

            parent = new Parent();
            parent.setEmail(etEmail.getText().toString());
            parent.setPassword(etPassword.getText().toString());

            new GetPushyToken(getActivity(), this).execute();

        }

    }

    @OnClick(R.id.tvForgot)
    public void onClickForgot(){
        startActivity(new Intent(getActivity(), ForgotPasswordActivity.class));
    }

    private Parent parent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.login_screen_2, container, false);
        ButterKnife.bind(this, v);

        ((LoginActivity) getActivity()).getSupportActionBar().setTitle(R.string.tbLogin);
        ((LoginActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((LoginActivity) getActivity()).getSupportActionBar().show();

        parent = ((LoginActivity) getActivity()).getParentUser();

        if (parent != null && parent.getEmail() != null && !parent.getEmail().trim().equals("")) {
            etEmail.setText(parent.getEmail());
        }

        return v;
    }

    /**
     * Function to validate data input
     */
    private boolean validate() {
        if ("".equals(etEmail.getText().toString().trim())) {
            Toast.makeText(getContext(), R.string.emEmailCannotBeBlank, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
            Toast.makeText(getContext(), R.string.emEmailInvalid, Toast.LENGTH_SHORT).show();
            return false;
        }

        if ("".equals(etPassword.getText().toString().trim())) {
            Toast.makeText(getContext(), R.string.emPasswordCannotBeBlank, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!WordKik.hasInternet) {
            Toast.makeText(getContext(), R.string.emNoInternetConnection, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    public void onDestroyView() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        super.onDestroyView();
    }

    @Override
    public void performTask(Object responseObject, String methodName) {
        Log.e("PERFORM_TASK", "METHOD: " + methodName + " CLASS: " + LoginStepTwoFragment.class);

        switch (methodName) {
            case LOGIN:
                performLogin(responseObject);
                break;
            case GET_CHILDREN:
                performGetChildren(responseObject);
                break;
            default:
                Log.e("PERFORM_TASK", "METHOD NOT IMPLEMENTED");
                break;
        }
    }

    private void performGetChildren(Object responseObject){
        ResponseChild responseChild = (ResponseChild) responseObject;

        if (responseChild.isSuccess()) {
            Constants.childs = ((ResponseChild) responseObject).getChildren();

            // SAVE CREDENTIALS AND REDIRECT TO MAIN ACTIVITY AFTER STORE ON PREFS
            saveCredentials();
        }

    }

    private void performLogin(Object responseObject) {
        ResponseParent responseParent = (ResponseParent) responseObject;

        if (responseParent.isSuccess()) {

            Constants.setContantsParentFields(responseParent.getParent());
            new ChildTask(getActivity(), this).getChildren();

        }
        else {

            Animate.anime(new View[]{loading, login_form}, 0, 300, new Techniques[]{Techniques.SlideOutRight, Techniques.SlideInLeft}, null);

            if (responseParent.getMessage().equals("Invalid email or password")) {
                ConfirmationDialog.with(getActivity())
                        .message(ConfirmationDialog.EMAIL_NOT_FOUND)
                        .background(R.color.red)
                        .show();
            }
            else if (responseParent.getMessage().equals("Email not registered")){
                ConfirmationDialog.with(getActivity())
                        .message(ConfirmationDialog.EMAIL_NOT_FOUND)
                        .background(R.color.red)
                        .show();
            }
            else if (responseParent.getMessage().equals("Unconfirmed email")){
                ConfirmationDialog.with(getActivity())
                        .message(ConfirmationDialog.EMAIL_NOT_ACTIVATED)
                        .gravity(Gravity.LEFT)
                        .background(R.color.red)
                        .show();
            }
        }
    }


    @Override
    public void onPushyTokenReady(String token) {
        parent.setRegistration_id(token);
        new ParentTask(getContext(), this).login(parent);

    }


    private void saveCredentials() {
        new EncryptionManager(getActivity(), parent.getPassword(), ENCRYPT_KEY, ENCRYPT_SALT, this, EncryptionManager.ENCRYPT_PASSWORD).execute();
    }


    @Override
    public void setSafeString(String safeString) {
        Log.e("LOGIN FRAG", "Saving credentials" + " Senha: " + safeString);

        WordKik.prefs.edit().putString(Constants.USER_EMAIL_KEY, parent.getEmail()).apply();
        WordKik.prefs.edit().putString(Constants.USER_PASS_KEY, safeString).apply();

        Constants.isLoggedIn = true;

        startActivity(new Intent(getActivity(), MainActivity.class));
        getActivity().finish();
    }


    @Override
    public void onResume() {
        if (ForgotPasswordActivity.email != null){
            etEmail.setText(ForgotPasswordActivity.email);
            etPassword.requestFocus();
        }

        ((LoginActivity) getActivity()).getSupportActionBar().show();

        super.onResume();
    }

}