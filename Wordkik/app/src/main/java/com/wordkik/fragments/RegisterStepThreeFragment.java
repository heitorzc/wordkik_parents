package com.wordkik.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.activities.RegisterActivity;
import com.wordkik.objects.Parent;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Fragment to register step three
 *
 * @author  Denis Heringer
 * @version  1.0.0
 */
public class RegisterStepThreeFragment extends Fragment{

    @OnClick(R.id.tvCheckEmail)
    public void onClickCheckEmail(){
        String url = parent.getEmail();
        url = "http://www." + url.substring(url.indexOf("@") + 1);

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);

        Constants.registeredParent = parent;

        getActivity().finish();
    }

    String TAG = "Registration Completed";
    private Parent parent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parent = ((RegisterActivity) getActivity()).getParentUser();

        View view = inflater.inflate(R.layout.register_screen_3, container, false);
        ButterKnife.bind(this, view);

        ((RegisterActivity) getActivity()).getSupportActionBar().setTitle(R.string.tbConfirmation);
        ((RegisterActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        return view;
    }

    @Override
    public void onResume() {
        ((WordKik) getActivity().getApplicationContext()).mpSetProfile("" + parent.getId(), parent.getFirstName() + " " + parent.getLastName(), parent.getEmail());
        ((WordKik) getActivity().getApplicationContext()).mpTrack(TAG);
        super.onResume();
    }
}