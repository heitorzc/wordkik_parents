package com.wordkik.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.wordkik.Constants;
import com.wordkik.R;

import java.io.File;

/**
 * Created by heitorzc on 04/04/16.
 */
public class AmazonS3LogsUploader extends AsyncTask<Void, Void, Void> {

    // TODO GENERATE USER ID

    private String amazon_id;
    private String amazon_api_key;
    private String file_name;
    private File logFile;

    Context context;

    private AmazonS3Client amazonS3Client;

    public AmazonS3LogsUploader(Context context, String file_name, File logFile) {
        this.context = context;
        this.amazon_id = context.getResources().getString(R.string.amazon_id);
        this.amazon_api_key = context.getResources().getString(R.string.amazon_api_key);
        this.logFile = logFile;
        this.file_name = file_name;
    }


    @Override
    protected Void doInBackground(Void... params) {

        try {

            amazonS3Client = new AmazonS3Client(new BasicAWSCredentials(amazon_id, amazon_api_key));

            amazonS3Client.createBucket("parentlogs");

            PutObjectRequest putObjectRequest = new PutObjectRequest("parentlogs/" + Constants.user_email, file_name, logFile);

            PutObjectResult putObjectResult = amazonS3Client.putObject(putObjectRequest);


        } catch (Exception ex) {
            Log.e("AMAZON", "Error uploading logs.");
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Log.e("AMAZON", "Log File Uploaded to server");
    }

}
