package com.wordkik.tasks;

import android.content.Context;

import com.wordkik.R;
import com.wordkik.objects.Alert;
import com.wordkik.objects.CardPurchase;
import com.wordkik.objects.ChatMessage;
import com.wordkik.objects.Child;
import com.wordkik.objects.Coupon;
import com.wordkik.objects.Feedback;
import com.wordkik.objects.Parent;
import com.wordkik.objects.ParentNotification;
import com.wordkik.objects.PurchaseData;
import com.wordkik.objects.RequestSendAppList;
import com.wordkik.objects.ResponseAddGeofence;
import com.wordkik.objects.ResponseAlert;
import com.wordkik.objects.ResponseChatMessages;
import com.wordkik.objects.ResponseCoupon;
import com.wordkik.objects.ResponseDefault;
import com.wordkik.objects.ResponseFeedback;
import com.wordkik.objects.ResponseGeoActivities;
import com.wordkik.objects.ResponseListGeofences;
import com.wordkik.objects.ResponseMetric;
import com.wordkik.objects.ResponseNotification;
import com.wordkik.objects.ResponseParent;
import com.wordkik.objects.ResponsePurchase;
import com.wordkik.objects.ResponseTimeLock;
import com.wordkik.objects.TimeLock;
import com.wordkik.objects.UserGeofence;
import com.wordkik.objects.UserMetric;

/**
 * Responsible for tasks related to Parent
 *
 * @author Denis Heringer
 * @version 1.0.0
 */
public class ParentTask extends TaskManager implements TaskManager.IMethodName {

    public ParentTask(Context context, TaskListener taskListener) {
        super(context, taskListener);
    }


    public void sendChatMessage(final ChatMessage message) {
        doRequest(context.getString(R.string.SEND_CHAT_MESSAGE), message, true, new ResponsePurchase(), SEND_CHAT_MESSAGE);
    }

    public void getChatMessages() {
        doRequest(context.getString(R.string.GET_CHAT_MESSAGES), false, new ResponseChatMessages(), GET_CHAT_MESSAGES);
    }

    public void sendSubscription(final PurchaseData purchaseData) {
        doRequest(context.getString(R.string.SEND_SUBSCRIPTION), purchaseData, true, new ResponsePurchase(), SEND_SUBSCRIPTION);
    }

    public void sendStripeSubscription(final CardPurchase cardPurchase) {
        doRequest(context.getString(R.string.SEND_STRIPE_SUBSCRIPTION), cardPurchase, false, new ResponsePurchase(), SEND_STRIPE_SUBSCRIPTION);
    }

    public void checkCoupon(final Coupon coupon) {
        doRequest(context.getString(R.string.CHECK_COUPON), coupon, true, new ResponseCoupon(), CHECK_COUPON);
    }

    public void upladoMetrics(final UserMetric metrics) {
        doRequest(context.getString(R.string.SEND_METRICS_URL), metrics, false, new ResponseMetric(), SEND_METRICS);
    }

    public void sendFeedback(final Feedback feedback) {
        doRequest(context.getString(R.string.SEND_FEEDBACK), feedback, true, new ResponseFeedback(), SEND_FEEDBACK);
    }

    public void createGeofence(final UserGeofence geofence) {
        doRequest(context.getString(R.string.CREATE_GEOFENCE), geofence, false, new ResponseAddGeofence(), CREATE_GEOFENCE);
    }

    public void deleteGeofence(final UserGeofence geofence) {
        doRequest(context.getString(R.string.DELETE_GEOFENCE), geofence, false, new ResponseDefault(), DELETE_GEOFENCE);
    }

    public void getGeoActivities() {
        doRequest(context.getString(R.string.GET_GEOACTIVITIES), false, new ResponseGeoActivities(), GET_GEOACTIVITIES);
    }

    public void listAllGeofences() {
        doRequest(context.getString(R.string.LIST_ALL_GEOFENCES), false, new ResponseListGeofences(), LIST_ALL_GEOFENCES);
    }

    public void createTimeLock(final TimeLock timeLock) {
        doRequest(context.getString(R.string.CREATE_TIMELOCK), timeLock, false, new ResponseTimeLock(), CREATE_TIMELOCK);
    }

    public void deleteTimeLock(final TimeLock timeLock) {
        doRequest(context.getString(R.string.DELETE_TIMELOCK), timeLock, false, new ResponseTimeLock(), DELETE_TIMELOCK);
    }

    public void editTimeLock(final TimeLock timeLock) {
        doRequest(context.getString(R.string.EDIT_TIMELOCK), timeLock, false, new ResponseTimeLock(), EDIT_TIMELOCK);
    }

    public void listTimeLocks(Child child) {
        doRequest(context.getString(R.string.LIST_TIMELOCK), child, false, new ResponseTimeLock(), LIST_TIMELOCK);
    }

    public void checkEmail(final Parent parent) {
        doRequest(context.getString(R.string.CHECK_EMAIL_URL), parent, true, new ResponseParent(), CHECK_EMAIL);
    }

    public void registerParent(final Parent parent) {
        doRequest(context.getString(R.string.REGISTER_URL), parent, true, new ResponseParent(), REGISTER_PARENT);
    }

    public void login(final Parent parent) {
        doRequest(context.getString(R.string.LOGIN_URL), parent, false, new ResponseParent(), LOGIN);
    }

    public void resetPassword(final Parent parent) {
        doRequest(context.getString(R.string.RESET_PASSWORD_URL), parent, true, new ResponseParent(), RESET_PASSWORD);
    }

    public void edit(final Parent parent) {
        doRequest(context.getString(R.string.EDIT_PARENT), parent, true, new ResponseParent(), EDIT_PARENT);
    }

    public void sendParentToChildNotification(final ParentNotification parentNotification) {
        doRequest(context.getString(R.string.SEND_PARENT_TO_CHILD_NOTIFICATION), parentNotification, false, new ResponseNotification(), SEND_PARENT_TO_CHILD_NOTIFICATION);
    }

    public void changePassword(final Parent parent) {
        doRequest(context.getString(R.string.CHANGE_PASSWORD), parent, true, new ResponseParent(), CHANGE_PASSWORD);
    }

    public void sendLockedAndUnlockedApps(final RequestSendAppList requestSendAppList) {
        doRequest(context.getString(R.string.SEND_LOCKED_AND_UNLOCKED_APPS), requestSendAppList, false, new ResponseNotification(), SEND_LOCKED_AND_UNLOCKED_APPS);
    }

    public void getAlerts() {
        doRequest(context.getString(R.string.GET_ALERTS), true, new ResponseAlert(), GET_ALERTS);
    }

    public void readAlert(Alert alert) {
        doRequest(context.getString(R.string.READ_ALERT), alert, false, new ResponseAlert(), READ_ALERT);
    }

}