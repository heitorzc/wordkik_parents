package com.wordkik.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import me.pushy.sdk.Pushy;


/**
 * Created by heitorzc on 14/12/2016.
 */

public class GetPushyToken extends AsyncTask<Void, String, String> {

    private Context context;
    private PushyTokenInterface listener;

    public interface PushyTokenInterface{
        void onPushyTokenReady(String token);
    }

    public GetPushyToken(Context context){
        this.context = context;
        this.listener = (PushyTokenInterface) context;
    }

    public GetPushyToken(Context context, PushyTokenInterface listener){
        this.context  = context;
        this.listener = listener;
    }

    protected String doInBackground(Void... params) {

        String registrationID = "";

        try {

            registrationID = Pushy.register(context);
            Log.w("PUSHY", "ID: " + registrationID);

        }
        catch (Exception exc) {

        }

        return registrationID;
    }

    @Override
    protected void onPostExecute(String token) {

        if (listener != null && !token.isEmpty()){
            listener.onPushyTokenReady(token);
        }
        else if (listener != null && token.isEmpty()){
            listener.onPushyTokenReady(null);
        }

    }

}
