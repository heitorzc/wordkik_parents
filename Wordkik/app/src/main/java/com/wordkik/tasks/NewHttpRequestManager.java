package com.wordkik.tasks;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.jiongbull.jlog.JLog;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.objects.ResponseError;
import com.wordkik.views.CustomProgressDialog;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.OkHttpClient;

/**
 * Responsible for managing http requests
 *
 * @author Heitor Zanetti
 * @version 1.0.0
 */
public class NewHttpRequestManager implements JSONObjectRequestListener{

    private boolean                    showProgressDialog;
    private CustomProgressDialog       customProgressDialog;
    private String                     methodName;
    private Object                     responseObjectClass;
    private TaskManager.TaskListener   taskListener;
    private Context                    context;
    private HashMap<String, String>    headers;

    public NewHttpRequestManager(Context context, boolean showProgressDialog, Object responseObjectClass, String methodName, TaskManager.TaskListener taskListener) {
        this.context                   = context;
        this.showProgressDialog        = showProgressDialog;
        this.responseObjectClass       = responseObjectClass;
        this.methodName                = methodName;
        this.taskListener              = taskListener;

        headers = new HashMap<>();

        headers.put("Authorization",   Constants.user_token);
        headers.put("Accept-Language", "en-us");
        headers.put("Content-Type",    "application/json");
        headers.put("Accept-Encoding", "gzip, deflate");

    }



    public void post(String url, JSONObject params) {

        if (WordKik.hasInternet) {
            showProgressDialog();

            AndroidNetworking.post(getAbsoluteUrl(url))
                    .addJSONObjectBody(params)
                    .addHeaders(headers)
                    .setPriority(Priority.HIGH)
                    .setOkHttpClient(newOkHttpClient())
                    .build()
                    .getAsJSONObject(this);
        }
        else {
            Toast.makeText(context, R.string.emNoInternetConnection, Toast.LENGTH_SHORT).show();
        }
    }

    public void post(String url) {

        if (WordKik.hasInternet) {
            showProgressDialog();

            AndroidNetworking.post(getAbsoluteUrl(url))
                    .addHeaders(headers)
                    .setPriority(Priority.HIGH)
                    .setOkHttpClient(newOkHttpClient())
                    .build()
                    .getAsJSONObject(this);
        }
        else {
            Toast.makeText(context, R.string.emNoInternetConnection, Toast.LENGTH_SHORT).show();
        }
    }

    private OkHttpClient newOkHttpClient(){
        return new OkHttpClient().newBuilder()
                .retryOnConnectionFailure(true)
                .build();
    }

    private void showProgressDialog(){
        if (showProgressDialog) {
            this.customProgressDialog = new CustomProgressDialog(context);
            customProgressDialog.show();
        }
    }

    private void dismissProgressDialog(){
        if (customProgressDialog != null) {
            customProgressDialog.dismiss();
        }
    }

    private String getAbsoluteUrl(String relativeUrl) {
        return context.getString(R.string.BASE_URL) + relativeUrl;
    }



    @Override
    public void onResponse(JSONObject response) {
        Log.i("onSuccess: " + methodName, response.toString());
        JLog.json("onSuccess: " + methodName, response.toString());

        Object responseObject = new Gson().fromJson(response.toString(), responseObjectClass.getClass());

        if(taskListener != null) {
            taskListener.performTask(responseObject, methodName);
        }

        dismissProgressDialog();
    }

    @Override
    public void onError(ANError anError) {

        ResponseError error = new ResponseError("Error trying to get data from API.", false);
        Object responseObject = new Gson().fromJson(new Gson().toJson(error), responseObjectClass.getClass());

        if(taskListener != null) {
            taskListener.performTask(responseObject, methodName);
        }

        Log.w("REQUEST", "Error: " + error.getMessage());
        Log.e("REQUEST", "Error: " + anError.getResponse().toString());
    }
}