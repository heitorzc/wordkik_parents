package com.wordkik.tasks;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.baoyz.widget.PullRefreshLayout;
import com.wordkik.Constants;
import com.wordkik.adapters.AlertsAdapter;
import com.wordkik.fragments.AlertsFragment;
import com.wordkik.objects.Alert;
import com.wordkik.objects.ResponseAlert;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by heitorzc on 11/04/16.
 */
public class NotificationManager implements ParentTask.TaskListener, TaskManager.IMethodName {

    public interface NotificationListener {
        void performNotificationTask();
    }

    Context mContext;
    PullRefreshLayout pullRefreshLayout;
    AlertsAdapter adapter;
    ListView lvAlerts;

    private NotificationListener notificationListener;

    public NotificationManager(Context mContext){
        this.mContext = mContext;

    }

    public NotificationManager(Context mContext, PullRefreshLayout pullRefreshLayout, ListView lvAlerts, AlertsAdapter adapter){
        this.mContext = mContext;
        this.pullRefreshLayout = pullRefreshLayout;
        this.adapter = adapter;
        this.lvAlerts = lvAlerts;

    }

    public void updateCounter(int counter){
        Constants.alerts_counter = counter;
        updateBagde();
        updateMenuIcon();
    }

    public void updateBagde(){
        ShortcutBadger.applyCount(mContext, Constants.alerts_counter);
    }

    public void updateMenuIcon(){
        try {
            if (Constants.alerts_counter > 0) {
                Constants.hasNewAlerts = true;
                //BaseActivity.getNavigationView().getMenu().getItem(2).setIcon(R.drawable.alerts_active);
            } else {
                Constants.hasNewAlerts = false;
                //BaseActivity.getNavigationView().getMenu().getItem(2).setIcon(R.drawable.alert_section_icon);
            }
        }
        catch (NullPointerException e){
            Log.e("NOTIFICATION_MANAGER", "Icon null");
        }
    }

    public void updateNotificationStatus(Alert alert, boolean isRead){
        if(!isRead) {
            alert.setStatus("read"); // TODO: CHECK IF IT IS UPDATING THIS ALERT ON "Constants.alerts"

            new ParentTask(mContext, this).readAlert(alert);
            Constants.alerts_counter--;

            updateCounter(Constants.alerts_counter);

            Log.e("ALERT_COUNT_UPDATE", "new: " + Constants.alerts_counter);


        }
    }

    public void updateAndSetAdapter(ListView list, AlertsAdapter adapter){
        adapter = new AlertsAdapter(mContext, Constants.alerts);
        list.setAdapter(adapter);
    }

    public void getAlerts(NotificationListener notificationListener) {
        Log.e("CALLING: ", "GET ALERTS");

        this.notificationListener = notificationListener;

        new ParentTask(mContext, this).getAlerts();
    }

    @Override
    public void performTask(Object responseObject, String methodName) {
        Log.e("PERFORM_TASK", "METHOD: " + methodName + " CLASS: " + NotificationManager.class);

        switch (methodName) {
            case READ_ALERT:
                performReadAlert(responseObject);
                break;
            case GET_ALERTS:
                performGetAlerts(responseObject);
                break;
            default:
                Log.e("PERFORM_TASK", "METHOD NOT IMPLEMENTED");
                break;
        }

        updateCounter(countUnreadAlerts());

        if (notificationListener != null) {
            notificationListener.performNotificationTask();
        }
    }

    private void performReadAlert(Object responseObject){
        ResponseAlert responseAlert = (ResponseAlert) responseObject;

        if (responseAlert.isSuccess()) {
            Constants.alerts_counter--;
        } else {
            // TODO SHOW FEEDBACK TO USER
        }

    }

    private void performGetAlerts(Object responseObject) {
        ResponseAlert responseAlert = (ResponseAlert) responseObject;

        if (responseAlert.isSuccess()) {
            Constants.alerts = responseAlert.getAlerts();

            if (pullRefreshLayout != null){
                pullRefreshLayout.setRefreshing(false);
                updateAndSetAdapter(lvAlerts, adapter);

                if (Constants.alerts .size() > 0){
                    AlertsFragment.empty.setVisibility(View.INVISIBLE);
                } else {
                    AlertsFragment.empty.setVisibility(View.VISIBLE);
                }
            }
        } else {
            // TODO SHOW FEEDBACK TO USER
        }
    }

    private int countUnreadAlerts() {
        int total = 0;

        for (int i = 0; i < Constants.alerts.size(); i ++) {
            if (Constants.alerts.get(i).getStatus().equals("unread")) {
                total++;
            }
        }

        return total;
    }


}
