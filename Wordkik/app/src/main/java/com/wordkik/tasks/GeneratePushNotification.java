package com.wordkik.tasks;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.activities.LoginActivity;
import com.wordkik.activities.MainActivity;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Heitor Zanetti
 * 10/03/2016
 */
public class GeneratePushNotification extends AsyncTask<Void, Void, Void> {

    private GeneratePushNotificationInterface listener;
    private Context mContext;
    private String notification_type;
    private String category = "";
    private String title;
    private String message;
    private String child_name;
    private String child_photo;

    private NotificationCompat.Builder builder;

    public interface GeneratePushNotificationInterface {
        void onNotificationReady(Notification notification);
    }



    public GeneratePushNotification(Context mContext, String notification_type, String title, String message, String child_name, String child_photo, GeneratePushNotificationInterface listener) {
        this.listener           = listener;
        this.mContext           = mContext;
        this.notification_type  = notification_type;
        this.title              = title;
        this.message            = message;
        this.child_name         = child_name;
        this.child_photo        = child_photo;
    }

    public GeneratePushNotification(Context mContext, String notification_type, String category, String title, String message, String child_name, String child_photo, GeneratePushNotificationInterface listener) {
        this.listener           = listener;
        this.mContext           = mContext;
        this.notification_type  = notification_type;
        this.category           = category;
        this.title              = title;
        this.message            = message;
        this.child_name         = child_name;
        this.child_photo        = child_photo;
    }



    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }


    @Override
    protected Void doInBackground(Void... params) {

        builder = new NotificationCompat.Builder(mContext);
        Bitmap largeIcon = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.wkp_red_logo);

        try {
            if(child_photo != null) {
                URL photo = new URL(child_photo);
                largeIcon = BitmapFactory.decodeStream(photo.openConnection().getInputStream());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }

        int smallIcon = (Constants.SDK > 20) ? R.drawable.ic_white : R.drawable.wkp_red_logo;



        if (message.contains("%")) {
            message = message.replace("%", child_name);
        }


        if (notification_type.equals(Constants.NOTIFICATION_TYPE_ALERT)){

            if (category.equals(Constants.NOTIFICATION_CAT_GEOFENCE)){
                Intent intent = (Constants.isLoggedIn) ? openMainActivity(category) : openLoginActivity(category);
                PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, 0);
                builder.setContentIntent(pendingIntent);
            }

        }

        else if (notification_type.equals(Constants.NOTIFICATION_TYPE_CMS)) {

            if (category.equals(Constants.NOTIFICATION_CAT_UPDATE)){
                PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, openPlayStore(), 0);
                builder.setContentIntent(pendingIntent);
            }

            else if (category.equals(Constants.NOTIFICATION_CAT_GENERAL)){
                Intent intent = (Constants.isLoggedIn) ? openMainActivity(category) : openLoginActivity(category);
                PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, 0);
                builder.setContentIntent(pendingIntent);
            } //TODO Implement different categories.

        }



        builder.setLargeIcon(largeIcon);
        builder.setSmallIcon(smallIcon);
        builder.setAutoCancel(true);
        builder.setContentTitle(title);
        builder.setContentText(message);

        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(uri);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        builder.setVibrate(new long[]{0, 500, 100, 50, 100, 50, 100, 50, 100, 500});

        return null;
    }


    @Override
    protected void onPostExecute(Void aVoid) {

        if (listener != null) {
            listener.onNotificationReady(builder.build());
        }
    }



    private Intent openMainActivity(String extra){
        return new Intent(mContext, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra("intent", extra);
    }



    private Intent openLoginActivity(String extra){
        return new Intent(mContext, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra("intent", extra);
    }



    private Intent openPlayStore() {

        try{
            Intent rateIntent = rateIntentForUrl("market://details");
            return rateIntent;
        }
        catch (ActivityNotFoundException e){
            Intent rateIntent = rateIntentForUrl("http://play.google.com/store/apps/details");
            return rateIntent;
        }

    }



    private Intent rateIntentForUrl(String url) {

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, mContext.getPackageName())));
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;

        if (Build.VERSION.SDK_INT >= 21){
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        }
        else{
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }

        intent.addFlags(flags);
        return intent;
    }

}


