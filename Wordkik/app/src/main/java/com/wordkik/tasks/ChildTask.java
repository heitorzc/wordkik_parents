package com.wordkik.tasks;

import android.content.Context;

import com.wordkik.R;
import com.wordkik.objects.Child;
import com.wordkik.objects.RequestScreenLimit;
import com.wordkik.objects.ResponseChild;
import com.wordkik.objects.ResponseDefault;
import com.wordkik.objects.ResponseInstalledApps;
import com.wordkik.objects.ResponseScreenTime;
import com.wordkik.objects.ResponseStatistics;

/**
 * Created by dheringer on 4/24/2016.
 */
public class ChildTask extends TaskManager implements TaskManager.IMethodName {

    public ChildTask(Context context, TaskListener taskListener) {
        super(context, taskListener);
    }

    public void linkChildToParent(final Child child) {
        doRequest(context.getString(R.string.LINK_TO_PARENT), child, true, new ResponseChild(), LINK_TO_PARENT);
    }

    public void getChildren() {
        doRequest(context.getString(R.string.GET_CHILDREN), false, new ResponseChild(), GET_CHILDREN);
    }

    public void editChild(final Child child) {
        doRequest(context.getString(R.string.EDIT_CHILD), child, true, new ResponseChild(), EDIT_CHILD);
    }

    public void removeChild(final Child child) {
        doRequest(context.getString(R.string.REMOVE_CHILD), child, true, new ResponseChild(), REMOVE_CHILD);
    }

    public void getInstalledApps(final Child child) {
        doRequest(context.getString(R.string.GET_APPS), child, false, new ResponseInstalledApps(), GET_APPS);
    }

    public void getStatistics(final Child child) {
        doRequest(context.getString(R.string.GET_STATISTICS), child, false, new ResponseStatistics(), GET_STATISTICS);
    }

    public void updateScreenLimit(final RequestScreenLimit requestScreenLimit) {
        doRequest(context.getString(R.string.CREATE_SCREEN_LIMIT), requestScreenLimit, false, new ResponseDefault(), CREATE_SCREEN_LIMIT);
    }

    public void getScreenTimeLimits(final Child child) {
        doRequest(context.getString(R.string.GET_SCREEN_TIME), child, true, new ResponseScreenTime(), GET_SCREEN_TIME);
    }

}
