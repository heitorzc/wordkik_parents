package com.wordkik.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.wordkik.activities.CropPhoto;
import com.wordkik.views.UploadDialog;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by heitorzc on 04/04/16.
 */
public class AmazonS3Uploader extends AsyncTask<Void, Void, Void> implements ProgressListener{

    // TODO GENERATE USER ID

    private String amazon_id;
    private String amazon_api_key;
    private String file_name;
    private File picture;

    Context context;
    UploadDialog progressDialog;

    private long fileSize;
    private long progressSize;
    int progress = 0;

    private AmazonS3Client amazonS3Client;
    private AmazonS3UploaderListener amazonS3UploaderListener;

    public interface AmazonS3UploaderListener {
        void setPictureURL(URL url, String url2);
    }

    public AmazonS3Uploader(Context context, CropPhoto cropPhoto, String amazon_id, String amazon_api_key, String file_name, File newPhoto, AmazonS3UploaderListener amazonS3UploaderListener) {
        this.context = context;
        this.amazon_id = amazon_id;
        this.amazon_api_key = amazon_api_key;
        this.picture = newPhoto;
        this.file_name = file_name + ".jpg";
        this.amazonS3UploaderListener = amazonS3UploaderListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog = new UploadDialog(context);
        progressDialog.show();

    }

    @Override
    protected Void doInBackground(Void... params) {
        try {

            fileSize = picture.length();

            amazonS3Client = new AmazonS3Client(new BasicAWSCredentials(amazon_id, amazon_api_key));

            amazonS3Client.createBucket("wordkikbucket");

            PutObjectRequest putObjectRequest = new PutObjectRequest("wordkikbucket", file_name, picture);
            putObjectRequest.setGeneralProgressListener(this);

            PutObjectResult putObjectResult = amazonS3Client.putObject(putObjectRequest);

            GetObjectRequest getObjectRequest = new GetObjectRequest("wordkikbucket", file_name);
            S3Object s3Object = amazonS3Client.getObject(getObjectRequest);
            InputStream inputStream = s3Object.getObjectContent();

            inputStream.close();

           // Log.e("doInBackground", "calling from doInBackground");
            //setPictureURL();

        } catch (Exception ex) {
            Log.e("FAILURE", "ERROR UPLOADING PICUTRE TO AMAZON S3");

            ex.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Log.e("onPostExecute", "calling from onPostExecute");
        setPictureURL();
    }

    @Override
    public void progressChanged(ProgressEvent progressEvent) {
        progressSize = progressSize + progressEvent.getBytesTransferred();
        progress = (int) (100.0 * progressSize / fileSize);

        progressDialog.setProgress(progress);

        Log.e("AMAZOIN", "Progress: " + progress);
        if (progress == 100){
            progressDialog.dismiss();

            ((CropPhoto) context).finish();
        }

    }

    private void setPictureURL() {
        if (amazonS3UploaderListener == null) {
            return;
        }

        URL url = amazonS3Client.getUrl("wordkikbucket", file_name);
        String url2 = amazonS3Client.getResourceUrl("wordkikbucket", file_name);

        amazonS3UploaderListener.setPictureURL(url, url2);
    }
}
