package com.wordkik.tasks;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.jiongbull.jlog.JLog;

import org.json.JSONObject;


/**
 * Created by heitorzc on 09/02/16.
 */
public class TaskManager {

    protected Context context;
    private TaskListener taskListener;


    public interface TaskListener {
        void performTask(final Object responseObject, final String methodName);
    }

    public interface IMethodName {
        String CHECK_COUPON                       = "CHECK_COUPON";
        String SEND_CHAT_MESSAGE                  = "SEND_CHAT_MESSAGE";
        String GET_CHAT_MESSAGES                  = "GET_CHAT_MESSAGES";
        String SEND_SUBSCRIPTION                  = "SEND_SUBSCRIPTION";
        String SEND_STRIPE_SUBSCRIPTION           = "SEND_STRIPE_SUBSCRIPTION";
        String REGISTER_PARENT                    = "REGISTER_PARENT";
        String SEND_METRICS                       = "SEND_METRICS";
        String SEND_FEEDBACK                      = "SEND_FEEDBACK";
        String CREATE_GEOFENCE                    = "CREATE_GEOFENCE";
        String LIST_GEOFENCE                      = "LIST_GEOFENCE";
        String LIST_ALL_GEOFENCES                 = "LIST_ALL_GEOFENCEs";
        String DELETE_GEOFENCE                    = "DELETE_GEOFENCE";
        String GET_GEOACTIVITIES                  = "GET_GEOACTIVITIES";
        String CREATE_TIMELOCK                    = "CREATE_TIMELOCK";
        String CHECK_EMAIL                        = "CHECK_EMAIL";
        String LOGIN                              = "LOGIN";
        String RESET_PASSWORD                     = "RESET_PASSWORD";
        String EDIT_PARENT                        = "EDIT_PARENT";
        String LINK_TO_PARENT                     = "LINK_TO_PARENT";
        String SEND_PARENT_TO_CHILD_NOTIFICATION  = "SEND_PARENT_TO_CHILD_NOTIFICATION";
        String GET_CHILDREN                       = "GET_CHILDREN";
        String EDIT_CHILD                         = "EDIT_CHILD";
        String REMOVE_CHILD                       = "REMOVE_CHILD";
        String GET_SUBSCRIPTION_DETAILS           = "GET_SUBSCRIPTION_DETAILS";
        String CANCEL_SUBSCRIPTION                = "CANCEL_SUBSCRIPTION";
        String CHANGE_PASSWORD                    = "CHANGE_PASSWORD";
        String GET_APPS                           = "GET_APPS";
        String GET_STATISTICS                     = "GET_STATISTICS";
        String SEND_LOCKED_AND_UNLOCKED_APPS      = "SEND_LOCKED_AND_UNLOCKED_APPS";
        String GET_ALERTS                         = "GET_ALERTS";
        String GET_SCREEN_TIME                    = "GET_SCREEN_TIME";
        String READ_ALERT                         = "READ_ALERT";
        String DELETE_TIMELOCK                    = "DELETE_TIMELOCK";
        String EDIT_TIMELOCK                      = "EDIT_TIMELOCK";
        String LIST_TIMELOCK                      = "LIST_TIMELOCK";
        String ADD_TIMELOCK                       = "ADD_TIMELOCK ";
        String CREATE_SCREEN_LIMIT                = "CREATE_SCREEN_LIMIT";
    }



    public TaskManager(Context context, TaskListener taskListener) {
        this.context = context;
        this.taskListener = taskListener;
    }



    void doRequest(final String url, final boolean showProgressDialog, final Object responseObjectClass, final String methodName) {
        new NewHttpRequestManager(context, showProgressDialog, responseObjectClass, methodName, taskListener).post(url);
    }



    void doRequest(final String url, final Object param, final boolean showProgressDialog, final Object responseObjectClass, final String methodName) {
        Log.i("REQUEST",     new Gson().toJson(param));
        JLog.json("REQUEST", new Gson().toJson(param)); //This log won't appear on release version.

        try {

            JSONObject objParam = new JSONObject(new Gson().toJson(param));
            new NewHttpRequestManager(context, showProgressDialog, responseObjectClass, methodName, taskListener).post(url, objParam);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}

