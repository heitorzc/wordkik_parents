package com.wordkik.tasks;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by heitorzc on 11/04/16.
 */
public class GetUserEmail extends AsyncTask<Void, String, String> {

    private Context mContext;
    private UserEmailListener userEmailListener;

    AccountManager accountManager;

    public interface UserEmailListener {
        void onEmailReceived(String email);
    }

    public GetUserEmail(Context mContext, UserEmailListener userEmailListener) {
        this.mContext = mContext;
        this.userEmailListener = userEmailListener;
    }

    @Override
    protected void onPreExecute() {
        accountManager = AccountManager.get(mContext);
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params) {
        Account account = getAccount(accountManager);

        if (account == null) {
            Log.w("GETEMAIL", "Acc is null");
            return null;
        } else {
            Log.w("GETEMAIL", "Acc is " + account.name);
            return account.name;
        }
    }

    @Override
    protected void onPostExecute(String email) {

        if(userEmailListener != null){
            userEmailListener.onEmailReceived(email);
        }

    }

    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

}

