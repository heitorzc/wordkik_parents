package com.wordkik.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.wordkik.R;
import com.wordkik.utils.Encryption;
import com.wordkik.views.CustomProgressDialog;

/**
 * Created by heitorzc on 11/04/16.
 */
public class EncryptionManager extends AsyncTask<Void, Void, Void> {

    public static final int ENCRYPT_PASSWORD = 0;
    public static final int DECRYPT_PASSWORD = 1;
    int TASK_KEY = 0;

    String password;
    String ENCRYPT_KEY;
    String ENCRYPT_SALT;
    String pd_message;

    Context mContext;
    CustomProgressDialog pd;

    String safeString;

    private EncryptionListener encryptionListener;

    public interface EncryptionListener {
        void setSafeString(String safeString);
    }

    public EncryptionManager(Context mContext, String password, String ENCRYPT_KEY, String ENCRYPT_SALT, EncryptionListener encryptionListener, int TASK_KEY) {
        this.mContext = mContext;
        this.password = password;
        this.ENCRYPT_KEY = ENCRYPT_KEY;
        this.ENCRYPT_SALT = ENCRYPT_SALT;
        this.encryptionListener = encryptionListener;
        this.TASK_KEY = TASK_KEY;
        this.pd_message = mContext.getResources().getString(R.string.logging);
    }

    @Override
    protected void onPreExecute() {
        pd = new CustomProgressDialog(mContext, pd_message);
        //pd.show();
    }

    @Override
    protected Void doInBackground(Void... params) {

        switch (TASK_KEY){
            case ENCRYPT_PASSWORD:
                encrypt();
                break;
            case DECRYPT_PASSWORD:
                decrypt();
                break;
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {

        if(encryptionListener != null){
            encryptionListener.setSafeString(safeString);
        }

        pd.dismiss();
    }

    private void encrypt(){
        Encryption encryption = Encryption.getDefault(ENCRYPT_KEY, ENCRYPT_SALT, new byte[16]);
        safeString = encryption.encryptOrNull(password);
    }

    private void decrypt(){
        Encryption encryption = Encryption.getDefault(ENCRYPT_KEY, ENCRYPT_SALT, new byte[16]);
        safeString = encryption.decryptOrNull(password);
    }

}

