package com.wordkik.preferences;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.enums.GenderEnum;
import com.wordkik.objects.Parent;
import com.wordkik.objects.ResponseParent;
import com.wordkik.tasks.AmazonS3Uploader;
import com.wordkik.tasks.EncryptionManager;
import com.wordkik.tasks.ParentTask;
import com.wordkik.tasks.TaskManager;
import com.wordkik.views.ConfirmationDialog;

import java.net.URL;


/**
 * Created by heitorzc on 09/02/16.
 */
public class EditAccountDialog implements ParentTask.TaskListener, AmazonS3Uploader.AmazonS3UploaderListener, EncryptionManager.EncryptionListener, TaskManager.IMethodName {

    public interface EditProfileListener {
        void onFinishEditing(Parent parent);
    }

    private EditProfileListener editProfileListener;

    private Activity context;
    private Parent parent;
    private AlertDialog alertDialog;

    private EditText etOldPassword;
    private EditText etNewPassword;
    private EditText etConfirmPassword;

    public EditAccountDialog(Context context) {
        this.context = (Activity) context;
    }


    public void editProfile(EditProfileListener editProfileListener, final boolean persistentMode) {
        this.editProfileListener = editProfileListener;
        final CircularImageView ivPhoto;
        final EditText etName;
        final EditText etLastName;
        final EditText etPhoneNumber;
        final Spinner spAge;
        final RadioGroup rgGender;

        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.edit_profile, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(dialoglayout);

        ivPhoto = (CircularImageView) dialoglayout.findViewById(R.id.ivPhoto);

        Picasso.with(context).load(Constants.user_photo)
                .error(R.drawable.add_photo_green)
                .placeholder(R.drawable.add_photo_green).into(ivPhoto);

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WordKik.callPhotoPicker(context, Constants.user_id, ivPhoto, EditAccountDialog.this);
            }
        });

        etName = (EditText) dialoglayout.findViewById(R.id.etFirstName);
        etName.setText(Constants.user_name);

        etLastName = (EditText) dialoglayout.findViewById(R.id.etLastName);
        etLastName.setText(Constants.user_lastName);

        rgGender = (RadioGroup) dialoglayout.findViewById(R.id.rgGender);

        if (Constants.user_gender != null && Constants.user_gender.length() > 0) {
            if (Constants.user_gender.equals("M")) {
                rgGender.check(R.id.rbMale);
            } else {
                rgGender.check(R.id.rbFemale);
            }
        }

        etPhoneNumber = (EditText) dialoglayout.findViewById(R.id.etPhoneNumber);
        etPhoneNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        etPhoneNumber.setText(Constants.user_phone_number);

        spAge = (Spinner) dialoglayout.findViewById(R.id.spAge);
        spAge.setSelection(getSelectedAge(Constants.user_age));

        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setNegativeButton(context.getString(R.string.text_cancel), null);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.save), null);

        alertDialog = alertDialogBuilder.create();
        alertDialog.show();


        alertDialog.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateEditProfile(etName.getText().toString(),
                        etLastName.getText().toString(),
                        etPhoneNumber.getText().toString(),
                        spAge.getSelectedItemPosition(),
                        rgGender)) {
                    return;
                }

                setParentInfo(etName.getText().toString(),
                        etLastName.getText().toString(),
                        getSelectedGender(rgGender).getStringValue(),
                        etPhoneNumber.getText().toString(),
                        getSelectedAge(spAge));

                new ParentTask(context, EditAccountDialog.this).edit(parent);
            }

        });

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if(persistentMode) {
                    if (!validateEditProfile(etName.getText().toString(),
                            etLastName.getText().toString(),
                            etPhoneNumber.getText().toString(),
                            spAge.getSelectedItemPosition(),
                            rgGender)) {

                        alertDialog.show();

                    }
                }
            }
        });
    }

    private void setParentInfo(String name, String lastname, String gender, String phone, String age){
        if (parent == null) {
            parent = new Parent();
        }

        parent.setFirstName(name);
        parent.setLastName(lastname);
        parent.setGender(gender);
        parent.setPhoneNumber(phone);

        Constants.user_pushy_id = WordKik.prefs.getString("gcm_id", null);

        parent.setRegistration_id(Constants.user_pushy_id);
        parent.setAge(age);
        parent.setId(Integer.parseInt(Constants.user_id));
    }

    public int getSelectedAge(String age){
        if (age == null){
            return 0;
        }

        else if (age.equals("Less than 20")){
            return 1;
        }

        else if (age.equals("20 to 30")){
            return 2;
        }

        else if (age.equals("30 to 40")){
            return 3;
        }

        else if (age.equals("40 to 50")){
            return 4;
        }

        else if (age.equals("50 to 60")){
            return 5;
        }

        else {
            return 6;
        }
    }

    public String getSelectedAge(Spinner spinner){
        int selected = spinner.getSelectedItemPosition();
        String age = "";

        switch (selected){
            case 1:
                age = "Less than 20";
                break;
            case 2:
                age = "20 to 30";
                break;
            case 3:
                age = "30 to 40";
                break;
            case 4:
                age = "40 to 50";
                break;
            case 5:
                age = "50 to 60";
                break;
            case 6:
                age = "More than 60";
                break;
        }

        return age;
    }

    /**
     * Function to get gender selected
     *
     * @param rgGender radio group gender to be checked
     * @return GenderEnum if the gender is selected, null if nothing is selected
     */
    private GenderEnum getSelectedGender(RadioGroup rgGender) {
        switch (rgGender.getCheckedRadioButtonId()) {
            case R.id.rbMale:
                return GenderEnum.MALE;
            case R.id.rbFemale:
                return GenderEnum.FEMALE;
            default:
                return null;
        }
    }

    public void changePassword() {

        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.change_password, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(dialoglayout);

        etOldPassword = (EditText) dialoglayout.findViewById(R.id.etOldPassword);
        etNewPassword = (EditText) dialoglayout.findViewById(R.id.etNewPassword);
        etConfirmPassword = (EditText) dialoglayout.findViewById(R.id.etConfirmPassword);

        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setNegativeButton(context.getString(R.string.text_cancel), null);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.save), null);

        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        alertDialog.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateChangePassoword(etOldPassword.getText().toString(), etNewPassword.getText().toString(), etConfirmPassword.getText().toString())) {
                    parent = new Parent();
                    parent.setOld_password(etOldPassword.getText().toString());
                    parent.setNew_password(etNewPassword.getText().toString());

                    new ParentTask(context, EditAccountDialog.this).changePassword(parent);
                }
            }

        });
    }

    @Override
    public void performTask(Object responseObject, String methodName) {
        Log.e("PERFORM_TASK", "METHOD: " + methodName + " CLASS: " + EditAccountDialog.class);

        switch (methodName) {
            case EDIT_PARENT:
                performEditParent(responseObject);
                break;
            case CHANGE_PASSWORD:
                performChangePassword(responseObject);
                break;
            default:
                Log.e("PERFORM_TASK", "METHOD NOT IMPLEMENTED");
                break;
        }
    }

    private void performEditParent(Object responseObject) {
        ResponseParent responseParent = (ResponseParent) responseObject;

        if (responseParent.isSuccess()) {
            alertDialog.dismiss();

            Constants.user_pushy_id = parent.getRegistration_id();

            if (parent.getPhoto() != null && !parent.getPhoto().trim().equals("")) {
                Constants.user_photo = parent.getPhoto();
            }

            Constants.user_name         = parent.getFirstName();
            Constants.user_lastName     = parent.getLastName();
            Constants.user_phone_number = parent.getPhoneNumber();
            Constants.user_country      = parent.getCountry();
            Constants.user_state        = parent.getState();
            Constants.user_age          = parent.getAge();
            Constants.user_gender       = parent.getGender();

            Toast.makeText(context, context.getString(R.string.profile_updated_successfuly), Toast.LENGTH_SHORT).show();

            if (editProfileListener != null) {
                editProfileListener.onFinishEditing(parent);
            }

        }
        else {
            ConfirmationDialog.with(context)
                    .title(R.string.title_activity_editprofile)
                    .message(R.string.something_wrong_msg)
                    .background(R.color.red)
                    .show();
        }
    }

    private void performChangePassword(Object responseObject) {
        ResponseParent responseParent = (ResponseParent) responseObject;

        if (responseParent.isSuccess()) {
            alertDialog.dismiss();

            saveCredentials();

            Toast.makeText(context, context.getString(R.string.password_updated_successfuly), Toast.LENGTH_SHORT).show();
        }
        else {
            ConfirmationDialog.with(context)
                    .title(R.string.change_password)
                    .message(generateFeedbackMessage(responseParent.getMessage()))
                    .background(R.color.red)
                    .show();
        }
    }

    public String generateFeedbackMessage(String response){
        String msg = context.getString(R.string.something_wrong_msg);

        if (response.contains("Old password not matching current password")){
            msg = context.getString(R.string.pass_not_match_error);
            return msg;
        }

        if (response.contains("Password has been used before")){
            msg = context.getString(R.string.pass_used_before);
            return msg;
        }


        return msg;
    }

    private boolean validateChangePassoword(String oldPassword, String newPassword, String confirmNewPassword) {

        if ("".equals(newPassword.trim())) {
            Toast.makeText(context, R.string.emPasswordCannotBeBlank, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (newPassword.length() < Integer.valueOf(context.getString(R.string.PASSWORD_MIN_LENGTH))) {
            Toast.makeText(context, R.string.emPasswordIsTooShort, Toast.LENGTH_SHORT).show();
            return false;
        }

        if ("".equals(confirmNewPassword.trim())) {
            Toast.makeText(context, R.string.emConfirmPasswordCannotBeBlank, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!newPassword.equals(confirmNewPassword)) {
            Toast.makeText(context, R.string.emPasswordDoesNotMatch, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    public void setPictureURL(URL url, String url2) {
        parent = new Parent();
        parent.setPhoto(url2);
        Log.e("URL", url != null ? url.toString() : "null");
        Log.e("string url", url2 != null ? url2 : "null");
    }

    private void saveCredentials() {
        Log.e("new pass", parent.getNew_password());

        String ENCRYPT_KEY = context.getString(R.string.encrypt_key);
        String ENCRYPT_SALT = context.getString(R.string.encrypt_salt);

        new EncryptionManager(context, parent.getNew_password(), ENCRYPT_KEY, ENCRYPT_SALT, this, EncryptionManager.ENCRYPT_PASSWORD).execute();
    }

    @Override
    public void setSafeString(String safeString) {
        SharedPreferences.Editor editor = WordKik.prefs.edit();

        editor.putString(Constants.USER_PASS_KEY, safeString);

        editor.commit();
    }

    private boolean validateEditProfile(String firstName, String lastName, String phoneNumber, int age, RadioGroup rgGender) {
        if (firstName == null || "".equals(firstName.trim())) {
            Toast.makeText(context, R.string.emFirstNameCannotBeBlank, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (lastName == null || "".equals(lastName.trim())) {
            Toast.makeText(context, R.string.emLastNameCannotBeBlank, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (getSelectedGender(rgGender) == null) {
            Toast.makeText(context, R.string.emGenderNotSelected, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (age <= 0) {
            Toast.makeText(context, R.string.emAgeNotSelected, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (phoneNumber == null || "".equals(phoneNumber.trim())) {
            Toast.makeText(context, R.string.emPhoneNumberCannotBeBlank, Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!WordKik.hasInternet) {
            Toast.makeText(context, R.string.emNoInternetConnection, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

}
