package com.wordkik.preferences;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.MenuItem;

import com.wordkik.Constants;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.activities.CropPhoto;
import com.wordkik.activities.ManagePincodeActivity;
import com.wordkik.activities.WebViewActivity;
import com.wordkik.views.FeedbackDialog;

import java.io.File;

import butterknife.BindString;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.tajchert.nammu.Nammu;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p/>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends AppCompatPreferenceActivity {
    @BindString(R.string.app_name)       String app_name;


    public static final String EXTRA_SHOW_FRAGMENT = ":android:show_fragment";
    public static final String EXTRA_NO_HEADERS = ":android:no_headers";

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {

            String stringValue = value.toString();
            preference.setSummary(stringValue);

            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();

    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || GeneralPreferenceFragment.class.getName().equals(fragmentName);
    }


    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends PreferenceFragment implements Preference.OnPreferenceClickListener{
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);

            PreferenceCategory account = (PreferenceCategory) getPreferenceScreen().findPreference("pref_key_account");
            PreferenceCategory logout = (PreferenceCategory) getPreferenceScreen().findPreference("pref_key_logout");
            PreferenceCategory about = (PreferenceCategory) getPreferenceScreen().findPreference("pref_key_about");
            account.findPreference("profile").setOnPreferenceClickListener(this);
            account.findPreference("password").setOnPreferenceClickListener(this);
            account.findPreference("pincode").setOnPreferenceClickListener(this);
            logout.findPreference("logout").setOnPreferenceClickListener(this);
            about.findPreference("feedback").setOnPreferenceClickListener(this);
            about.findPreference("faq").setOnPreferenceClickListener(this);

            about.findPreference("version").setSummary(Constants.app_version);

        }

        @Override
        public boolean onPreferenceClick(Preference preference) {
            if(preference.getKey().equals("profile")) {
                new EditAccountDialog(getActivity()).editProfile(null, false);
            }
            if(preference.getKey().equals("password")) {
                new EditAccountDialog(getActivity()).changePassword();
            }
            if(preference.getKey().equals("pincode")) {
                startActivity(new Intent(getActivity(), ManagePincodeActivity.class));
            }
            if(preference.getKey().equals("faq")) {
                startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("url", "http://www.wordkik.com/faq"));
            }
            if(preference.getKey().equals("logout")) {
                logout(getActivity());
            }
            if(preference.getKey().equals("feedback")) {
                new FeedbackDialog(getActivity()).show();
            }
            return true;
        }
    }

    public static void logout(Context mContext){

        WordKik.prefs.edit().remove(Constants.USER_EMAIL_KEY).apply();
        WordKik.prefs.edit().remove(Constants.USER_PASS_KEY).apply();

        Constants.hasNewAlerts        = false;
        Constants.isLoggedIn          = false;
        Constants.isSubscriptionValid = true;

        Constants.user_id             = "";
        Constants.user_name           = "";
        Constants.user_lastName       = "";
        Constants.user_email          = "";
        Constants.user_phone_number   = "";
        Constants.user_country        = "";
        Constants.user_token          = "";
        Constants.user_gender         = "";
        Constants.user_account_type   = "";
        Constants.user_expiration     = 0 ;
        Constants.user_age            = "";
        Constants.user_pincode        = "";
        Constants.user_state          = "";

        Constants.childs.clear();
        Constants.alerts.clear();


        ((Activity) mContext).finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("REQUEST_CODE", ":: " + requestCode + " ResultCode: " + resultCode);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                Log.d("Photo", e.getMessage().toString());
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                startActivity(new Intent(SettingsActivity.this, CropPhoto.class).putExtra("newPhoto", imageFile.getAbsolutePath())
                        .putExtra("file_name", WordKik.file_name));

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }
}
