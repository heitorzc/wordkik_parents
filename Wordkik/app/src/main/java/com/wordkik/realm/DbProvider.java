package com.wordkik.realm;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.support.v4.app.Fragment;

import com.wordkik.objects.Metric;

import io.realm.Realm;
import io.realm.RealmResults;


public class DbProvider {

    private static DbProvider instance;
    private final Realm realm;

    public DbProvider(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static DbProvider with(Fragment fragment) {

        if (instance == null) {
            instance = new DbProvider(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static DbProvider with(Activity activity) {

        if (instance == null) {
            instance = new DbProvider(activity.getApplication());
        }
        return instance;
    }

    public static DbProvider with(Application application) {

        if (instance == null) {
            instance = new DbProvider(application);
        }
        return instance;
    }


    public static DbProvider with(Service service) {

        if (instance == null) {
            instance = new DbProvider(service.getApplication());
        }
        return instance;
    }


    public static DbProvider getInstance() {
        return instance;
    }



    public Realm getRealm() {
        return realm;
    }



    public void clearStoredMetrics() {

        realm.beginTransaction();
        realm.delete(Metric.class);
        realm.commitTransaction();
    }


    public void registerNewMetric(Metric metric){

        realm.beginTransaction();
        realm.copyToRealm(metric);
        realm.commitTransaction();

    }



    public RealmResults<Metric> getStoredMetrics() {
        return realm.where(Metric.class).findAll();
    }



    public boolean hasStoredMetrics() {
        return !realm.where(Metric.class).findAll().isEmpty();
    }


}














/*
        //query a single item with the given id
    public Parent getMetric(String token) {

        return realm.where(Parent.class).equalTo("auth_token", token).findFirst();
    }

    //query example
    public RealmResults<Metric> queryedParentss() {

        return realm.where(Metric.class)
                .contains("first_name", "Jessica")
                .or()
                .contains("last_name", "Abrego")
                .findAll();

    }*/
