package com.wordkik.objects;

/**
 * Created by heitorzc on 23/08/16.
 */
public class ResponseAddGeofence {

    boolean success;
    String message;
    UserGeofence geofence;

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public UserGeofence getGeofence() {
        return geofence;
    }
}
