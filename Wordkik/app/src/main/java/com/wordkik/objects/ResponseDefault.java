package com.wordkik.objects;

/**
 * Created by heitorzc on 28/08/16.
 */
public class ResponseDefault {

    boolean success;
    String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
