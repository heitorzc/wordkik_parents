package com.wordkik.objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by heitorzc on 04/01/2017.
 */

public class RequestScreenLimit implements Serializable {

    private String profile_id;
    private ArrayList<ScreenTime> screen_limits;

    public RequestScreenLimit(String profile_id, ArrayList<ScreenTime> screen_limits) {
        this.profile_id = profile_id;
        this.screen_limits = screen_limits;
    }

    public String getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(String profile_id) {
        this.profile_id = profile_id;
    }

    public ArrayList<ScreenTime> getScreen_limits() {
        return screen_limits;
    }

    public void setScreen_limits(ArrayList<ScreenTime> screen_limits) {
        this.screen_limits = screen_limits;
    }
}
