package com.wordkik.objects;

import java.io.Serializable;

public class ResponsePurchase implements Serializable {

    private boolean success;
    private String message;
    private int subscription_left;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getSubscription_left() {
        return subscription_left;
    }

    public void setSubscription_left(int subscription_left) {
        this.subscription_left = subscription_left;
    }
}
