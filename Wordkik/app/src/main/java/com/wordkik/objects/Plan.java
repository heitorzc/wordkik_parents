package com.wordkik.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by heitorzc on 17/08/16.
 */
public class Plan implements Parcelable {

    String title;
    String currency;
    String price;
    String play_id;
    String stripe_id;
    String oldPrice;
    String saving;

    public Plan(String title, String currency, String price, String play_id, String stripe_id, String oldPrice, String saving) {
        this.title = title;
        this.currency = currency;
        this.price = price;
        this.play_id = play_id;
        this.stripe_id = stripe_id;
        this.oldPrice = oldPrice;
        this.saving = saving;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPlay_id() {
        return play_id;
    }

    public void setPlay_id(String play_id) {
        this.play_id = play_id;
    }

    public String getStripe_id() {
        return stripe_id;
    }

    public void setStripe_id(String stripe_id) {
        this.stripe_id = stripe_id;
    }

    public String getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(String oldPrice) {
        this.oldPrice = oldPrice;
    }

    public String getSaving() {
        return saving;
    }

    public void setSaving(String saving) {
        this.saving = saving;
    }

    protected Plan(Parcel in) {
        title = in.readString();
        currency = in.readString();
        price = in.readString();
        play_id = in.readString();
        stripe_id = in.readString();
        oldPrice = in.readString();
        saving = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(currency);
        dest.writeString(price);
        dest.writeString(play_id);
        dest.writeString(stripe_id);
        dest.writeString(oldPrice);
        dest.writeString(saving);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Plan> CREATOR = new Parcelable.Creator<Plan>() {
        @Override
        public Plan createFromParcel(Parcel in) {
            return new Plan(in);
        }

        @Override
        public Plan[] newArray(int size) {
            return new Plan[size];
        }
    };
}