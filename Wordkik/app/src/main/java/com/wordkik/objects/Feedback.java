package com.wordkik.objects;

import java.io.Serializable;

/**
 * Created by heitorzc on 07/06/16.
 */
public class Feedback implements Serializable {

    String satisfaction;
    String message;

    public Feedback(String satisfaction, String message) {
        this.satisfaction = satisfaction;
        this.message = message;
    }

    public String getSatisfaction() {
        return satisfaction;
    }

    public void setSatisfaction(String satisfaction) {
        this.satisfaction = satisfaction;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
