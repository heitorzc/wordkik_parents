package com.wordkik.objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by dheringer on 5/1/2016.
 */
public class ResponseAlert implements Serializable {

    private boolean success;
    private String message;
    private ArrayList<Alert> alerts;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Alert> getAlerts() {
        return alerts;
    }

    public void setAlerts(ArrayList<Alert> alerts) {
        this.alerts = alerts;
    }
}
