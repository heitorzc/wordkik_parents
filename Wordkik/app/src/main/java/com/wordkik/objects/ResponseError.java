package com.wordkik.objects;

/**
 * Created by heitorzc on 12/10/2016.
 */

public class ResponseError {

    String message;
    boolean success;

    public ResponseError(String message, boolean success) {
        this.message = message;
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
