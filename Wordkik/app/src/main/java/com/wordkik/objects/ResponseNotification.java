package com.wordkik.objects;

import java.io.Serializable;

/**
 * Created by pen15 on 4/24/2016.
 */
public class ResponseNotification implements Serializable {

    private boolean success;
    private String message;
    private NotificationMessage response;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public NotificationMessage getResponse() {
        return response;
    }

    public void setResponse(NotificationMessage response) {
        this.response = response;
    }
}
