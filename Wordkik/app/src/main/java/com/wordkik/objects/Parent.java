package com.wordkik.objects;

import com.wordkik.BuildConfig;
import com.wordkik.Constants;

import java.io.Serializable;



/**
 * Model to describe a parent
 *
 * @author Denis Heringer
 * @version 1.0.0
 */
public class Parent implements Serializable {

    private String make             = android.os.Build.MANUFACTURER;
    private String brand            = android.os.Build.BRAND;
    private String model            = android.os.Build.MODEL;
    private String api_version      = "Android API " + Constants.SDK;
    private String release_version  = android.os.Build.VERSION.RELEASE;
    private String apk_version      = BuildConfig.VERSION_NAME;

    private int id; // THIS FIED IS UNIQUE ON THE DB
    //private String registration_id;
    private String registration_id;
    private String gcm_id = "invalid";
    private String firstName;
    private String lastName;
    private String age;
    private String email;
    private String password;
    private String gender;
    private String photo;
    private String country;
    private String state;
    private String phoneNumber;
    private String auth_token;
    private String account_type;
    private int subscription_left;
    private String old_password;
    private String new_password;
    private String pincode;
    private String customer_id; // THIS FIELD IS RELATED TO STRIPE SUBSCRIPTION
    private boolean app_up_to_date;


    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getApi_version() {
        return api_version;
    }

    public void setApi_version(String api_version) {
        this.api_version = api_version;
    }

    public String getRelease_version() {
        return release_version;
    }

    public void setRelease_version(String release_version) {
        this.release_version = release_version;
    }




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getRegistration_id() {
        return registration_id;
    }

    public void setRegistration_id(String registration_id) {
        this.registration_id = registration_id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public int getSubscription_left() {
        return subscription_left;
    }

    public void setSubscription_left(int subscription_left) {
        this.subscription_left = subscription_left;
    }

    public String getOld_password() {
        return old_password;
    }

    public void setOld_password(String old_password) {
        this.old_password = old_password;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getApk_version() {
        return apk_version;
    }

    public void setApk_version(String apk_version) {
        this.apk_version = apk_version;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isApp_up_to_date() {
        return app_up_to_date;
    }

    public void setApp_up_to_date(boolean app_up_to_date) {
        this.app_up_to_date = app_up_to_date;
    }
}
