package com.wordkik.objects;

import java.io.Serializable;

/**
 * Created by pen15 on 4/25/2016.
 */
public class ParentNotification implements Serializable {

    private String profile_id;
    private NotificationData data;
    private Notification notification;

    public String getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(String profile_id) {
        this.profile_id = profile_id;
    }

    public NotificationData getData() {
        return data;
    }

    public void setData(NotificationData data) {
        this.data = data;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }
}
