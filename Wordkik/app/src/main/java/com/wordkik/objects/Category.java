package com.wordkik.objects;

import java.io.Serializable;

/**
 * Created by heitorzc on 24/06/16.
 */
public class Category implements Serializable {

    String name;
    float usage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getUsage() {
        return usage;
    }

    public void setUsage(float usage) {
        this.usage = usage;
    }
}
