package com.wordkik.objects;

import java.io.Serializable;

public class Alert implements Serializable{
    private String id;
    private String category;
    private String status;
	private String child_profile_id;
	private String received;
    private String app_name;

    public Alert(String id, String category, String status, String child_profile_id, String received) {
        this.id = id;
        this.category = category;
        this.status = status;
        this.child_profile_id = child_profile_id;
        this.received = received;
    }

    public Alert() {}

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getChild_profile_id() {
        return child_profile_id;
    }

    public void setChild_profile_id(String child_profile_id) {
        this.child_profile_id = child_profile_id;
    }

    public String getReceived() {
        return received;
    }

    public void setReceived(String received) {
        this.received = received;
    }
}