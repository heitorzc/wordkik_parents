package com.wordkik.objects;

/**
 * Created by heitorzc on 16/06/16.
 */
public class PurchaseData {

    String product_id;
    String purchase_token;
    String order_id;

    public PurchaseData(String product_id, String purchase_token, String order_id) {
        this.product_id = product_id;
        this.purchase_token = purchase_token;
        this.order_id = order_id;
    }

    public String getProductId() {
        return product_id;
    }

    public void setProductId(String productId) {
        this.product_id = product_id;
    }

    public String getPurchaseToken() {
        return purchase_token;
    }

    public void setPurchaseToken(String purchase_token) {
        this.purchase_token = purchase_token;
    }

    public String getOrderId() {
        return order_id;
    }

    public void setOrderId(String orderId) {
        this.order_id = order_id;
    }
}
