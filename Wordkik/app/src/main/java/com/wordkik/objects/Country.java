package com.wordkik.objects;

import java.util.ArrayList;

/**
 * Created by heitorzc on 11/07/16.
 */
public class Country {

    String name_en;
    String name_pt;
    String code;
    String currency;
    String monthly_price;
    String yearly_price;
    ArrayList<String> states;

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getName_pt() {
        return name_pt;
    }

    public void setName_pt(String name_pt) {
        this.name_pt = name_pt;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ArrayList<String> getStates() {
        return states;
    }

    public void setStates(ArrayList<String> states) {
        this.states = states;
    }

    public String getMonthly_price() {
        return monthly_price;
    }

    public void setMonthly_price(String monthly_price) {
        this.monthly_price = monthly_price;
    }

    public String getYearly_price() {
        return yearly_price;
    }

    public void setYearly_price(String yearly_price) {
        this.yearly_price = yearly_price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
