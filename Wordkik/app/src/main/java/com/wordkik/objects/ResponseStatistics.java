package com.wordkik.objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by dheringer on 5/1/2016.
 */
public class ResponseStatistics implements Serializable {

    private boolean success;
    private String message;
    private int blocked_websites;
    private String screen_time;
    private ArrayList<App> most_used_apps;
    private ArrayList<Category> top_categories;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getBlocked_websites() {
        return blocked_websites;
    }

    public void setBlocked_websites(int blocked_websites) {
        this.blocked_websites = blocked_websites;
    }

    public String getScreen_time() {
        return screen_time;
    }

    public void setScreen_time(String screen_time) {
        this.screen_time = screen_time;
    }

    public ArrayList<App> getMost_used_apps() {
        return most_used_apps;
    }

    public void setMost_used_apps(ArrayList<App> most_used_apps) {
        this.most_used_apps = most_used_apps;
    }

    public ArrayList<Category> getTop_categories() {
        return top_categories;
    }

    public void setTop_categories(ArrayList<Category> top_categories) {
        this.top_categories = top_categories;
    }
}
