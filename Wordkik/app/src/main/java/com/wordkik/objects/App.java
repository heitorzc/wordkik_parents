package com.wordkik.objects;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.io.Serializable;
import java.util.ArrayList;


public class App extends ExpandableGroup<DailyUsage> implements Serializable {

    private static final long serialVersionUID = 8410225478837821066L;

    private String id;
    private String name;
    private String package_name;
    private String icon;
    private String category;
    private String usage_in_time;
    private ArrayList<DailyUsage> daily_usages = new ArrayList<>();
    private ArrayList<WeeklyUsage> weekly_usages = new ArrayList<>();
    private boolean locked;


    public App(String app_name, String app_icon, ArrayList<DailyUsage> usages) {
        super(app_name, usages);
        this.name = app_name;
        this.icon = app_icon;
    }


    public String getId() {
        return id;
    }



    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }



    public void setName(String name) {
        this.name = name;
    }



    public String getPackage_name() {
        return package_name;
    }



    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }


    public String getIcon() {
        return icon;
    }



    public void setIcon(String icon) {
        this.icon = icon;
    }



    public String getCategory() {
        return category;
    }



    public void setCategory(String category) {
        this.category = category;
    }



    public String getUsage_in_time() {
        return usage_in_time;
    }



    public void setUsage_in_time(String usage_in_time) {
        this.usage_in_time = usage_in_time;
    }



    public ArrayList<DailyUsage> getDaily_usages() {
        return daily_usages;
    }



    public void setDaily_usages(ArrayList<DailyUsage> daily_usages) {
        this.daily_usages = daily_usages;
    }



    public void updateWeekdaysUsage(String days, String usage){

        if (!weekly_usages.isEmpty()){
            for(WeeklyUsage wUsage : weekly_usages){
                if (wUsage.getDays().equals(days)){
                    wUsage.setUsage(usage);
                }
            }
        }
        else {
            weekly_usages.add(new WeeklyUsage(days, usage));
        }

        if (days.equals("weekdays")){
            setDaily_usage(1, usage);
            setDaily_usage(2, usage);
            setDaily_usage(3, usage);
            setDaily_usage(4, usage);
            setDaily_usage(5, usage);
        }
        else{
            setDaily_usage(0, usage);
            setDaily_usage(6, usage);
        }

    }



    public String getWeekdaysUsage(){

        if (!getWeekly_usages().isEmpty()){
            for (WeeklyUsage usage : getWeekly_usages()){
                if (usage.getDays().equals("weekdays")){
                    return usage.getUsage();
                }
            }
        }

        return getLowestUsageWeekday(1, 5);
    }



    public String getWeekendsUsage(){

        if (!getWeekly_usages().isEmpty()){
            for (WeeklyUsage usage : getWeekly_usages()){
                if (usage.getDays().equals("weekend")){
                    return usage.getUsage();
                }
            }
        }

        return getLowestUsageWeekend();
    }



    private String getLowestUsageWeekday(int min, int max) {

        int maxValue = 0;
        String lUsage = "Unlocked";

        if (getDaily_usages() != null && !getDaily_usages().isEmpty()) {

            for (DailyUsage usage : daily_usages) {

                if (usage.getWeekday() >= min && usage.getWeekday() <= max) {

                    if (!usage.getUsage().equals("Unlocked") && !usage.getUsage().equals("Locked")) {

                        if (Integer.parseInt(usage.getUsage().replace(":", "")) > maxValue) {
                            maxValue = Integer.parseInt(usage.getUsage().replace(":", ""));
                            lUsage = usage.getUsage();
                        }

                    }
                    else if (usage.getUsage().equals("Locked")) {
                        lUsage = usage.getUsage();
                    }
                }

            }

        }

        return lUsage;
    }



    private String getLowestUsageWeekend() {

        String lUsage = "Unlocked";

        if (getDaily_usages() != null && !getDaily_usages().isEmpty()) {

            if (!getDaily_usage(0).equals("Unlocked") && !getDaily_usage(0).equals("Locked") && !getDaily_usage(6).equals("Unlocked") && !getDaily_usage(6).equals("Locked")) {

                if (Integer.parseInt(getDaily_usage(0).replace(":", "")) >= Integer.parseInt(getDaily_usage(6).replace(":", ""))) {
                    lUsage = getDaily_usage(6);
                }
                else {
                    lUsage = getDaily_usage(0);
                }

            }
            else if (getDaily_usage(0).equals("Locked") || getDaily_usage(6).equals("Locked")) {
                lUsage = "Locked";
            }

        }

        return lUsage;
    }



    public String getDaily_usage(int weekday) {
        for (DailyUsage usage : daily_usages){
            if (usage.getWeekday() == weekday){
                return usage.getUsage();
            }
        }

        return "Unlocked";
    }



    private DailyUsage getDaily_usage(int weekday, boolean u) {
        for (DailyUsage usage : daily_usages){
            if (usage.getWeekday() == weekday){
                return usage;
            }
        }

        return this.getDaily_usages().get(weekday);
    }



    public void setDaily_usage(int weekday, String usage) {
        this.getDaily_usage(weekday, true).setUsage(usage);
    }


    public boolean hasUsageLimit(){
        for (DailyUsage usage : getDaily_usages()){
            if (!usage.getUsage().equals("Unlocked")){
                return true;
            }
        }

        return false;
    }



    public boolean isLocked() {
        return locked;
    }



    public void setLocked(boolean locked) {
        this.locked = locked;
    }



    public ArrayList<WeeklyUsage> getWeekly_usages() {
        return weekly_usages;
    }



    public void setWeekly_usages(ArrayList<WeeklyUsage> weekly_usages) {
        this.weekly_usages = weekly_usages;
    }
}