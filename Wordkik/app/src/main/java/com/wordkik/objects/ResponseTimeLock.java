package com.wordkik.objects;

import java.io.Serializable;
import java.util.ArrayList;

public class ResponseTimeLock implements Serializable {

    private boolean success;
    private String message;
    ArrayList<TimeLock> time_locks;
    ArrayList<TimeLock> overlapping;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<TimeLock> getTimelocks() {
        return time_locks;
    }

    public void setTimelocks(ArrayList<TimeLock> timelocks) {
        this.time_locks = timelocks;
    }

    public ArrayList<TimeLock> getTime_locks() {
        return time_locks;
    }

    public void setTime_locks(ArrayList<TimeLock> time_locks) {
        this.time_locks = time_locks;
    }

    public ArrayList<TimeLock> getOverlapping() {
        return overlapping;
    }

    public void setOverlapping(ArrayList<TimeLock> overlapping) {
        this.overlapping = overlapping;
    }
}
