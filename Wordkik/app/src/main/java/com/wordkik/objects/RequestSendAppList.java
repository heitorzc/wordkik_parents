package com.wordkik.objects;

import java.io.Serializable;
import java.util.List;

/**
 * Created by dheringer on 5/1/2016.
 */
public class RequestSendAppList implements Serializable {

    private String profile_id;
    private List<App> apps;

    public String getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(String profile_id) {
        this.profile_id = profile_id;
    }

    public List<App> getApps() {
        return apps;
    }

    public void setApps(List<App> apps) {
        this.apps = apps;
    }
}
