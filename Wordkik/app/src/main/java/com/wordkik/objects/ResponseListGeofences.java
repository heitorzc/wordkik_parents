package com.wordkik.objects;

import java.util.ArrayList;

/**
 * Created by heitorzc on 23/08/16.
 */
public class ResponseListGeofences {

    boolean success;
    String message;
    ArrayList<UserGeofence> geofences;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<UserGeofence> getGeofences() {
        return geofences;
    }

    public void setGeofences(ArrayList<UserGeofence> geofences) {
        this.geofences = geofences;
    }
}
