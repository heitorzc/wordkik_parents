package com.wordkik.objects;

import java.util.ArrayList;

/**
 * Created by heitorzc on 11/07/16.
 */
public class CountryList {

    ArrayList<Country> list;

    public ArrayList<Country> getList() {
        return list;
    }

    public void setList(ArrayList<Country> list) {
        this.list = list;
    }
}
