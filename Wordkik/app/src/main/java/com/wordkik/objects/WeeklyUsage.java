package com.wordkik.objects;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by heitorzc on 26/06/16.
 */
public class WeeklyUsage implements Serializable, Parcelable {

    String days;
    String usage;

    public WeeklyUsage(String days, String usage) {
        this.days = days;
        this.usage = usage;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    protected WeeklyUsage(Parcel in) {
        days = in.readString();
        usage = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(days);
        dest.writeString(usage);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<WeeklyUsage> CREATOR = new Parcelable.Creator<WeeklyUsage>() {
        @Override
        public WeeklyUsage createFromParcel(Parcel in) {
            return new WeeklyUsage(in);
        }

        @Override
        public WeeklyUsage[] newArray(int size) {
            return new WeeklyUsage[size];
        }
    };
}