package com.wordkik.objects;

/**
 * Created by heitorzc on 29/07/16.
 */
public class Coupon {

    String code;


    public Coupon(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
