package com.wordkik.objects.viewHolders;

/**
 * Created by heitorzc on 29/11/2016.
 */

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;
import com.wordkik.R;
import com.wordkik.WordKik;
import com.wordkik.utils.ResourceManager;
import com.wordkik.views.DailyUsageTextView;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class AppParentHolder extends GroupViewHolder {
    @Nullable @Bind(R.id.ivPhoto)    ImageView ivPhoto;
    @Nullable @Bind(R.id.tvName)     TextView tvName;
    @Nullable @Bind(R.id.tvLimit)    DailyUsageTextView tvLimit;
    @Nullable @Bind(R.id.indicator)  ImageView indicator;



    public AppParentHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }



    public void setAppIcon(Context context, String icon){

        icon = (icon != null && icon.length() > 0) ? icon : WordKik.getAppIcon(context, "com.android");

        Picasso.with(context).load(icon)
                .placeholder(ResourceManager.getInstance().drawable(R.drawable.android))
                .error(ResourceManager.getInstance().drawable(R.drawable.android))
                .into(ivPhoto);

    }



    public void setAppName(String name){
        tvName.setText(name);
    }



    public void setAppLimit(String wkUsage, String weUsage){
        tvLimit.setColorByUsage(wkUsage, weUsage);
    }



    @Override
    public void expand() {
        RotateAnimation rotate = new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);

        indicator.setAnimation(rotate);
    }



    @Override
    public void collapse() {
        RotateAnimation rotate = new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);

        indicator.setAnimation(rotate);

    }

}