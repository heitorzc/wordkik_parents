package com.wordkik.objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by dheringer on 6/12/2016.
 */
public class ResponseScreenTime implements Serializable {

    private boolean success;
    private String message;
    private Child child;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Child getChild() {
        return child;
    }

    public void setChild(Child child) {
        this.child = child;
    }
}
