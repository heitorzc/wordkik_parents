package com.wordkik.objects;

import java.util.ArrayList;

public class TimeLock {

    private int id;
    private String profile_id;
    private boolean active;
    private String start;
    private String duration;
    private String description;
    private int week_day;
    private ArrayList<Integer> days;
    private ArrayList<App> apps;


    public TimeLock() {
        this.days = new ArrayList<>();
        this.apps = new ArrayList<>();
    }

    public TimeLock(boolean active, String start, String description) {
        this.active = active;
        this.start = start;
        this.duration = duration;
        this.description = description;
        this.days = new ArrayList<>();
        this.apps = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void activate() {
        this.active = true;
    }

    public void deactivate() {
        this.active = false;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Integer> getDays() {
        return days;
    }

    public void setDays(ArrayList<Integer> days) {
        this.days = days;
    }

    public ArrayList<App> getApps() {
        return apps;
    }

    public void setApps(ArrayList<App> apps) {
        this.apps = apps;
    }

    public String getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(String child_id) {
        this.profile_id = child_id;
    }

    public int getWeek_day() {
        return week_day;
    }

    public void setWeek_day(int week_day) {
        this.week_day = week_day;
    }
}