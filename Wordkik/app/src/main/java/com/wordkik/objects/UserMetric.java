package com.wordkik.objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User metrics
 *
 * @author Denis Heringer
 * @version 1.0.0
 */
public class UserMetric implements Serializable {

    private ArrayList<Metric> metrics;

    public ArrayList<Metric> getMetrics() {
        return metrics;
    }

    public void setMetrics(ArrayList<Metric> metrics) {
        this.metrics = metrics;
    }

}
