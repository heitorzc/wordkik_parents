package com.wordkik.objects;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by heitorzc on 05/04/16.
 */
public class Metric extends RealmObject implements Serializable{

    String section;
    String time_entering;
    String time_leaving;

    public Metric() {
    }

    public Metric(String section, String time_entering) {
        this.section = section;
        this.time_entering = time_entering;
    }

    public Metric(String section, String time_entering, String time_leaving) {
        this.section = section;
        this.time_entering = time_entering;
        this.time_leaving = time_leaving;
    }

    public String getSection() {
        return section;
    }

    public String getTime_entering() {
        return time_entering;
    }

    public String getTime_leaving() {
        return time_leaving;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public void setTime_entering(String time_entering) {
        this.time_entering = time_entering;
    }

    public void setTime_leaving(String time_leaving) {
        this.time_leaving = time_leaving;
    }
}
