package com.wordkik.objects;

/**
 * Created by heitorzc on 29/08/16.
 */
public class CardPurchase {

    String stripe_token;
    String plan_id;
    String coupon;

    public CardPurchase(String stripe_token, String plan_id, String coupon) {
        this.stripe_token = stripe_token;
        this.plan_id = plan_id;
        this.coupon = coupon;
    }

    public String getStripe_token() {
        return stripe_token;
    }

    public void setStripe_token(String stripe_token) {
        this.stripe_token = stripe_token;
    }

    public String getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(String plan_id) {
        this.plan_id = plan_id;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }
}
