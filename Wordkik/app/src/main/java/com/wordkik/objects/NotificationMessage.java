package com.wordkik.objects;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pen15 on 4/25/2016.
 */
public class NotificationMessage implements Serializable {

    private Long multicast_id;
    private int success;
    private int failure;
    private int canonical_ids;
    private List<NotificationResult> results;

    public Long getMulticast_id() {
        return multicast_id;
    }

    public void setMulticast_id(Long multicast_id) {
        this.multicast_id = multicast_id;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getFailure() {
        return failure;
    }

    public void setFailure(int failure) {
        this.failure = failure;
    }

    public int getCanonical_ids() {
        return canonical_ids;
    }

    public void setCanonical_ids(int canonical_ids) {
        this.canonical_ids = canonical_ids;
    }

    public List<NotificationResult> getResults() {
        return results;
    }

    public void setResults(List<NotificationResult> results) {
        this.results = results;
    }
}
