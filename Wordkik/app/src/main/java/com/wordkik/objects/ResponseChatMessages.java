package com.wordkik.objects;

import java.util.ArrayList;

/**
 * Created by heitorzc on 09/08/16.
 */
public class ResponseChatMessages {

    boolean success;
    String message;
    ArrayList<ChatMessage> conversation_messages;
    int page;
    int total;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ChatMessage> getConversation_messages() {
        return conversation_messages;
    }

    public void setConversation_messages(ArrayList<ChatMessage> conversation_messages) {
        this.conversation_messages = conversation_messages;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
