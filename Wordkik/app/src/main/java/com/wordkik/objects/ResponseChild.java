package com.wordkik.objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by pen15 on 4/24/2016.
 */
public class ResponseChild implements Serializable {

    private boolean success;
    private String message;
    private Child child;
    private ArrayList<Child> children;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Child getChild() {
        return child;
    }

    public void setChild(Child child) {
        this.child = child;
    }

    public ArrayList<Child> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Child> children) {
        this.children = children;
    }
}


