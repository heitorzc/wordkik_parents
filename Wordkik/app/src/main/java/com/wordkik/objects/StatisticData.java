package com.wordkik.objects;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

public class StatisticData implements Serializable, Parcelable{
    private String id;
    private int block_count;
	private ArrayList<App> top_5;
	private String time_spent_games;
	private String time_spent_social;
	private String time_educational;

    public StatisticData(){

    }

    public StatisticData(int block_count, ArrayList<App> top_5, String time_spent_games, String time_spent_social, String time_educational) {
        this.block_count = block_count;
        this.top_5 = top_5;
        this.time_spent_games = time_spent_games;
        this.time_spent_social = time_spent_social;
        this.time_educational = time_educational;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getBlock_count() {
        return block_count;
    }

    public void setBlock_count(int block_count) {
        this.block_count = block_count;
    }

    public ArrayList<App> getTop_5() {
        return top_5;
    }

    public void setTop_5(ArrayList<App> top_5) {
        this.top_5 = top_5;
    }

    public String getTime_spent_games() {
        return time_spent_games;
    }

    public void setTime_spent_games(String time_spent_games) {
        this.time_spent_games = time_spent_games;
    }

    public String getTime_spent_social() {
        return time_spent_social;
    }

    public void setTime_spent_social(String time_spent_social) {
        this.time_spent_social = time_spent_social;
    }

    public String getTime_educational() {
        return time_educational;
    }

    public void setTime_educational(String time_educational) {
        this.time_educational = time_educational;
    }


    protected StatisticData(Parcel in) {
        id = in.readString();
        block_count = in.readInt();
        if (in.readByte() == 0x01) {
            top_5 = new ArrayList<App>();
            in.readList(top_5, App.class.getClassLoader());
        } else {
            top_5 = null;
        }
        time_spent_games = in.readString();
        time_spent_social = in.readString();
        time_educational = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeInt(block_count);
        if (top_5 == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(top_5);
        }
        dest.writeString(time_spent_games);
        dest.writeString(time_spent_social);
        dest.writeString(time_educational);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<StatisticData> CREATOR = new Parcelable.Creator<StatisticData>() {
        @Override
        public StatisticData createFromParcel(Parcel in) {
            return new StatisticData(in);
        }

        @Override
        public StatisticData[] newArray(int size) {
            return new StatisticData[size];
        }
    };
}