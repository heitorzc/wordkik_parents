package com.wordkik.objects;

import java.io.Serializable;

/**
 * Created by pen15 on 4/24/2016.
 */
public class Notification implements Serializable {

    private String title;
    private String body;
    private String sound;
    private String click_action;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getClick_action() {
        return click_action;
    }

    public void setClick_action(String click_action) {
        this.click_action = click_action;
    }

}
