package com.wordkik.objects;

import java.io.Serializable;

/**
 * Created by heitorzc on 07/12/2016.
 */

public class ScreenTime implements Serializable {

    private int id;
    private String weekday;
    private String usage;
    private long timeSpent;
    private boolean limitExceeded;

    public ScreenTime(int id, String weekday, String usage) {
        this.id = id;
        this.weekday = weekday;
        this.usage = usage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWeekday() {
        return weekday;
    }

    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public long getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(long timeSpent) {
        this.timeSpent = timeSpent;
    }

    public boolean isLimitExceeded() {
        return limitExceeded;
    }

    public void setLimitExceeded(boolean limitExceeded) {
        this.limitExceeded = limitExceeded;
    }

}
