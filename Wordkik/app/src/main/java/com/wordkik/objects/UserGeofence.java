package com.wordkik.objects;

import java.io.Serializable;

/**
 * Created by heitorzc on 12/04/16.
 */
public class UserGeofence implements Serializable{

    int id;
    int child_id;
    String profile_id;
    String category;
    String description;
    double lat;
    double lon;
    int radius = 0;

    public UserGeofence(String profile_id, String category, String description, double lat, double lon, int radius) {
        this.profile_id = profile_id;
        this.category = category;
        this.description = description;
        this.lat = lat;
        this.lon = lon;
        this.radius = radius;
    }

    public UserGeofence(double lat, double lon, int radius) {
        this.lat = lat;
        this.lon = lon;
        this.radius = radius;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getChild_id() {
        return child_id;
    }

    public void setChild_id(int child_id) {
        this.child_id = child_id;
    }

    public String getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(String profile_id) {
        this.profile_id = profile_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public boolean isCompleted(){
        if (description != null && lat != 0 && lon != 0 && radius != 0){
            return true;
        }
        else{
            return false;
        }
    }
}
