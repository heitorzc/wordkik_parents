package com.wordkik.objects;

import com.wordkik.Constants;

/**
 * Created by heitorzc on 10/06/16.
 */
public class DeviceInfo {

    String MANUFACTURER  = android.os.Build.MANUFACTURER;
    String BRAND         = android.os.Build.BRAND;
    String MODEL         = android.os.Build.MODEL;
    String BOOTLOADER    = android.os.Build.BOOTLOADER;
    String OS_VERSION    = "Android API " + Constants.SDK;
}
