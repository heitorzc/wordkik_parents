package com.wordkik.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by heitorzc on 26/06/16.
 */
public class DailyUsage implements Parcelable {

    int weekday;
    String usage;

    public DailyUsage(int weekday, String usage) {
        this.weekday = weekday;
        this.usage = usage;
    }

    public int getWeekday() {
        return weekday;
    }

    public void setWeekday(int weekday) {
        this.weekday = weekday;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    protected DailyUsage(Parcel in) {
        weekday = in.readInt();
        usage = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(weekday);
        dest.writeString(usage);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<DailyUsage> CREATOR = new Parcelable.Creator<DailyUsage>() {
        @Override
        public DailyUsage createFromParcel(Parcel in) {
            return new DailyUsage(in);
        }

        @Override
        public DailyUsage[] newArray(int size) {
            return new DailyUsage[size];
        }
    };
}