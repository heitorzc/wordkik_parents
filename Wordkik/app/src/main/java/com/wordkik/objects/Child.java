package com.wordkik.objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Model to describe a child
 *
 * @author  Heitor Zanetti
 * @version  1.0.0
 */
public class Child implements Serializable {

    private int id;
    private String profile_id;
    private String name;
    private String photo;
    private String gender;
    private String device;
    private String status;
    private String age;
    private boolean app_up_to_date;
    private ArrayList<Alert> alerts             = new ArrayList<>();
    private ArrayList<App> locked_apps          = new ArrayList<>();
    private ArrayList<TimeLock> timeLocks       = new ArrayList<>();
    private ArrayList<StatisticData> statistics = new ArrayList<>();
    private ArrayList<UserGeofence> geofences   = new ArrayList<>();
    private ArrayList<ScreenTime> daily_usages  = new ArrayList<>();

    public Child() {

    }

    public Child(String id, String name, String photo, String gender, String age) {
        this.profile_id = id;
        this.name = name;
        this.photo = photo;
        this.gender = gender;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProfile_id() {
        return profile_id;
    }

    public String getName() {
        return name;
    }

    public String getPhoto() {
        return photo;
    }

    public String getDevice() {
        return device;
    }

    public String getGender() {
        return gender;
    }

    public ArrayList<Alert> getAlerts() {
        return alerts;
    }

    public ArrayList<Alert> getAlertsByStatus(String status) {
        ArrayList<Alert> filtered_alerts = new ArrayList<>();
        for (int i = 0; i < alerts.size(); i++){
            if (alerts.get(i).getStatus().equals(status)){
                filtered_alerts.add(alerts.get(i));
            }
        }
        return filtered_alerts;
    }

    public ArrayList<App> getLocked_apps() {
        return locked_apps;
    }

    public ArrayList<App> getAppsWithUsageLimit(){

        ArrayList<App> apps = new ArrayList<>();

        for (App app : getLocked_apps()){
            if (app.hasUsageLimit()){
                apps.add(app);
            }
        }

        return apps;
    }


    public int getUsageCount(){
        return getAppsWithUsageLimit().size();
    }

    public ArrayList<TimeLock> getTimeLocks() {
        return timeLocks;
    }

    public ArrayList<StatisticData> getStatistics() {
        return statistics;
    }

    public ArrayList<UserGeofence> getGeofences() {
        return geofences;
    }

    public UserGeofence getGeofenceById(int id){
        for (UserGeofence geofence : geofences){
            if (geofence.getId() == id){
                return geofence;
            }
        }

        return null;
    }

    public void setGeofences(ArrayList<UserGeofence> geofences) {
        this.geofences = geofences;
    }

    public void setProfile_id(String profile_id) {
        this.profile_id = profile_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAlerts(ArrayList<Alert> alerts) {
        this.alerts = alerts;
    }

    public void setLocked_apps(ArrayList<App> locked_apps) {
        this.locked_apps = locked_apps;
    }

    public void setTimeLocks(ArrayList<TimeLock> timeLocks) {
        this.timeLocks = timeLocks;
    }

    public void setStatistics(ArrayList<StatisticData> statistics) {
        this.statistics = statistics;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public boolean isApp_up_to_date() {
        return app_up_to_date;
    }

    public void setApp_up_to_date(boolean app_up_to_date) {
        this.app_up_to_date = app_up_to_date;
    }

    public ArrayList<ScreenTime> getDaily_usages() {
        return daily_usages;
    }

    public void setDaily_usages(ArrayList<ScreenTime> daily_usages) {
        this.daily_usages = daily_usages;
    }

    public ScreenTime getScreenLimitByWeekday(String days) {

        for (ScreenTime s : getDaily_usages()){
            if (s.getWeekday().equals(days)){
                return s;
            }
        }

        return null;
    }
}
