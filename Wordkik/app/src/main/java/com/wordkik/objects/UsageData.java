package com.wordkik.objects;

import java.io.Serializable;

/**
 * Created by heitorzc on 17/06/16.
 */
public class UsageData implements Serializable {

    String category;
    float value;

    public UsageData(String category, float value) {
        this.category = category;
        this.value = value;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
