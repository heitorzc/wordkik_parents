package com.wordkik.objects;

import java.util.ArrayList;

/**
 * Created by heitorzc on 02/09/16.
 */
public class ResponseGeoActivities {

    boolean success;
    String message;
    ArrayList<GeoActivity> transitions;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GeoActivity> getTransitions() {
        return transitions;
    }

    public void setTransitions(ArrayList<GeoActivity> transitions) {
        this.transitions = transitions;
    }
}
