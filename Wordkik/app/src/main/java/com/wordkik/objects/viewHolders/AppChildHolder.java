package com.wordkik.objects.viewHolders;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.wordkik.R;
import com.wordkik.adapters.RvLockAppsAdapter;
import com.wordkik.objects.App;
import com.wordkik.views.SeekBarHint;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by heitorzc on 29/11/2016.
 */

public class AppChildHolder extends ChildViewHolder {
    @Bind(R.id.sbWeekdays)       SeekBarHint sbWeekdays;
    @Bind(R.id.sbWeekends)       SeekBarHint sbWeekends;
    @Bind(R.id.tvWeekdaysBubble) TextView tvWeekdaysBubble;
    @Bind(R.id.tvWeekendBubble)  TextView tvWeekendsBubble;

    public AppChildHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void configureSeekBars(RvLockAppsAdapter adapter, App app, boolean validLimit){

        sbWeekdays.configure(adapter, app, tvWeekdaysBubble, true);
        sbWeekends.configure(adapter, app, tvWeekendsBubble, false);

        if (!validLimit) {
            sbWeekdays.setEnabled(false);
            sbWeekends.setEnabled(false);
        }
        else{
            sbWeekdays.setEnabled(true);
            sbWeekends.setEnabled(true);
        }

    }

}