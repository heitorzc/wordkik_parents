package com.wordkik.objects;

import java.io.Serializable;

/**
 * Created by pen15 on 4/25/2016.
 */
public class NotificationResult implements Serializable {

    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
