package com.wordkik.objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by dheringer on 5/1/2016.
 */
public class ResponseInstalledApps implements Serializable {

    private boolean success;
    private String message;
    private ArrayList<App> apps;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<App> getApps() {
        return apps;
    }

    public void setApps(ArrayList<App> apps) {
        this.apps = apps;
    }
}
