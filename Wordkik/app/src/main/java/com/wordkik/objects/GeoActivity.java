package com.wordkik.objects;

/**
 * Created by heitorzc on 31/08/16.
 */
public class GeoActivity {

    String geofence_name;
    int transition; // 1 for GEOFENCE_TRANSITION_ENTER and 2 for GEOFENCE_TRANSITION_EXIT
    String timestamp;
    int child_id;


    public GeoActivity(String geofence_name, int transition, String timestamp, int child_id) {
        this.geofence_name = geofence_name;
        this.transition = transition;
        this.timestamp = timestamp;
        this.child_id = child_id;
    }

    public String getGeofence_name() {
        return geofence_name;
    }

    public void setGeofence_name(String geofence_name) {
        this.geofence_name = geofence_name;
    }

    public int getTransition() {
        return transition;
    }

    public void setTransition(int transition) {
        this.transition = transition;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getChild_id() {
        return child_id;
    }

    public void setChild_id(int child_id) {
        this.child_id = child_id;
    }
}
