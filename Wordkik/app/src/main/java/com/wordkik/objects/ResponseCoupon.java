package com.wordkik.objects;

/**
 * Created by heitorzc on 08/08/16.
 */
public class ResponseCoupon {

    private boolean success;
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
