package com.wordkik;

import android.content.Context;
import android.os.Build;

import com.wordkik.objects.Alert;
import com.wordkik.objects.Child;
import com.wordkik.objects.Parent;

import java.util.ArrayList;

/**
 * Created by heitorzc on 09/02/16.
 */
public class Constants {

    public static int screen_width;
    public static int screen_height;

    // DONT USE THIS PARENT OBJECT - IT SHOULD BE USED JUST IN THE REGISTRATION PROCESS
    public static Parent registeredParent = null;

    public static int SDK = Build.VERSION.SDK_INT;
    public static String app_version = BuildConfig.VERSION_NAME;

    public static boolean allow_notifications = WordKik.prefs.getBoolean("allow_push", true);

    public static boolean hasNewAlerts = false;

    public static String user_id;
    public static String user_pushy_id;
    //public static String user_gcm_id;
    public static String user_photo;
    public static String user_name;
    public static String user_lastName;
    public static String user_email;
    public static String user_phone_number;
    public static String user_country;
    public static String user_token             = "";
    public static String user_gender;
    public static String user_account_type      = "";
    public static int user_expiration           = 999;
    public static String user_age;
    public static String user_state;
    public static boolean user_up_to_date;
    public static String user_pincode = WordKik.prefs.getString("pincode", "");

    public static boolean isSubscriptionValid                     = true;

    public static ArrayList<Child> childs                         = new ArrayList<>();
    public static ArrayList<Alert> alerts                         = new ArrayList<>();

    public static int alerts_counter                              = 0;

    public static final String USER_EMAIL_KEY                     = "USER_EMAIL";
    public static final String USER_PASS_KEY                      = "USER_PASS";

    public static boolean isLoggedIn                              = false;
    public static boolean shouldNotProceed                        = false;
    public static final String WORDKIK_BROWSER_PACKAGE_NAME       = "com.wordkik.wordkikbrowser";


    public static final String NOTIFICATION_TYPE_REQUEST_LOCATION = "location";
    public static final String NOTIFICATION_TYPE_CMS              = "wk_cms";
    public static final String NOTIFICATION_TYPE_CHAT             = "chat_message";
    public static final String NOTIFICATION_TYPE_ALERT            = "alert";
    public static final String NOTIFICATION_TYPE_LOCK_APPS        = "lockapps";
    public static final String NOTIFICATION_TYPE_GEOFENCE_RPT     = "geofence_report";


    public static final String NOTIFICATION_CAT_UPDATE            = "update";
    public static final String NOTIFICATION_CAT_GENERAL           = "general";
    public static final String NOTIFICATION_CAT_GEOFENCE          = "geofence_activity";
    public static final String NOTIFICATION_CAT_GEOMODE           = "geofence_mode";


    public static boolean hasChildren(){
        return (childs.size() > 0);
    }

    public static Child getChildByProfileId(String id){

        for (Child child : childs){
            if (child.getProfile_id().equals(id)){
                return child;
            }
        }

        return null;
    }

    public static Child getChildById(int id){

        for (Child child : childs){
            if (child.getId() == id){
                return child;
            }
        }

        return null;
    }


    public static String getAccountTypeString(Context context, String user_account_type){
        if (user_account_type != null) {
            String result = (user_account_type.equals("paying")) ? context.getString(R.string.premium_acc) : context.getString(R.string.free_acc);
            return result;
        }

        return context.getString(R.string.free_acc);
    }

    public static void setContantsParentFields(Parent parent) {
        Constants.user_id           = String.valueOf(parent.getId());
        Constants.user_photo        = parent.getPhoto();
        Constants.user_name         = parent.getFirstName();
        Constants.user_lastName     = parent.getLastName();
        Constants.user_email        = parent.getEmail();
        Constants.user_phone_number = parent.getPhoneNumber();
        Constants.user_country      = parent.getCountry();
        Constants.user_token        = parent.getAuth_token();
        Constants.user_gender       = parent.getGender();
        Constants.user_account_type = parent.getAccount_type();
        Constants.user_expiration   = parent.getSubscription_left();
        Constants.user_age          = parent.getAge();
        Constants.user_pincode      = parent.getPincode();
        Constants.user_state        = parent.getState();
        Constants.user_up_to_date   = parent.isApp_up_to_date();
    }


}
